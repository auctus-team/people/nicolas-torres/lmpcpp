import time
import traceback
from pdb import pm
import matplotlib.pyplot as plt
import aux
aux.setup_latex()

import sys
import os

srcabspath = os.path.join(os.path.abspath(os.path.dirname(__file__)))
print('abspath(__file__):', srcabspath)
###################################################################
import os
import sys

srcabspath = os.path.join(os.path.abspath(os.path.dirname(__file__)))
buildabspath = os.path.join(
    os.path.abspath(os.path.dirname(__file__)),
    '../../../../'  # workspace build/
    + 'build')
print('buildabspath:', buildabspath)
## add mpcpp dir to path
sys.path.append(os.path.join(buildabspath, 'lmpcpp'))
print('import mpcpp...')
import pylmpcpp as mpcpp
print('finished import mpcpp...')

## add ruckig dir to path
sys.path.append(os.path.join(buildabspath, 'ruckig'))
import ruckig as ruc
###################################################################
import numpy as np

np.set_printoptions(
    edgeitems=4,
    linewidth=130,
    # suppress=True,
    threshold=1000,
    precision=5)
import scipy as sp
import pinocchio as pin
import csv


##########################################################################
def stringToMotion(mystr):
    return pin.Motion(np.array([float(x) for x in mystr.split()]))


pose0_log = stringToMotion(
    '0.81813072  0.73563557 -0.32793589  2.87568122 -1.19176581 -0.10189045')

start_pose = pin.exp(pose0_log)
start_pose.rotation = np.eye(3)

objl = pin.SE3(
    start_pose.rotation,
    # start_pose.translation + np.array([-0.6, 0.5, -0.2]))
    start_pose.translation + np.array([-.05, -0.1, -.3]))
# start_pose.translation + np.array([-.05, -2.1, -.3]))
objr = pin.SE3(
    start_pose.rotation,
    # start_pose.translation + np.array([-0.1, 0.5, -0.2]))
    start_pose.translation + np.array([-.05, 0.1, -.3]))
objrx = pin.SE3(start_pose.rotation, objl.translation + np.array([.2, 0, 0]))
objrz = pin.SE3(start_pose.rotation, objl.translation + np.array([0, 0, 0.2]))
# start_pose.translation + np.array([-.05, 20.1, -.3]))
# start_pose.translation + np.array([-.05, 8.1, -.3]))

print('pos0:', objl.translation)
print('posf:', objr.translation)

#######################################################
#######################################################

fake_compute_time = 0.01
# fake_compute_time = 0
ndt = 5
# ndt = 1
solveparams = mpcpp.SolveParameters()
# umax = np.hstack([1.7, 1.7, 1.7, 2.5, 2.5, 2.5])
# udmax = np.hstack([0.65, 0.65, 0.65, 1.25, 1.25, 1.25]) * 2
# uddmax = np.hstack([6500, 6500, 6500, 12500, 12500, 12500])
solveparams.u_scaling_factor = 0.3
solveparams.ud_scaling_factor = 1
solveparams.udd_scaling_factor = 1
umax = np.hstack([1, 1, 1, 2, 2, 2])
udmax = np.hstack([5, 5, 5, 10, 10, 10])
uddmax = np.hstack([1000, 1000, 1000, 3000, 3000, 3000])
solveparams.u = mpcpp.VectorLimits(-umax, umax)
solveparams.ud = mpcpp.VectorLimits(-udmax, udmax)
solveparams.udd = mpcpp.VectorLimits(-uddmax, uddmax)

print('Create mpc.MPCProblem...')
params = mpcpp.HorizonParameters(
    # H=200,
    # H=10,
    # H=80,
    # H=38,
    H=15,
    ndt=ndt,
    # dt=5.0e-3,
)
sim_dt = params.dt
# sim_dt = params.hdt
solver = 'osqp'
# solver = 'qpoases'
mpcprob = mpcpp.Planifier(params, solver=solver)
mpcprob.use_origin_dlog = True
mpcprob.update_cost_ponderation(
    mpcpp.CostPonderation(1, 1, terminal_weight_factor=1))
mpcprob.wreg1 = 1e-6
mpcprob.wreg1 = 1.5e-4
# mpcprob.wreg1 = 1e-3
# mpcprob.wreg1 = 1e-2
mpcprob.wreg2 = 8e-8
# mpcprob.wreg2 = 1e-6
# mpcprob.wreg2 = 1e-5
# mpcprob.wreg2 = 1e-4
# mpcprob.wreg1 = 0
# mpcprob.wreg2 = 0
simulation_duration = 0.7  # [s]
simulation_duration = .097  # [s]
# simulation_duration = .105  # [s]
simulation_duration = .205  # [s]
# simulation_duration = .3  # [s]
simulation_duration = .8  # [s]
simulation_duration = 1  # [s]
# simulation_duration = 2  # [s]
# simulation_duration = 3  # [s]
# mpc_update_delay = 2
# simulation_duration = 3  # [s]
# simulation_duration = 20  # [s]
# simulation_duration = .005  # [s]
# simulation_duration = 3*params.hdt
# simulation_duration = mpcprob.input_horizon_duration() - params.dt
# simulation_duration = 3 * (mpcprob.input_horizon_duration() - params.dt)
mpc_update_delay = 0.02  # 50Hz
# mpc_update_delay = 0.05  # 20Hz
# mpc_update_delay = 0.1
# mpc_update_delay = .3
# mpc_update_delay = .5
# mpc_update_delay = 3
# mpc_update_delay = 0.01
# mpc_update_delay = mpcprob.input_horizon_duration()
# mpc_update_delay = simulation_duration / 2
# mpc_update_delay = simulation_duration + fake_compute_time
# simulation_duration = 3 * mpc_update_delay

mpcprob.set_compute_time(30)
mpcprob.set_compute_time(10)
mpcprob.set_compute_time(1)
# mpcprob.set_compute_time(0.1)
# mpcprob.set_compute_time(0.01)
mpcprob.update_solve_parameters(solveparams)
print('Create mpcpp.MPCProblem...')

pose_desired = aux.History(objl.homogeneous)
# pose_desired = aux.History(start_pose.homogeneous)
pose_target = aux.History(objr.homogeneous)
# pose_target = aux.History(objrx.homogeneous)
# pose_target = aux.History(objrz.homogeneous)
# pose_target = aux.History(pose_desired.initial())

vel_desired = aux.History(pin.Motion.Zero().vector, t0=-sim_dt)
acc_desired = aux.History(pin.Motion.Zero().vector, t0=-sim_dt)
jerk_desired = aux.History(pin.Motion.Zero().vector, t0=-sim_dt)

pose_real = aux.History(pose_desired.last())

#######################################################
#######################################################
# ruckig
ruc_traj = ruc.Ruckig(3, sim_dt)
# Set input parameters

inp = ruc.InputParameter(3)
inp.current_position = pin.SE3(pose_real.last()).translation
inp.current_velocity = [0.0, 0., 0.]
inp.current_acceleration = [0., 0., 0.]
inp.target_position = pin.SE3(pose_target.last()).translation
inp.target_velocity = [0., 0., 0.]
inp.target_acceleration = [0., 0., 0.]
inp.max_velocity = mpcprob.u_max().max[:3]
inp.max_acceleration = mpcprob.ud_max().max[:3]
inp.max_jerk = mpcprob.udd_max().max[:3]
otp = ruc.OutputParameter(3)

position_real_ruc = aux.History(np.array(inp.current_position))
position_desired_ruc = aux.History(np.array(inp.current_position))
velocity_desired_ruc = aux.History(np.array(inp.current_velocity), t0=-sim_dt)
acceleration_desired_ruc = aux.History(np.array(inp.current_acceleration),
                                       t0=-sim_dt)
jerk_desired_ruc = aux.History(np.zeros(3), t0=-sim_dt)

#######################################################
#######################################################
print('+' * 80)
print('simulation_duration:', simulation_duration)
print('mpc_update_delay:', mpc_update_delay)
print('mpcprob.input_horizon_duration():', mpcprob.input_horizon_duration())
if 2 * fake_compute_time > mpc_update_delay:
    print('Error: 2 x fake_compute_time < mpc_update_delay <<<=======')
    print('fake_compute_time:', fake_compute_time)
    print('mpc_update_delay:', mpc_update_delay)
    sys.exit(0)

if mpcprob.input_horizon_duration(
) < 2 * fake_compute_time + mpc_update_delay - params.dt:
    print(
        'Warning: input_horizon <= 2x fake_compute_time + mpc_update_delay - dt <<<======='
    )
    print('mpcprob.input_horizon_duration():',
          mpcprob.input_horizon_duration())
    print('mpc_update_delay:', mpc_update_delay)
    print('fake_compute_time:', fake_compute_time)
    print('2x fake_compute_time + mpc_update_delay - params.dt:',
          2 * fake_compute_time + mpc_update_delay - params.dt)
    # sys.exit(0)
print('+' * 80)

t = 0
t_mpc_update = -1  # so that it forces update first time
t_mpc_compute = -1  # so that it forces update first time
target = pose_target.initial()
vel_prev = None
while t < simulation_duration:
    if t > t_mpc_compute:
        print('compute horizon t: {:.2f}s, t_mpc_update: {:.2f}'.format(
            t, t_mpc_compute))
        start = mpcpp.TimedPose()
        start.t = t
        start.pose = pose_real.last()
        goal = mpcpp.Goal()
        goal.u0 = vel_desired.last()
        goal.ud0 = acc_desired.last()
        goal.start = start
        # if t > simulation_duration / 2:
        # if t > 0.3:
        #     target = objl.homogeneous
        goal.pose = target
        t_mpc_compute = t + mpc_update_delay - fake_compute_time
        print('computed horizon t: {:.2f}s, next: {:.2f}'.format(
            t, t_mpc_compute))
    if t > t_mpc_update:
        horizon = mpcprob.solve(goal, solveparams)
        t_mpc_update = t + mpc_update_delay
        print('updated horizon t: {:.2f}s, next: {:.2f}'.format(
            t, t_mpc_update))
        print('_' * 80)

    print('t0:{:.3f}s, t:{:.3f}s'.format(horizon.t0(), t))
    ###################################################
    # ruckig
    inp.target_position = pin.SE3(target).translation
    res = ruc_traj.update(inp, otp)
    if res != ruc.Result.Working and res != ruc.Result.Finished:
        print('Error: res:', res)

    position_desired_ruc.add(t, np.array(otp.new_position))
    velocity_desired_ruc.add(t, np.array(otp.new_velocity))
    acceleration_desired_ruc.add(t, np.array(otp.new_acceleration))
    jerk_desired_ruc.add(
        t, (np.array(otp.new_acceleration) - acceleration_desired_ruc.at(-2)) /
        sim_dt)

    otp.pass_to_input(inp)
    ##################################################

    pose_desired.add(t, horizon.pose(t + sim_dt))
    pose_desired_LWA = pin.SE3(
        pin.SE3(pose_desired.at(-1)).rotation, np.zeros(3))

    pose_target.add(t, target)
    # clipping
    if vel_prev is not None:
        vmax = vel_prev + mpcprob.ud_max().max * sim_dt
        vmin = vel_prev + mpcprob.ud_max().min * sim_dt
        vel_prev = np.minimum(vmax, horizon.twist(t))
        vel_prev = np.maximum(vmin, vel_prev)
    else:
        vel_prev = horizon.twist(t)
    acc_prev = (vel_prev - vel_desired.last()) / sim_dt
    # vel_prev2 = horizon.u(t - sim_dt)

    ###################################################
    # udd(k=0) = (ud(k=0) - ud(k=-1)) / dt
    #          = (u(k=0) - 2 u(k=-1) + u(k=-2)) / (dt*dt)
    if acc_desired.len() > 1:
        jerk_desired.add(t, (acc_desired.at(-1) - acc_desired.at(-2)) / sim_dt)
    # if acc_desired.len() > 1:
    #     jerk_desired.add(t, (acc_prev - acc_desired.last()) / sim_dt)
    # if vel_desired.len() >= 2:
    #     jerk_desired.add(
    #         t,
    #         aux.ActTwist(
    #             pose_desired_LWA,
    #             # (vel_prev - 2 * vel_desired.at(-1) + vel_desired.at(-2))
    #             (horizon.twist(t + sim_dt) - 2 * vel_prev +
    #              vel_desired.last()) / (sim_dt * sim_dt)))
    ###################################################

    if vel_desired.len() > 1:
        acc_desired.add(t, aux.ActTwist(pose_desired_LWA, acc_prev))
    vel_desired.add(t, aux.ActTwist(pose_desired_LWA, vel_prev))

    if t >= sim_dt:
        pose_real.add(t, pose_real.last() @ pin.exp(vel_prev * sim_dt))
        # pose_real.add(t, (pin.SE3(pose_real.last())*pin.exp(velk)).homogeneous)

    t += sim_dt

t = 0
while t < simulation_duration:
    t += sim_dt
    jerk = jerk_desired_ruc.at_t(t - 3 * sim_dt)
    if t <= 3 * sim_dt:
        jerk.fill(0)
    acc = acceleration_desired_ruc.at_t(t - 2 * sim_dt)
    if t <= 2 * sim_dt:
        acc.fill(0)
    position_real_ruc.add(
        t,
        position_real_ruc.last() +
        velocity_desired_ruc.at_t(t - sim_dt) * sim_dt + acc *
        (sim_dt**2) / 2 + jerk * (sim_dt**3) / 6)

# t = sim_dt
# # t = 0
# acc_desired = aux.History(pin.Motion.Zero().vector, t0=-sim_dt)
# while t < simulation_duration:
#     acc_desired.add(
#         t, (vel_desired.at_t(t) - vel_desired.at_t(t - sim_dt)) / sim_dt)
#     t += sim_dt

# t = 0
# jerk_desired = aux.History(pin.Motion.Zero().vector, t0=-sim_dt)
# while t < simulation_duration:
#     t += sim_dt
#     jerk_desired.add(
#         t, (vel_desired.at_t(t + sim_dt) - 2 * vel_desired.at_t(t) +
#             vel_desired.at_t(t - sim_dt)) / (sim_dt * sim_dt))
#     # jerk_desired.add(
#     #     t, (acc_desired.at_t(t) - acc_desired.at_t(t - sim_dt)) / sim_dt)
#     # jerk_desired.add(
#     #     t, (acc_desired.at_t(t + sim_dt) - acc_desired.at_t(t)) / sim_dt)

#######################################################
# rows = 3

# fig.suptitle('linear')
position_real = aux.History.fromHomogeneous(pose_real)
position_desired = aux.History.fromHomogeneous(pose_desired)
position_target = aux.History.fromHomogeneous(pose_target)
jerk_desired = None  # uncomment to remove jerk from graph

aux.save_xyz(
    position_real,
    position_desired,
    position_target,
    vel_desired,
    acc_desired,
    jerk_desired,
    single_coord=1,
    # separate_coords=True,
    show_position_real_ruc=False,
    # show_position_des_ruc=True,
    ruckig_list=[
        position_real_ruc,
        position_desired_ruc,
        velocity_desired_ruc,
        acceleration_desired_ruc,
        # jerk_desired_ruc
    ],
)

# fig, axes = plt.subplots(1,
#                          1,
#                          num=1,
#                          sharex='col',
#                          gridspec_kw={
#                              'hspace': 0.18,
#                              'wspace': 0
#                          })
# position_error = aux.History.Errorfrom(position_desired_ruc, position_real_ruc)
# position_error.plot(axes,
#                     lbl='ruckig error',
#                     ls='solid',
#                     clr='r',
#                     lw=5,
#                     alp=1,
#                     row=1,
#                     botvisible=True)

plt.show()
