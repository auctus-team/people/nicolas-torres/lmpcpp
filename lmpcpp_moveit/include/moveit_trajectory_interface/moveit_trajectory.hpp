#pragma once

#include "pinocchio/fwd.hpp" // SE3
#include "pinocchio/multibody/data.hpp"
#include "pinocchio/multibody/model.hpp"

// #include <cstddef>
#include <ros/node_handle.h>

// MoveIt
// #include <actionlib/server/simple_action_server.h>
// #include <moveit_trajectory_interface/TrajectoryAction.h>
// #include <moveit/kinematic_constraints/utils.h>
// #include <moveit/move_group_interface/move_group_interface.h>
// #include <moveit/planning_interface/planning_interface.h>
// #include <moveit/planning_pipeline/planning_pipeline.h>
// #include <moveit/planning_scene_monitor/planning_scene_monitor.h>
// #include <moveit/robot_model_loader/robot_model_loader.h>
// #include <moveit/robot_state/conversions.h>
// #include <moveit/trajectory_processing/time_optimal_trajectory_generation.h>
// #include <moveit_msgs/DisplayTrajectory.h>
#include <moveit_msgs/RobotTrajectory.h>
// #include <moveit_msgs/PlanningScene.h>

#include <geometry_msgs/PoseArray.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TwistStamped.h> //
#include <realtime_tools/realtime_publisher.h>
//

// #include <nav_msgs/Path.h>
#include <chrono>
#include <lmpcpp/mpc.hpp>
#include <mutex>
#include <sensor_msgs/JointState.h>
// #include <std_srvs/Empty.h>
#include <atomic>
#include <string>

class MoveItTrajectory {
public:
  MoveItTrajectory();

  void init(std::string planning_group,
            std::string robot_description_param_name, Eigen::VectorXd q_init,
            std::vector<std::string> joint_names = std::vector<std::string>());

  bool load_robot_model(
      std::string robot_description,
      std::vector<std::string> joint_names = std::vector<std::string>());

  void setControlledFrame(std::string controlled_frame);
  void catch_update_horizon(bool force_wait = false);

  void abort_objective();

  void set_objective(const Eigen::VectorXd start, const Eigen::VectorXd end);
  /**
   * \brief
   * Get the desired Pose
   */
  Eigen::Affine3d getCartesianPose();

  /**
   * \brief
   * Get the desired Twist
   */
  Eigen::Matrix<double, 6, 1> getCartesianVelocity();

  /**
   * \brief
   * Get the desired Acceleration
   */
  Eigen::Matrix<double, 6, 1> getCartesianAcceleration();

  Eigen::VectorXd getJointAcceleration();

  Eigen::VectorXd getJointVelocity();

  Eigen::VectorXd getJointConfiguration();

  Eigen::VectorXd getJointAccelerationAtTime(double time);

  bool computeNewTrajectory(moveit_msgs::RobotTrajectory path,
                            double max_velocity_scaling_factor,
                            double max_acceleration_scaling_factor);

  void updateTrajectory(const Eigen::VectorXd &q, const Eigen::VectorXd &qd,
                        const Eigen::VectorXd &qdd);

  void publish_trajectory();

  double getTimeProgress();

  bool GoalReached();

  bool isInitialized();

  std::string getRobotName();

  void sortJointState(moveit_msgs::RobotTrajectory &msg);

  Eigen::VectorXd xd_limits();

  double control_period();
  void set_u_scaling_factor(double u_scaling_factor);

private:
  ros::NodeHandle node_handle;
  // Model information
  pinocchio::Model model; /*!< @brief pinocchio model*/
  pinocchio::Data data;   /*!< @brief pinocchio data*/
  std::string controlled_frame;

  // Trajectory parameters
  double t_traj = 0.0;
  Eigen::Matrix3d epsR;
  Eigen::Affine3d cartesian_pose;
  pinocchio::SE3 target_pose;
  Eigen::Matrix<double, 6, 1> target_pose_log;
  Eigen::Matrix<double, 6, 1> cartesian_velocity;
  Eigen::Matrix<double, 6, 1> cartesian_velocity_local;
  Eigen::Matrix<double, 6, 1> cartesian_acceleration;
  Eigen::Matrix<double, 6, 1> cartesian_acceleration_localpub;
  Eigen::Matrix<double, 6, 1> cartesian_acceleration_local;
  Eigen::Matrix<double, 6, 1> cartesian_acceleration_local_prev;
  Eigen::Matrix<double, 6, 1> cartesian_jerk_local;
  Eigen::VectorXd joint_configuration;
  Eigen::VectorXd joint_velocity;
  Eigen::VectorXd joint_acceleration;
  std::atomic<bool> trajectory_is_built;

  lmpcpp::Delay horizon_publish_delay;
  realtime_tools::RealtimePublisher<geometry_msgs::PoseArray> horizon_publisher;
  realtime_tools::RealtimePublisher<sensor_msgs::JointState>
      jointspace_limit_distance_publisher;
  realtime_tools::RealtimePublisher<geometry_msgs::TwistStamped>
      current_velocity_se3_publisher;
  realtime_tools::RealtimePublisher<geometry_msgs::TwistStamped>
      desired_velocity_se3_publisher;
  realtime_tools::RealtimePublisher<geometry_msgs::TwistStamped>
      min_velocity_se3_publisher;
  realtime_tools::RealtimePublisher<geometry_msgs::TwistStamped>
      max_velocity_se3_publisher;
  realtime_tools::RealtimePublisher<geometry_msgs::TwistStamped>
      current_acceleration_se3_publisher;
  realtime_tools::RealtimePublisher<geometry_msgs::TwistStamped>
      desired_acceleration_se3_publisher;
  realtime_tools::RealtimePublisher<geometry_msgs::TwistStamped>
      desired_jerk_se3_publisher;
  realtime_tools::RealtimePublisher<geometry_msgs::TwistStamped>
      current_pose_se3_publisher;
  realtime_tools::RealtimePublisher<geometry_msgs::TwistStamped>
      error_target_pose_se3_publisher;
  realtime_tools::RealtimePublisher<geometry_msgs::TwistStamped>
      desired_pose_se3_publisher;
  realtime_tools::RealtimePublisher<geometry_msgs::TwistStamped>
      target_pose_se3_publisher;
  realtime_tools::RealtimePublisher<geometry_msgs::TwistStamped>
      initial_mpc_pose_se3_publisher;
  realtime_tools::RealtimePublisher<geometry_msgs::TwistStamped>
      obstacle_distance_se3_publisher;

  Eigen::Matrix<double, 6, 1> current_initial_mpc_pose;
  ros::Time current_initial_mpc_pose_rostime;

  // ROS Publishers
  realtime_tools::RealtimePublisher<geometry_msgs::PoseStamped>
      desired_pose_publisher;
  realtime_tools::RealtimePublisher<geometry_msgs::PoseStamped>
      target_pose_publisher;
  realtime_tools::RealtimePublisher<geometry_msgs::PoseStamped>
      initial_pose_publisher;
  realtime_tools::RealtimePublisher<geometry_msgs::TwistStamped>
      desired_velocity_world_publisher, desired_acceleration_world_publisher;
  geometry_msgs::Twist cartesian_velocity_msg, cartesian_acceleration_msg;
  geometry_msgs::Pose cartesian_pose_msg;

  // moveit_trajectory_interface::progress trajectory_progress;
  double time_increment = 0.001;
  bool control_frame_set = false;
  bool is_initialized = false;
  std::string planning_group;
  std::string robot_description_param_name;
  bool joints_are_unordered = false;

  double current_mpcresult_starttime;
  std::unique_ptr<lmpcpp::Horizon> current_horizon = nullptr;
  std::future<std::unique_ptr<lmpcpp::Horizon>> future_horizon;
  Eigen::VectorXd current_q;
  Eigen::VectorXd current_qd;
  Eigen::VectorXd current_qdd;
  pinocchio::SE3 current_pose;
  Eigen::VectorXd current_twist;
  Eigen::VectorXd current_twist_acc;
  double mpc_wreg1 = 1e-6;
  double mpc_wreg2 = 1e-9;

  unsigned int trajcount;
  unsigned int H, ndt;
  std::unique_ptr<lmpcpp::Planifier> mpcprob;
  double mpc_update_delay_ttraj;
  double mpc_update_next_ttraj;
  double u_scaling_ttraj_next;
  double u_scaling_ttraj_delay = 0.1;
  double u_scaling_update_increment = .1;
  double u_scaling_max = .1;
  double u_scaling_min = .1;
  int traj_number = 0;

  std::mutex pin_mutex;
  std::mutex current_horizon_mutex;
  unsigned int vel_scaling_iter = 0;
  double vel_scaling = 0.3;

  double filter_w0 = 200;
  double mpc_compute_time = 0.1;
  lmpcpp::LPFilter lptwist;

  inline static lmpcpp::Delay input_zeroaccmsg_delay = lmpcpp::Delay(1);
  lmpcpp::SolveParameters solveparams;
  bool stop_the_world = false;
  bool use_delta_reference = true;
  double _control_period = 1e-3;
};
