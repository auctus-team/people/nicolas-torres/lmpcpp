#include "mpc.hpp"
#include <Eigen/Sparse>

#include <Eigen/Dense>

#include <Eigen/src/Core/Matrix.h>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <sstream>

// #include <ruckig/ruckig.hpp>

using namespace lmpcpp;

// Eigen::MatrixXd Ginv(const Eigen::Vector3d &so3) {

//   auto theta = so3.norm();
//   auto w = so3 / theta;
//   auto wskew = pinocchio::skew(w);

//   return Eigen::MatrixXd::Identity(3, 3) / theta - wskew / 2 +
//          (1 / theta - (1 / tan(theta / 2)) / 2) * (wskew * wskew);
// }

const std::string Planifier::StateToString(const State s) {
  switch (s) {
  case State::OK:
    return std::string("State::OK");
  case State::Computing:
    return std::string("State::Computing");
  case State::Failed:
    return std::string("State::Failed");
  case State::NonInitialized:
    return std::string("State::NonInitialized");
  case State::Waiting:
    return std::string("State::Waiting");
  case State::Ending:
    return std::string("State::Ending");
  }
  return std::string("Unimplemented State: ") + std::to_string(s);
}

Planifier::Planifier(HorizonParameters params, std::string _solver)
    : params(params), mpcmat(params), _status{State::NonInitialized},
      _horizon(params) {
  solver = _solver;
  if (solver == "osqp") {
    qpprob.reset(new OSQPProblem(params.nz));
  } else if (solver == "qpoases") {
    qpprob.reset(new QPOasesProblem(params.nz));
  } else {
    std::stringstream ss;
    ss << "Error: solver '" << solver
       << "' unknown, using default: " << lmpcpp_default_solver << std::endl;
    ss << "valid values are: osqp, qpoases" << std::endl;
    print_error(ss);
    solver = lmpcpp_default_solver;
    qpprob.reset(new OSQPProblem(params.nz));
  }
  std::cout << "Planifier::solver:" << solver << std::endl;
  auto cost = mpcmat.cost_function();
  qpprob->update_cost_function(cost.a, cost.Hreg);
}

// Planifier::~Planifier() {
//   _status.store(State::Ending);
// }

Planifier::Planifier(Planifier const &other)
    : Planifier(other.params, other.solver) {}

Planifier::State Planifier::status() const { return _status.load(); }

c_int Planifier::test(Eigen::VectorXd x0 __attribute__((unused)),
                      Eigen::MatrixXd A __attribute__((unused))) {
  std::cout << "Planifier::test" << __LINE__ << ":" << __PRETTY_FUNCTION__
            << std::endl;
  return 0;
}

void Planifier::test2(Eigen::VectorXd x0, Eigen::MatrixXd A) {
  std::cout << "Planifier::test2" << __LINE__ << ":" << __PRETTY_FUNCTION__
            << std::endl;
  std::cout << "x0:" << std::endl << x0 << std::endl;
  std::cout << "A:" << std::endl << A << std::endl;
}

std::future<UniqueHorizon> Planifier::solve_async(const Goal goal) {
  std::promise<UniqueHorizon> promise;
  auto future = promise.get_future();
  std::thread(&Planifier::_solve, this, std::move(promise), goal).detach();

  return future;
}

std::future<UniqueHorizon>
Planifier::solve_async(const Goal goal, const SolveParameters solveparams) {
  update_solve_parameters(solveparams);
  return solve_async(goal);
}

Horizon Planifier::solve(const Goal goal, const SolveParameters solveparams) {
  update_solve_parameters(solveparams);
  // return Horizon(*solve_async(goal).get());
  Horizon horizon(*(solve_async(goal).get()).get());
  std::cout << "horizon(*solve_async(goal).get().get())" << std::endl;
  return horizon;
}

void Planifier::_solve(std::promise<UniqueHorizon> &&promise, const Goal goal) {

  TickTock temp("complete Planifier::setup_and_solve_mpc");

  _status.store(State::Computing);

  auto xx0exp = pinocchio::SE3(goal.start.pose);
  auto xxfexp = pinocchio::SE3(goal.pose);

  // auto xx0exp = pinocchio::exp6(xx0);
  // auto xxfexp = pinocchio::exp6(xxf);
  pinocchio::SE3 ref_pose;
  // ref_pose =
  //     pinocchio::SE3(Eigen::Matrix3d::Identity(),
  //     Eigen::Vector3d::Zero());
  // ref_pose = xx0exp; // this yields x0 = 0
  ref_pose = xxfexp; // this yields xf = 0
  // ref_pose = pinocchio::SE3::Interpolate(xx0exp, xxfexp, 0.5);
  // ref_pose = pinocchio::SE3(xx0exp.rotation(), Eigen::Vector3d::Zero());
  // ref_pose = pinocchio::SE3(xx0exp.rotation() * epsR,
  // Eigen::Vector3d::Zero());

  auto x0 = pinocchio::log6(ref_pose.actInv(xx0exp)).toVector();
  // auto x0 =
  //     closest_log6(ref_pose.actInv(xx0exp), ref_pose.actInv(xxfexp))
  //         .toVector();
  auto xf = pinocchio::log6(ref_pose.actInv(xxfexp)).toVector();
  // auto xf =
  //     closest_log6(ref_pose.actInv(xxfexp), ref_pose.actInv(xx0exp))
  //         .toVector();

  _mutex.lock();
  mpcmat.update_initial_u(goal.u0);
  mpcmat.update_initial_ud(goal.ud0);
  if (_use_origin_dlog) {
    // mpcmat.update_linear_horizon_matrices(x0, xf);
    mpcmat.update_linear_horizon_matrices(x0, xf, _horizon.result());
  } else {
    // mpcmat.update_linear_horizon_matrices(pinocchio::log6(xx0exp).toVector(),
    // xf);
    mpcmat.update_linear_horizon_matrices(pinocchio::log6(xx0exp).toVector(),
                                          xf, _horizon.result());
  }
  mpcmat.update_initial_state(x0);
  mpcmat.update_cost_b(x0, xf);
  // mpcmat.update_extra_linear_constraints(polytope_linconstraints);
  // mpcmat.update_extra_linear_constraints(obstacle_linconstraints);

  CostFunctionLS cost = mpcmat.cost_function();
  LinearInequalityConstraints linear_constraints = mpcmat.linear_constraints();
  _mutex.unlock();

  // -------------------------------------------------------------------------------------
  // ruckig
  // Create instances: the Ruckig OTG as well as input and output parameters
  // ruckig::Ruckig<6> otg{params.dt}; // control cycle
  // ruckig::InputParameter<6> input;
  // ruckig::OutputParameter<6> output;

  // // Set input parameters
  // for (unsigned int i = 0; i < 6; ++i) {
  //   input.current_position[i] = 0.0;
  //   input.current_velocity[i] = goal.u0[i];
  //   input.current_acceleration[i] = goal.ud0[i];

  //   input.target_position[i] = xf[i];
  //   input.target_velocity[i] = 0.0;
  //   input.target_acceleration[i] = 0.0;

  //   input.max_velocity[i] = mpcmat.u_max().max[i];
  //   input.max_acceleration[i] = mpcmat.ud_max().max[i];
  //   input.max_jerk[i] = mpcmat.udd_max().max[i];
  // }

  // Eigen::VectorXd result;
  // result.resize(params.nz);
  // result.setZero();
  // ruckig::Result state;
  // bool error = false;
  // for (unsigned int k = 0; k < params.H; ++k) {
  //   for (unsigned int i = 0; i < 6; ++i) {
  //     result(k * params.nx + i) = output.new_position[i];
  //     result(params.nx_hat + k * params.nu + i) = output.new_velocity[i];
  //   }

  //   for (double t = k * params.hdt; t < (k + 1) * params.hdt; t += params.dt)
  //   {
  //     state = otg.update(input, output);
  //     if ((state != ruckig::Finished) && (state != ruckig::Working)) {
  //       std::stringstream ss;
  //       ss << "Error: ruckig FAILED" << std::endl;
  //       ss << "Error: ruckig FAILED" << std::endl;
  //       ss << "Error: ruckig FAILED" << std::endl;
  //       ss << "Error: ruckig FAILED" << std::endl;
  //       ss << "ruckig::Finished: " << ruckig::Finished << std::endl;
  //       ss << "ruckig::Working: " << ruckig::Working << std::endl;
  //       ss << "state: " << state << std::endl;
  //       ss << "k: " << k << std::endl;
  //       ss << "t: " << t << std::endl;
  //       print_error(ss);
  //       error = true;
  //       break;
  //     }
  //     output.pass_to_input(input);
  //   }
  //   if (error) {
  //     break;
  //   }
  // }

  // if (!error) {
  //   _horizon.update(result, goal, ref_pose, temp.tock_s());
  //   promise.set_value(std::move(std::make_unique<Horizon>(_horizon)));
  // }

  // -------------------------------------------------------------------------------------
  if (!(bool)qpprob->solve(cost.b, linear_constraints.A,
                           linear_constraints.b.min,
                           linear_constraints.b.max)) {
    _horizon.update(qpprob->result(), goal, ref_pose, temp.tock_s());
    promise.set_value(std::move(std::make_unique<Horizon>(_horizon)));
  } else {
    std::stringstream ss;
    ss << "Error: mpc::qpprob->solve() FAILED" << std::endl;
    ss << "Error: mpc::qpprob->solve() FAILED" << std::endl;
    ss << "Error: mpc::qpprob->solve() FAILED" << std::endl;
    print_error(ss);
  }
  // -------------------------------------------------------------------------------------

  // ROS_INFO("MPC results ready");
  // ROS_INFO("Save MPC results...");
  // {
  //   std::string filename("lmpcv" + std::to_string(traj_number));
  //   current_mpcresult->dump_result(filename);
  //   current_mpcresult->dump_pose_reference(filename);
  // }

  _status.store(State::Waiting);
}

// **************************************************
// redirecting calls
void Planifier::update_cost_ponderation(const CostPonderation ponderation) {
  mpcmat.update_cost_ponderation(ponderation);
}

void Planifier::update_solve_parameters(const SolveParameters solveparams) {
  mpcmat.update_u_limits(solveparams.u);
  mpcmat.update_ud_limits(solveparams.ud);
  mpcmat.update_udd_limits(solveparams.udd);
  mpcmat.update_u_scaling(solveparams.u_scaling_factor);
  mpcmat.update_ud_scaling(solveparams.ud_scaling_factor);
  mpcmat.update_udd_scaling(solveparams.udd_scaling_factor);
}

void Planifier::set_compute_time(double tt) { qpprob->set_compute_time(tt); }

void Planifier::set_use_origin_dlog(bool val) { _use_origin_dlog = val; }
bool Planifier::use_origin_dlog() const { return _use_origin_dlog; }

double Planifier::compute_time_limit() const {
  return qpprob->compute_time_limit();
}

void Planifier::print_matrices() const { mpcmat.print_matrices(); }

VectorLimits Planifier::u_max() const { return mpcmat.u_max(); }
VectorLimits Planifier::ud_max() const { return mpcmat.ud_max(); }
VectorLimits Planifier::udd_max() const { return mpcmat.udd_max(); }
void Planifier::set_wreg1(double w) {
  mpcmat.update_wreg1(w);
  auto cost = mpcmat.cost_function();
  qpprob->update_cost_function(cost.a, cost.Hreg);
}

void Planifier::set_wreg2(double w) {
  mpcmat.update_wreg2(w);
  auto cost = mpcmat.cost_function();
  qpprob->update_cost_function(cost.a, cost.Hreg);
}

double Planifier::t_horizon(double t) const { return _horizon.t_horizon(t); }

double Planifier::get_wreg1() const { return mpcmat.wreg1(); }
double Planifier::get_wreg2() const { return mpcmat.wreg2(); }

double Planifier::horizon_duration() const {
  return params.state_horizon_duration();
}

double Planifier::input_horizon_duration() const {
  return params.input_horizon_duration();
}

// void Planifier::update_cartacc_polytope(const Eigen::VectorXd current_qd,
//                                         const double polytope_horizon,
//                                         const Eigen::MatrixXd J) {

//   Eigen::VectorXd qddlim(7);
//   qddlim << 15, 7.5, 10, 12.5, 15, 20, 20;

//   // VectorLimits compressed_qdlims =
//   //     lmpcpp::qdmax_from_qqlims(qlims, qdlims, current_q,
//   // polytope_horizon);
//   LinearInequalityConstraints poly_ineq =
//       lmpcpp::compute_Jqd_halfplane({-qddlim, qddlim}, J, params.nu);
//   assert(poly_ineq.A.cols() == params.nu);

//   Eigen::MatrixXd Alin_single(
//       Eigen::MatrixXd::Zero(poly_ineq.A.rows(), params.nz));
//   Alin_single.block(0, params.nx_hat, poly_ineq.A.rows(),
//   poly_ineq.A.cols())
//   =
//       poly_ineq.A;
//   poly_ineq.A = Alin_single;

//   Eigen::MatrixXd Alin_all(
//       Eigen::MatrixXd::Zero(Alin_single.rows() * (params.H - 1),
//       params.nz));
//   Eigen::VectorXd b_min_all(
//       Eigen::VectorXd::Zero(Alin_single.rows() * (params.H - 1)));
//   Eigen::VectorXd b_max_all(b_min_all);

//   for (unsigned int n = 0; n < params.H - 1; ++n) {
//     Alin_all.block(n * Alin_single.rows(), 0, Alin_single.rows(),
//     params.nz)
//         << block_rcircshift(Alin_single, n * params.nu);

//     b_max_all.segment(n * poly_ineq.b.max.rows(), poly_ineq.b.max.rows()) =
//         poly_ineq.b.max;
//     b_min_all.segment(n * poly_ineq.b.min.rows(), poly_ineq.b.min.rows()) =
//         poly_ineq.b.min;
//   }
//   polytope_linconstraints.A = Alin_all;
//   polytope_linconstraints.b.max = b_max_all;
//   polytope_linconstraints.b.min = b_min_all;
// }

void Planifier::update_cartvel_polytope(const VectorLimits qlims,
                                        const VectorLimits qdlims,
                                        const Eigen::VectorXd current_q,
                                        const double polytope_horizon,
                                        const Eigen::MatrixXd J) {
  VectorLimits compressed_qdlims =
      lmpcpp::qdmax_from_qqlims(qlims, qdlims, current_q, polytope_horizon);
  LinearInequalityConstraints poly_ineq =
      lmpcpp::compute_Jqd_halfplane(compressed_qdlims, J, params.nu);
  assert(poly_ineq.A.cols() == params.nu);

  Eigen::MatrixXd Alin_single(
      Eigen::MatrixXd::Zero(poly_ineq.A.rows(), params.nz));
  Alin_single.block(0, params.nx_hat, poly_ineq.A.rows(), poly_ineq.A.cols()) =
      poly_ineq.A;
  poly_ineq.A = Alin_single;

  // polytope_linconstraints = poly_ineq;

  Eigen::MatrixXd Alin_all(
      Eigen::MatrixXd::Zero(Alin_single.rows() * (params.H - 1), params.nz));
  Eigen::VectorXd b_min_all(
      Eigen::VectorXd::Zero(Alin_single.rows() * (params.H - 1)));
  Eigen::VectorXd b_max_all(b_min_all);

  // print_size("Alin_all:", Alin_all);
  // print_size("Alin_single:", Alin_single);
  // {
  //   unsigned int n = 0;
  //   print_size("block_rcircshift(Alin_single, n * params.nu):",
  //              block_rcircshift(Alin_single, n * params.nu));
  //   print_size("Alin_all.block(n * Alin_single.rows(), 0,
  //   Alin_single.rows(),
  //   "
  //              "params.nz):",
  //              Alin_all.block(n * Alin_single.rows(), 0,
  //              Alin_single.rows(),
  //                             params.nz));

  //   print_size("poly_ineq.b.max:", poly_ineq.b.max);
  //   print_size("b_max_all:", b_max_all);
  //   print_size("b_max_all.segment(n * params.nu + poly_ineq.b.max.rows(), "
  //              "poly_ineq.b.max.rows()):",
  //              b_max_all.segment(n * params.nu + poly_ineq.b.max.rows(),
  //                                poly_ineq.b.max.rows()));
  // }
  for (unsigned int n = 0; n < params.H - 1; ++n) {
    Alin_all.block(n * Alin_single.rows(), 0, Alin_single.rows(), params.nz)
        << block_rcircshift(Alin_single, n * params.nu);

    b_max_all.segment(n * poly_ineq.b.max.rows(), poly_ineq.b.max.rows()) =
        poly_ineq.b.max;
    b_min_all.segment(n * poly_ineq.b.min.rows(), poly_ineq.b.min.rows()) =
        poly_ineq.b.min;
  }
  polytope_linconstraints.A = Alin_all;
  polytope_linconstraints.b.max = b_max_all;
  polytope_linconstraints.b.min = b_min_all;
}

void Planifier::update_obstacle(const pinocchio::SE3 obs_pose, const double r) {
  LinearInequalityConstraints obs_ineq =
      lmpcpp::mindistance_obstacle(obs_pose.toHomogeneousMatrix(), r);

  Eigen::MatrixXd Alin_single(
      Eigen::MatrixXd::Zero(obs_ineq.A.rows(), params.nz));
  Alin_single.block(0, 0, obs_ineq.A.rows(), obs_ineq.A.cols()) = obs_ineq.A;

  // NOTE: (params.H - 1) because it's for x_overline
  Eigen::MatrixXd Alin_all(
      Eigen::MatrixXd::Zero(Alin_single.rows() * params.H, params.nz));
  Eigen::VectorXd b_min_all(
      Eigen::VectorXd::Zero(Alin_single.rows() * params.H));
  Eigen::VectorXd b_max_all(b_min_all);

  // std::cout << "params.nx_hat:" << params.nx_hat << std::endl;
  // std::cout << "params.H:" << params.H << std::endl;
  // std::cout << "params.H*nx:" << params.H * params.nx << std::endl;
  for (unsigned int n = 1; n <= params.H; ++n) {
    // std::cout << "n:" << n << "  n*params.nx:" << n * params.nx << std::endl;
    Alin_all.block(n * Alin_single.rows(), 0, Alin_single.rows(), params.nz) =
        block_rcircshift(Alin_single, n * params.nx);

    b_max_all.segment(n * obs_ineq.b.max.rows(), obs_ineq.b.max.rows()) =
        obs_ineq.b.max;
    b_min_all.segment(n * obs_ineq.b.min.rows(), obs_ineq.b.min.rows()) =
        obs_ineq.b.min;
  }
  obstacle_linconstraints.A = Alin_all;
  obstacle_linconstraints.b.max = b_max_all;
  obstacle_linconstraints.b.min = b_min_all;
}
