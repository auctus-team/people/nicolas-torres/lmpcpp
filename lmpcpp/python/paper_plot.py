import numpy as np
import pinocchio as pin
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
from matplotlib.patches import Patch
import aux
import os
import sys

import tikzplotlib
plt.style.use("ggplot")

import matplotlib as mpl
mpl.rcParams['font.family'] = 'Avenir'
# mpl.rcParams['text.usetex'] = True
mpl.rcParams['font.size'] = 60
mpl.rcParams['axes.linewidth'] = 2
mpl.rcParams['lines.markersize'] = 5
# mpl.rcParams['text.latex.preamble'] = [
#     r'\usepackage{amsmath}', r'\usepackage{amssymb}'
# ]
mpl.rcParams['font.family'] = 'serif'
mpl.rcParams['font.serif'] = 'Computer Modern'


def load_hdf(path):
    path = path + '.hdf'
    if os.path.exists(path):
        print('load {}...'.format(path))
        return pd.read_hdf(path)
    return None


class Axes:
    axes = []

    @classmethod
    def add(cls, ax):
        cls.axes.append(ax)

    @classmethod
    def grid(cls):
        for ax in cls.axes:
            ax.grid()


def setup_ax(ax, botvisible=True):
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.spines['bottom'].set_visible(botvisible)
    # ax.grid(b=True)
    # ax.grid()
    Axes.add(ax)
    return ax


# fname = 'export2'
fname = 'export4'
full_csv = load_hdf(fname)
if full_csv is None:
    full_csv = pd.read_csv(fname + '.csv')
    full_csv.to_hdf(fname + '.hdf', fname)
# full_csv.to_pickle('export2.pkl')
# full_csv=pd.read_pickle('export2.hdf')

topics = {
    # error
    # '/panda/velocity_control/err_pose_velqp/pose/position/x',
    #            '/panda/velocity_control/err_pose_velqp/pose/position/y',
    #            '/panda/velocity_control/err_pose_velqp/pose/position/z'
    # position
    '/lmpcpp_moveit/target_pose/pose/position/x':
    'x',
    '/lmpcpp_moveit/target_pose/pose/position/y':
    'y',
    '/lmpcpp_moveit/target_pose/pose/position/z':
    'z',
    '/panda/velocity_control/current_pose_velqp/pose/position/x':
    '$x^*$',
    '/panda/velocity_control/current_pose_velqp/pose/position/y':
    '$y^*$',
    '/panda/velocity_control/current_pose_velqp/pose/position/z':
    '$z^*$',
    '/lmpcpp_moveit/desired_pose/pose/position/x':
    aux.deriv_title('x', '', supind='des'),
    '/lmpcpp_moveit/desired_pose/pose/position/y':
    aux.deriv_title('y', '', supind='des'),
    '/lmpcpp_moveit/desired_pose/pose/position/z':
    aux.deriv_title('z', '', supind='des'),
    '/lmpcpp_moveit/current_pose_se3/twist/angular/x':
    '$\phi_x$',
    '/lmpcpp_moveit/current_pose_se3/twist/angular/y':
    '$\phi_y$',
    '/lmpcpp_moveit/current_pose_se3/twist/angular/z':
    '$\phi_z$',
    '/lmpcpp_moveit/desired_pose_se3/twist/angular/x':
    '$\phi_x^{des}$',
    '/lmpcpp_moveit/desired_pose_se3/twist/angular/y':
    '$\phi_y^{des}$',
    '/lmpcpp_moveit/desired_pose_se3/twist/angular/z':
    '$\phi_z^{des}$',
    '/lmpcpp_moveit/target_pose_se3/twist/angular/x':
    '$\phi_x^*$',
    '/lmpcpp_moveit/target_pose_se3/twist/angular/y':
    '$\phi_y^*$',
    '/lmpcpp_moveit/target_pose_se3/twist/angular/z':
    '$\phi_z^*$',
    # velocity
    '/lmpcpp_moveit/current_velocity_se3/twist/linear/x':
    aux.deriv_title('x', 'd'),
    '/lmpcpp_moveit/current_velocity_se3/twist/linear/y':
    aux.deriv_title('y', 'd'),
    '/lmpcpp_moveit/current_velocity_se3/twist/linear/z':
    aux.deriv_title('z', 'd'),
    '/lmpcpp_moveit/desired_velocity_se3/twist/linear/x':
    aux.deriv_title('x', 'd', supind='des'),
    '/lmpcpp_moveit/desired_velocity_se3/twist/linear/y':
    aux.deriv_title('y', 'd', supind='des'),
    '/lmpcpp_moveit/desired_velocity_se3/twist/linear/z':
    aux.deriv_title('z', 'd', supind='des'),
    '/lmpcpp_moveit/current_velocity_se3/twist/angular/x':
    aux.deriv_title('\phi_x', 'd'),
    '/lmpcpp_moveit/current_velocity_se3/twist/angular/y':
    aux.deriv_title('\phi_y', 'd'),
    '/lmpcpp_moveit/current_velocity_se3/twist/angular/z':
    aux.deriv_title('\phi_z', 'd'),
    '/lmpcpp_moveit/desired_velocity_se3/twist/angular/x':
    aux.deriv_title('\phi_x', 'd', supind='des'),
    '/lmpcpp_moveit/desired_velocity_se3/twist/angular/y':
    aux.deriv_title('\phi_y', 'd', supind='des'),
    '/lmpcpp_moveit/desired_velocity_se3/twist/angular/z':
    aux.deriv_title('\phi_z', 'd', supind='des'),
    '/lmpcpp_moveit/max_velocity_se3/twist/angular/x':
    '',
    '/lmpcpp_moveit/max_velocity_se3/twist/angular/y':
    '',
    '/lmpcpp_moveit/max_velocity_se3/twist/angular/z':
    '',
    '/lmpcpp_moveit/max_velocity_se3/twist/linear/x':
    '',
    '/lmpcpp_moveit/max_velocity_se3/twist/linear/y':
    '',
    '/lmpcpp_moveit/max_velocity_se3/twist/linear/z':
    '',
    '/lmpcpp_moveit/min_velocity_se3/twist/angular/x':
    '',
    '/lmpcpp_moveit/min_velocity_se3/twist/angular/y':
    '',
    '/lmpcpp_moveit/min_velocity_se3/twist/angular/z':
    '',
    '/lmpcpp_moveit/min_velocity_se3/twist/linear/x':
    '',
    '/lmpcpp_moveit/min_velocity_se3/twist/linear/y':
    '',
    '/lmpcpp_moveit/min_velocity_se3/twist/linear/z':
    '',
    '/lmpcpp_moveit/current_acceleration_se3/twist/angular/x':
    '',
    '/lmpcpp_moveit/current_acceleration_se3/twist/angular/y':
    '',
    '/lmpcpp_moveit/current_acceleration_se3/twist/angular/z':
    '',
    '/lmpcpp_moveit/current_acceleration_se3/twist/linear/x':
    '',
    '/lmpcpp_moveit/current_acceleration_se3/twist/linear/y':
    '',
    '/lmpcpp_moveit/current_acceleration_se3/twist/linear/z':
    '',
    '/lmpcpp_moveit/desired_acceleration_se3/twist/angular/x':
    '',
    '/lmpcpp_moveit/desired_acceleration_se3/twist/angular/y':
    '',
    '/lmpcpp_moveit/desired_acceleration_se3/twist/angular/z':
    '',
    '/lmpcpp_moveit/desired_acceleration_se3/twist/linear/x':
    '',
    '/lmpcpp_moveit/desired_acceleration_se3/twist/linear/y':
    '',
    '/lmpcpp_moveit/desired_acceleration_se3/twist/linear/z':
    '',
    '/lmpcpp_moveit/error_target_pose_se3/twist/linear/x':
    '',
    '/lmpcpp_moveit/error_target_pose_se3/twist/linear/y':
    '',
    '/lmpcpp_moveit/error_target_pose_se3/twist/linear/z':
    '',
    '/lmpcpp_moveit/error_target_pose_se3/twist/angular/x':
    '',
    '/lmpcpp_moveit/error_target_pose_se3/twist/angular/y':
    '',
    '/lmpcpp_moveit/error_target_pose_se3/twist/angular/z':
    '',
}

fname = fname + '_df_interpolated'
df = load_hdf(fname)
# df  = None
if df is None:
    df = full_csv[topics.keys()].copy()
    print('columns:')
    for i in full_csv.columns:
        print('-', i)
    df['datetime'] = full_csv['__time'].copy()
    df['datetime'] = pd.to_datetime(df.loc[:, 'datetime'], unit='s')
    df['time'] = (df['datetime'] - df['datetime'][0]).dt.total_seconds()
    df.set_index('time', inplace=True)
    df.interpolate(inplace=True)
    # df.rename(topics, inplace=True, errors='raise')
    df.to_hdf(fname + '.hdf', fname)

df = df[df.index > 1.7]
df = df[df.index < 14.3]
df = df.loc[df.index[::1000]]
df.index = (df.index - df.index[0])
df.interpolate(inplace=True)

# single_col = False
single_col = True
show_desired = False

if single_col:
    rows = 4
    # rows = 8
    columns = 1
else:
    rows = 2
    columns = 3

# figsize = (18, 8)
fig1, axes1 = plt.subplots(
    rows,
    columns,
    num=0,
    # sharex='col',
    sharex='all',
    sharey='row',
    gridspec_kw={
        # 'hspace': 0.18,
        # 'hspace': 0.05,
        'hspace': 0.05,
        'wspace': 0
    },
    # figsize=(8, 4),
    squeeze=True,
    # figsize=figsize,
)

# fig2, axes2 = fig1, axes1[4:]
# axes1 = axes1[:4]
fig2, axes2 = fig1, axes1[2:]
axes1 = axes1[:2]

# fig2, axes2 = plt.subplots(
#     rows,
#     columns,
#     num=1,
#     # sharex='col',
#     sharex='all',
#     sharey='row',
#     gridspec_kw={
#         # 'hspace': 0.18,
#         'hspace': 0.05,
#         'wspace': 0
#     },
#     squeeze=True,
#     figsize=figsize,
# )

if single_col == 1:
    axes1 = axes1[None].T
    axes2 = axes2[None].T

rotation = 0
coords_color = ['firebrick', 'forestgreen', 'steelblue']
coords = ['x', 'y', 'z']
legends = []
legend_labels = []
zero_y = [0, 0]
zero_t = [df.index[0], df.index[-1]]
for icoord in np.arange(3):
    coord = coords[icoord]
    coord_color = coords_color[icoord]
    print('icoord:', icoord, 'coord:', coord)
    col = icoord
    if single_col:
        col = 0

    lw = 5
    lw_des = 2.5 * lw
    max_color = 'brown'
    max_alpha = 0.2
    des_alpha = 0.5

    def plot_des(df, ax, color):
        if show_desired:
            df.plot(ax=ax, alpha=des_alpha, color=color, linewidth=lw_des)

    def plot_zero(ax):
        ax.plot(zero_t,
                zero_y,
                linewidth=0.8 * lw,
                color='k',
                linestyle='dashed',
                alpha=0.5)

    ###################################################################################
    # position
    row = 0
    if single_col:
        # row = icoord
        row = 0
    ax = setup_ax(axes1[row, col], botvisible=False)
    # shay = ax.get_shared_y_axes()
    # shay.remove(ax)
    # topic = '/lmpcpp_moveit/target_pose/pose/position/'
    # df[topic + coord].plot(ax=ax, linestyle='dashed', color='k', linewidth=lw)
    # topic = '/panda/velocity_control/current_pose_velqp/pose/position/'
    # df[topic + coord].plot(ax=ax, linewidth=lw, color=coord_color)

    plot_zero(ax)
    topic = '/lmpcpp_moveit/error_target_pose_se3/twist/linear/'
    df[topic + coord].plot(ax=ax, linewidth=lw, color=coord_color)

    topic = '/lmpcpp_moveit/desired_pose/pose/position/'
    plot_des(df[topic + coord], ax, color=coord_color)

    varname = aux.deriv_title('e_{{{}}} '.format(coord))
    if single_col:
        # ax.set_ylabel(varname + ' ' + aux.deriv_unit('m', ''))
        # ax.set_ylabel('e ' + aux.deriv_unit('m', ''), rotation=rotation)
        ax.set_ylabel('e', rotation=rotation)
        # ax.set_ylabel('e', rotation=rotation)
        # ax2 = setup_ax(ax.twinx(), botvisible=False)
        # ax2.set_ylabel(aux.deriv_unit('m', ''), rotation=rotation)
        # ax2.set_yticks([])
    else:
        ax.set_ylabel('Position ' + aux.deriv_unit('m', ''))
        ax.set_title(varname)

    # velocity
    row = 2
    if single_col:
        # row = 3
        row = 1
    ax = setup_ax(axes1[row, col], botvisible=False)
    topic = '/lmpcpp_moveit/current_velocity_se3/twist/linear/'
    df[topic + coord].plot(ax=ax, linewidth=lw, color=coord_color)
    topic = '/lmpcpp_moveit/desired_velocity_se3/twist/linear/'
    plot_des(df[topic + coord], ax, color=coord_color)

    if icoord == 0:
        topic = '/lmpcpp_moveit/max_velocity_se3/twist/linear/'
        ax.fill_between(df.index,
                        df[topic + coord],
                        color=max_color,
                        alpha=max_alpha)
        topic = '/lmpcpp_moveit/min_velocity_se3/twist/linear/'
        ax.fill_between(df.index,
                        df[topic + coord],
                        color=max_color,
                        alpha=max_alpha)
    if col == 0:
        # ax.set_ylabel('Velocity ' + aux.deriv_unit('m', 'd'))
        ax.set_ylabel('$v$ ' + aux.deriv_unit('m', 'd'), rotation=rotation)

    # acceleration
    if rows == 3:
        row += 1
        ax = setup_ax(axes1[row, col], botvisible=True)
        topic = '/lmpcpp_moveit/current_acceleration_se3/twist/linear/'
        df[topic + coord].plot(ax=ax, linewidth=lw, color=coord_color)
        topic = '/lmpcpp_moveit/desired_acceleration_se3/twist/linear/'
        plot_des(df[topic + coord], ax, color=coord_color)
        if col == 0:
            ax.set_ylabel('Acceleration ' + aux.deriv_unit('m', 'dd'))
    # ax.set_xlabel('t [s]')
    # ax.set_xlabel('')
    ax.get_xaxis().get_label().set_visible(False)

    ####################################################################
    # orientation
    # ax.set_title(coord)
    row = 0
    if single_col:
        # row = icoord
        row = 0
    ax = setup_ax(axes2[row, col], botvisible=False)
    # topic = '/lmpcpp_moveit/target_pose_se3/twist/angular/'
    # df[topic + coord].plot(ax=ax, linestyle='dashed', color='k', linewidth=lw)
    # topic = '/lmpcpp_moveit/current_pose_se3/twist/angular/'
    # df[topic + coord].plot(ax=ax, linewidth=lw, color=coord_color)

    plot_zero(ax)
    topic = '/lmpcpp_moveit/error_target_pose_se3/twist/angular/'
    df[topic + coord].plot(ax=ax, linewidth=lw, color=coord_color)

    topic = '/lmpcpp_moveit/desired_pose_se3/twist/angular/'
    plot_des(df[topic + coord], ax, color=coord_color)
    varname = aux.deriv_title('\phi_{{{}}} '.format(coord))
    if single_col:
        # ax.set_ylabel(varname + aux.deriv_unit('rad', ''), rotation=rotation)
        ax.set_ylabel('$\phi$', rotation=rotation)
    else:
        ax.set_ylabel('Orientation ' + aux.deriv_unit('rad', ''))
        ax.set_title(varname)

    # velocity
    row = 2
    if single_col:
        row = 1
    ax = setup_ax(axes2[row, col], botvisible=False)
    topic = '/lmpcpp_moveit/current_velocity_se3/twist/angular/'
    df[topic + coord].plot(ax=ax, linewidth=lw, color=coord_color)
    topic = '/lmpcpp_moveit/desired_velocity_se3/twist/angular/'
    plot_des(df[topic + coord], ax, color=coord_color)
    if icoord == 0:
        topic = '/lmpcpp_moveit/max_velocity_se3/twist/angular/'
        ax.fill_between(df.index,
                        df[topic + coord],
                        color=max_color,
                        alpha=max_alpha)
        topic = '/lmpcpp_moveit/min_velocity_se3/twist/angular/'
        ax.fill_between(df.index,
                        df[topic + coord],
                        color=max_color,
                        alpha=max_alpha)
    if col == 0:
        # ax.set_ylabel('Velocity ' + aux.deriv_unit('rad', 'd'))
        ax.set_ylabel('$\omega$ ' + aux.deriv_unit('m', 'd'),
                      rotation=rotation)

    # acceleration
    if rows == 3:
        row += 1
        ax = setup_ax(axes2[2, col], botvisible=True)
        topic = '/lmpcpp_moveit/current_acceleration_se3/twist/angular/'
        df[topic + coord].plot(ax=ax, linewidth=lw, color=coord_color)
        topic = '/lmpcpp_moveit/desired_acceleration_se3/twist/angular/'
        plot_des(df[topic + coord], ax, color=coord_color)
        if col == 0:
            ax.set_ylabel('Acceleration ' + aux.deriv_unit('rad', 'dd'))
    ax.set_xlabel('t [s]')

    coord_des_label = aux.deriv_title(coord, supind='des'),
    if show_desired:
        legends.append(
            Line2D([0], [0],
                   color=coord_color,
                   lw=lw_des,
                   alpha=des_alpha,
                   label=coord_des_label))
    legends.append(Line2D([0], [0], color=coord_color, lw=lw_des, label=coord))

legends.append(
    Patch(facecolor=max_color,
          edgecolor=max_color,
          alpha=max_alpha,
          label='max velocity'))
# ax.legend(handles=legends,
#           loc='lower center',
#           bbox_to_anchor=(0.5, -.55),
#           fancybox=True,
#           shadow=True,
#           ncol=len(legends))

Axes.grid()

dpi = 400
width = 2560
# height = 1440
height = 10000
fig1.set_size_inches(width / dpi, height / dpi)

fig1.align_ylabels()
fig2.align_ylabels()

fpath = 'result.tex'
print('saving {}...'.format(fpath))
tikzplotlib.clean_figure()
tikzplotlib.save(fpath)

plt.show()

fpath = 'result.png'
print('saving {}...'.format(fpath))
fig1.savefig(
    fpath,
    dpi=dpi,
    bbox_inches='tight',
)

# fpath = 'result_linear.png'
# print('saving {}...'.format(fpath))
# fig1.savefig(
#     fpath,
#     dpi=dpi,
#     bbox_inches='tight',
# )

# fpath = 'result_angular.png'
# print('saving {}...'.format(fpath))
# fig2.savefig(
#     fpath,
#     dpi=dpi,
#     bbox_inches='tight',
# )

figl, axl = plt.subplots(1)
leg = axl.legend(handles=legends,
                 loc='lower center',
                 bbox_to_anchor=(0.5, -.55),
                 fancybox=True,
                 shadow=True,
                 ncol=len(legends))
figl.canvas.draw()
bbox = leg.get_window_extent().transformed(figl.dpi_scale_trans.inverted())
fpath = 'result_legend.png'
print('saving {}...'.format(fpath))
figl.savefig(
    fpath,
    dpi=dpi,
    bbox_inches=bbox,
)
