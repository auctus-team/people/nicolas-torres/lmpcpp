#pragma once
#include "methods.hpp"
#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <memory>
#include <osqp.h>
#include <qpOASES.hpp>

namespace lmpcpp {
// constexpr double compute_time_s = 0.050; // [s]
constexpr double compute_time_s = 1; // [s]

class QPProblem {
public:
  QPProblem() = delete;
  QPProblem(unsigned int nz);

  virtual double compute_time_limit();

  virtual void set_compute_time(double tt);

  virtual void update_cost_function(const Eigen::MatrixXd &a,
                                    const Eigen::MatrixXd &Hreg);

  virtual int solve(const Eigen::MatrixXd cost_a, const Eigen::MatrixXd Hreg,
                    const Eigen::MatrixXd cost_b, const Eigen::MatrixXd Alin,
                    Eigen::VectorXd Alin_min, Eigen::VectorXd Alin_max) = 0;
  virtual int solve(const Eigen::MatrixXd cost_b, const Eigen::MatrixXd Alin,
                    Eigen::VectorXd Alin_min, Eigen::VectorXd Alin_max) = 0;

  virtual Eigen::VectorXd result() = 0;

  void mark_initialized();

  const unsigned int nz;

protected:
  bool initialized = false;
  Eigen::MatrixXd _cost_a;
  Eigen::MatrixXd _Hreg;
  double compute_time = compute_time_s;
};

class OSQPProblem : public QPProblem {
public:
  OSQPProblem(unsigned int nz);

  void set_compute_time(double tt) final;

  void update_cost_function(const Eigen::MatrixXd &a,
                            const Eigen::MatrixXd &Hreg) final;

  int solve(const Eigen::MatrixXd cost_a, const Eigen::MatrixXd Hreg,
            const Eigen::MatrixXd cost_b, const Eigen::MatrixXd Alin,
            Eigen::VectorXd Alin_min, Eigen::VectorXd Alin_max) final;
  int solve(const Eigen::MatrixXd cost_b, const Eigen::MatrixXd Alin,
            Eigen::VectorXd Alin_min, Eigen::VectorXd Alin_max) final;

  Eigen::VectorXd result() final;

private:
  OSQPWorkspace *osqp_workspace;
  OSQPSettings osqp_settings;
  OSQPData osqp_data;

  std::unique_ptr<csc, decltype(&c_free)> P;
  Eigen::MatrixXd Q;
  Eigen::Matrix<c_int, Eigen::Dynamic, 1> P_row_ind;
  Eigen::Matrix<c_int, Eigen::Dynamic, 1> P_col_ind;
  Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> U;

  Eigen::SparseMatrix<double> Alin_sparse;
  Eigen::SparseMatrix<double> Q_sparse;
};

using MatrixXdRowmajor =
    Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>;

class QPOasesProblem : public QPProblem {
public:
  QPOasesProblem(unsigned int nz);

  void update_variable_bounds(VectorLimits varbounds);
  void update_cost_function(const Eigen::MatrixXd &a,
                            const Eigen::MatrixXd &Hreg) final;

  int solve(const Eigen::MatrixXd cost_a, const Eigen::MatrixXd Hreg,
            const Eigen::MatrixXd cost_b, const Eigen::MatrixXd Alin,
            Eigen::VectorXd Alin_min, Eigen::VectorXd Alin_max) final;
  int solve(const Eigen::MatrixXd cost_b, const Eigen::MatrixXd Alin,
            Eigen::VectorXd Alin_min, Eigen::VectorXd Alin_max) final;

  Eigen::VectorXd result() final;

private:
  int max_iterations = 1e6;
  MatrixXdRowmajor H;
  VectorLimits bounds;
  std::unique_ptr<qpOASES::SQProblem> solver;
  qpOASES::Options options;
};

} // namespace lmpcpp
