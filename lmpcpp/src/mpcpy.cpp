#include <Eigen/Core>
#include <Eigen/Dense>
#include <chrono>
#include <tuple>
#include <vector>
// #include <iostream>

#include <pybind11/eigen.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include "horizon.hpp"
#include "methods.hpp"
#include "mpc.hpp"
#include <pinocchio/spatial/explog.hpp>
#include <pinocchio/spatial/se3.hpp>
#include <pinocchio/spatial/skew.hpp>

#define STRINGIFY(x) #x
#define MACRO_STRINGIFY(x) STRINGIFY(x)

using Vector6d = Eigen::Matrix<double, 6, 1>;
using RefVector6d = const Eigen::Ref<Vector6d>;

struct ResTime {
  ResTime() : result(Vector6d::Zero()) {}
  void tick() { startt = std::chrono::steady_clock::now(); }
  void tock() {
    endd = std::chrono::steady_clock::now();
    total_ns =
        std::chrono::duration_cast<std::chrono::nanoseconds>(endd - startt)
            .count();
  }

  double total_ns;
  std::chrono::time_point<std::chrono::steady_clock> startt;
  std::chrono::time_point<std::chrono::steady_clock> endd;
  Vector6d result;
};

ResTime test_vec(std::vector<Vector6d> vec) {

  ResTime restime;
  restime.tick();
  for (const auto &elem : vec) {
    restime.result += elem;
  }
  restime.tock();
  return restime;
};

namespace py = pybind11;
using namespace lmpcpp;

PYBIND11_MODULE(pylmpcpp, m) {
  // ResTime
  py::class_<ResTime>(m, "ResTime")
      .def(py::init<>())
      .def_readonly("total_ns", &ResTime::total_ns)
      .def_readwrite("result", &ResTime::result);

  // HorizonParameters
  py::class_<HorizonParameters>(m, "HorizonParameters")
      .def(py::init<unsigned int, unsigned int, double, unsigned int,
                    unsigned int, unsigned int>(),
           py::arg("H"), py::arg("ndt") = 15, py::arg("dt") = double(1e-3),
           py::arg("nx") = 6, py::arg("nu") = 6, py::arg("ny") = 6)
      .def(py::init<HorizonParameters>(), py::arg("HorizonParameters"))
      .def("state_horizon_duration", &HorizonParameters::state_horizon_duration)
      .def("input_horizon_duration", &HorizonParameters::input_horizon_duration)
      .def("__repr__", &HorizonParameters::str)
      // TODO fix repr (should return string)
      // .def("__repr__", &HorizonParameters::str)
      // .def("__repr__",
      //      [](const HorizonParameters &params) { return params.str(); })
      // .def("__str__", &HorizonParameters::str)
      .def_readonly("H", &HorizonParameters::H)
      .def_readonly("nx", &HorizonParameters::nx)
      .def_readonly("nu", &HorizonParameters::nu)
      .def_readonly("ny", &HorizonParameters::ny)
      .def_readonly("ndt", &HorizonParameters::ndt)
      .def_readonly("dt", &HorizonParameters::dt)
      .def_readonly("nx_underline", &HorizonParameters::nx_underline)
      .def_readonly("nx_overline", &HorizonParameters::nx_overline)
      .def_readonly("nx_hat", &HorizonParameters::nx_hat)
      .def_readonly("nu_underline", &HorizonParameters::nu_underline)
      .def_readonly("nud_all", &HorizonParameters::nud_all)
      .def_readonly("nudd_all", &HorizonParameters::nudd_all)
      .def_readonly("nz", &HorizonParameters::nz)
      .def_readonly("hdt", &HorizonParameters::hdt);

  // Horizon
  py::class_<Horizon>(m, "Horizon")
      .def(py::init<HorizonParameters>(), py::arg("HorizonParameters"))
      .def(py::init<Horizon>(), py::arg("Horizon"))
      .def("update", &Horizon::update, py::arg("result"), py::arg("goal"),
           py::arg("pullpose"), py::arg("solve_time"))
      .def("t_horizon", &Horizon::t_horizon, py::arg("t"))
      .def("solve_time", &Horizon::solve_time)
      .def("result", &Horizon::result)
      .def("x", py::overload_cast<double>(&Horizon::x, py::const_),
           py::arg("t"))
      .def("pose", py::overload_cast<double>(&Horizon::pose, py::const_),
           py::arg("t"))
      .def("twist", &Horizon::twist, py::arg("t"))
      .def("u", &Horizon::u, py::arg("t"))
      .def("u0", &Horizon::u0)
      .def("ud0", &Horizon::ud0)
      .def("t0", &Horizon::t0)
      .def("sample_horizon", &Horizon::sample_horizon, py::arg("samples") = 10)
      .def("dump_result", &Horizon::dump_result, py::arg("filepath"))
      .def("dump_pose_reference", &Horizon::dump_pose_reference,
           py::arg("filepath") = 10)
      .def_readonly("params", &Horizon::params);

  // VectorLimits
  py::class_<VectorLimits>(m, "VectorLimits")
      .def(py::init())
      .def(py::init<Eigen::VectorXd, Eigen::VectorXd>(), py::arg("min"),
           py::arg("max"))
      .def_readwrite("min", &VectorLimits::min)
      .def_readwrite("max", &VectorLimits::max);

  // CostPonderation
  py::class_<CostPonderation>(m, "CostPonderation")
      .def(py::init())
      .def(py::init<double, double, double>(), py::arg("linear_weight"),
           py::arg("angular_weight"), py::arg("terminal_weight_factor"))
      .def_static("build_vector", &CostPonderation::build_vector,
                  py::arg("linear_weight"), py::arg("angular_weight"))
      .def_readonly("linear_weight", &CostPonderation::linear_weight)
      .def_readonly("angular_weight", &CostPonderation::angular_weight)
      .def_readonly("terminal_weight_factor",
                    &CostPonderation::terminal_weight_factor)
      .def_readonly("vec", &CostPonderation::vec);

  // CostFunctionLS
  py::class_<CostFunctionLS>(m, "CostFunctionLS")
      .def(py::init())
      .def(py::init<Eigen::MatrixXd, Eigen::VectorXd, Eigen::MatrixXd>(),
           py::arg("a"), py::arg("b"), py::arg("Hreg"))
      .def_readwrite("a", &CostFunctionLS::a)
      .def_readwrite("b", &CostFunctionLS::b)
      .def_readwrite("Hreg", &CostFunctionLS::Hreg);

  // LinearInequalityConstraints
  py::class_<LinearInequalityConstraints>(m, "LinearInequalityConstraints")
      .def(py::init())
      .def(py::init<Eigen::MatrixXd, VectorLimits>(), py::arg("A"),
           py::arg("b"))
      .def_readwrite("A", &LinearInequalityConstraints::A)
      .def_readwrite("b", &LinearInequalityConstraints::b);

  // MPCMatrices
  py::class_<MPCMatrices>(m, "MPCMatrices")
      .def(py::init<HorizonParameters>(), py::arg("HorizonParameters"))
      .def("Fu", &MPCMatrices::Fu, py::arg("ind0"), py::arg("indf"))
      .def("u_max", &MPCMatrices::u_max)
      .def("ud_max", &MPCMatrices::ud_max)
      .def("udd_max", &MPCMatrices::udd_max)
      .def_property("wreg1", &MPCMatrices::update_wreg1, &MPCMatrices::wreg1)
      .def_property("wreg2", &MPCMatrices::update_wreg2, &MPCMatrices::wreg2)
      .def("update_initial_u", &MPCMatrices::update_initial_u, py::arg("u_1"))
      .def("update_u_scaling", &MPCMatrices::update_u_scaling,
           py::arg("factor"))
      .def("update_ud_scaling", &MPCMatrices::update_ud_scaling,
           py::arg("factor"))
      .def("update_udd_scaling", &MPCMatrices::update_udd_scaling,
           py::arg("factor"))
      .def("update_u_limits", &MPCMatrices::update_u_limits, py::arg("limits"))
      .def("update_ud_limits", &MPCMatrices::update_ud_limits,
           py::arg("limits"))
      .def("update_udd_limits", &MPCMatrices::update_udd_limits,
           py::arg("limits"))
      .def("update_initial_state", &MPCMatrices::update_initial_state,
           py::arg("x0"))
      .def("cost_function", &MPCMatrices::cost_function)
      .def("linear_constraints", &MPCMatrices::linear_constraints)
      .def("update_extra_linear_constraints",
           &MPCMatrices::update_extra_linear_constraints,
           py::arg("constraints"))
      .def("update_linear_horizon_matrices",
           py::overload_cast<const Eigen::VectorXd, const Eigen::VectorXd,
                             const Eigen::VectorXd>(
               &MPCMatrices::update_linear_horizon_matrices),
           py::arg("x0"), py::arg("xf"), py::arg("previous_x"))
      .def("update_linear_horizon_matrices",
           py::overload_cast<const Eigen::VectorXd, const Eigen::VectorXd>(
               &MPCMatrices::update_linear_horizon_matrices),
           py::arg("x0"), py::arg("xf"))
      .def("update_cost_ponderation", &MPCMatrices::update_cost_ponderation,
           py::arg("ponderation") = CostPonderation())
      .def("update_cost_b", &MPCMatrices::update_cost_b, py::arg("x0"),
           py::arg("xf"))
      .def("__repr__", &MPCMatrices::str_matrices)
      .def_readonly("params", &MPCMatrices::params)
      .def_readonly("Fx_underline", &MPCMatrices::Fx_underline)
      .def_readonly("Fx_overline", &MPCMatrices::Fx_overline)
      .def_readonly("Fu_underline", &MPCMatrices::Fu_underline)
      .def_readonly("Fud", &MPCMatrices::Fud)
      .def_readonly("Fudd", &MPCMatrices::Fudd)
      .def_readonly("X_overline_iden", &MPCMatrices::X_overline_iden)
      .def_readonly("A", &MPCMatrices::A);

  // SolveParamters
  py::class_<SolveParameters>(m, "SolveParameters")
      .def(py::init())
      .def_readwrite("u", &SolveParameters::u)
      .def_readwrite("ud", &SolveParameters::ud)
      .def_readwrite("udd", &SolveParameters::udd)
      .def_readwrite("u_scaling_factor", &SolveParameters::u_scaling_factor)
      .def_readwrite("ud_scaling_factor", &SolveParameters::ud_scaling_factor)
      .def_readwrite("udd_scaling_factor",
                     &SolveParameters::udd_scaling_factor);

  // TimedPose
  py::class_<TimedPose>(m, "TimedPose")
      .def(py::init())
      .def_readwrite("t", &TimedPose::t)
      .def_readwrite("pose", &TimedPose::pose);

  // Goal
  py::class_<Goal>(m, "Goal")
      .def(py::init())
      .def_readwrite("start", &Goal::start)
      .def_readwrite("pose", &Goal::pose)
      .def_readwrite("u0", &Goal::u0)
      .def_readwrite("ud0", &Goal::ud0);

  // Planifier
  py::class_<Planifier> planifier_class(m, "Planifier");

  // Planifier::State
  py::enum_<Planifier::State>(planifier_class, "State", py::arithmetic())
      .value("OK", Planifier::State::OK)
      .value("Computing", Planifier::State::Computing)
      .value("Failed", Planifier::State::Failed)
      .value("NonInitialized", Planifier::State::NonInitialized)
      .value("Waiting", Planifier::State::Waiting)
      .value("Ending", Planifier::State::Ending)
      .export_values();

  planifier_class
      .def(py::init<HorizonParameters, std::string>(),
           py::arg("HorizonParameters"),
           py::arg("solver") = lmpcpp_default_solver)
      .def(py::init<Planifier>(), py::arg("Planifier"))
      .def("status", &Planifier::status)
      .def("solve", &Planifier::solve, py::arg("goal"),
           py::arg("solve_parameters"))
      .def("update_cost_ponderation", &Planifier::update_cost_ponderation,
           py::arg("cost_ponderation"))
      .def("update_solve_parameters", &Planifier::update_solve_parameters,
           py::arg("solve_parameters"))
      .def("set_compute_time", &Planifier::set_compute_time, py::arg("t"))
      .def_property("use_origin_dlog", &Planifier::use_origin_dlog,
                    &Planifier::set_use_origin_dlog)
      .def("compute_time_limit", &Planifier::compute_time_limit)
      .def("print_matrices", &Planifier::print_matrices)
      .def("u_max", &Planifier::u_max)
      .def("ud_max", &Planifier::ud_max)
      .def("udd_max", &Planifier::udd_max)
      .def_property("wreg1", &Planifier::get_wreg1, &Planifier::set_wreg1)
      .def_property("wreg2", &Planifier::get_wreg2, &Planifier::set_wreg2)
      .def("t_horizon", &Planifier::t_horizon, py::arg("t"))
      .def("horizon_duration", &Planifier::horizon_duration)
      .def("input_horizon_duration", &Planifier::input_horizon_duration)
      // TODO add update_cartvel_polytope
      // TODO add update_obstacle
      .def_readonly("params", &Planifier::params)
      .def_readonly("solver", &Planifier::solver)
      .def_readonly("mpcmat", &Planifier::mpcmat)
      // TODO fix
      // .def_readonly("qpprob", &Planifier::qpprob)
      .def_readonly("polytope_linconstraints",
                    &Planifier::polytope_linconstraints)
      .def_readonly("obstacle_linconstraints",
                    &Planifier::obstacle_linconstraints)
      .def("test", &Planifier::test, py::arg("x0"), py::arg("A"))
      .def("test2", &Planifier::test2, py::arg("x0"), py::arg("A"));

  m.doc() = R"pbdoc(
           Linear MPC library for pose trajectory planification.
    )pbdoc";

  m.def("test_vec", &test_vec, R"pbdoc(
        test sum std::vector of Vector6d
    )pbdoc");

#ifdef VERSION_INFO
  m.attr("__version__") = MACRO_STRINGIFY(VERSION_INFO);
#else
  m.attr("__version__") = "dev";
#endif
}
