#include <Eigen/Core>
// #include <Eigen/Geometry>
#include <chrono>
#include <cmath>
#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <thread>

// #include "pinocchio/fwd.hpp"
// #include "pinocchio/parsers/urdf.hpp"
// #include "pinocchio/algorithm/joint-configuration.hpp"
// #include "pinocchio/algorithm/kinematics.hpp"
// #include "pinocchio/algorithm/jacobian.hpp"
// #include "pinocchio/algorithm/aba.hpp"
// #include "pinocchio/algorithm/rnea.hpp"
// #include "pinocchio/algorithm/crba.hpp"
// #include "pinocchio/algorithm/frames.hpp"
// #include "pinocchio/multibody/model.hpp"
// #include "pinocchio/algorithm/model.hpp"
// #include "pinocchio/algorithm/center-of-mass.hpp"

#include "pinocchio/spatial/explog.hpp"
#include "pinocchio/spatial/se3.hpp"
#include <mpc.hpp>

using namespace std;
using namespace lmpcpp;

constexpr double PI = 3.14159265359;
constexpr int m = 2; // % Must be 2 or 3 for plotting
constexpr int n = 5; //% Must be greater than m
using mymatrix = Eigen::Matrix<double, n, m, Eigen::RowMajor>;
using mycolvectorn = Eigen::Matrix<double, n, 1>;
using mycolvectorm = Eigen::Matrix<double, m, 1>;
using Vector6d = Eigen::Matrix<double, 6, 1>;

Eigen::Vector3d skew_cross3(Eigen::Vector3d x, Eigen::Vector3d y) {
  Eigen::Vector3d res;
  res(0) = -x(2) * y(1) + x(1) * y(2);
  res(1) = x(2) * y(0) - x(0) * y(2);
  res(2) = -x(1) * y(0) + x(0) * y(1);
  return res;
}

pinocchio::Motion brse3(pinocchio::Motion x, pinocchio::Motion y,
                        float factor) {
  return pinocchio::Motion(skew_cross3(x.angular(), y.linear()) * factor -
                               skew_cross3(y.angular(), x.linear()) * factor,
                           skew_cross3(x.angular(), y.angular()) * factor);
}

Vector6d brse3(Vector6d x, Vector6d y) {
  auto lin1 = x.head<3>();
  auto ang1 = x.tail<3>();
  auto lin2 = y.head<3>();
  auto ang2 = y.tail<3>();

  Vector6d res;
  res << ang1.cross(lin2) - ang2.cross(lin1), ang1.cross(ang2);
  return res;
}

std::vector<pinocchio::Motion> random_motions(long long N) {
  std::vector<pinocchio::Motion> motions;
  long long i = 0;
  while (i < N) {
    // auto so3 = Eigen::Vector3d::Random()*PI;
    // if (so3.norm()<= PI){
    //   auto pos = Eigen::Vector3d::Random();
    //   motions.push_back(pinocchio::Motion(pos,so3));
    //   i++;
    // }

    auto rand = pinocchio::Motion(Vector6d::Random());
    rand.angular() = rand.angular() * PI;
    // rand.angular() = rand.angular() * 0.5;
    // rand.linear() = rand.linear() * 2; // increase linear range
    // rand.linear() = rand.linear() * .3;
    if (rand.angular().norm() <= PI / 2) {
      // if (rand.angular().norm() <= PI / 6) {
      motions.push_back(rand);
      i++;
    }
  }
  return motions;
}

int main() {
  {
    unsigned int H = 4, nx = 3, nu = 2, ny = 4;
    unsigned int ndt = 2;
    double dt = 1e-3;
    cout << "Planifier(H=4, nx=3, nu=2, ny=4)" << endl;

    auto mpcparams = HorizonParameters(H, ndt, dt, nx, nu, ny);
    auto mpcprob = Planifier(mpcparams);
    cout << "mpcprob.test():" << endl << mpcprob.test() << endl;

    cout << "mpcprob.params.print():" << endl;
    mpcprob.params.print();

    SolveParameters solveparams;
    solveparams.u_scaling_factor = 0.3;
    solveparams.ud_scaling_factor = 0.2;
    solveparams.udd_scaling_factor = 0.1;
    solveparams.u = {Eigen::VectorXd::Constant(nu, -3),
                     Eigen::VectorXd::Constant(nu, 3)};
    solveparams.ud = {Eigen::VectorXd::Constant(nu, -2),
                      Eigen::VectorXd::Constant(nu, 2)};
    solveparams.udd = {Eigen::VectorXd::Constant(nu, -1),
                       Eigen::VectorXd::Constant(nu, 1)};
    mpcprob.update_solve_parameters(solveparams);
    mpcprob.print_matrices();

    // mymatrix A_mid = mymatrix::Random().array() - 0.5;
    // cout << "A_mid:" << endl << A_mid << endl;

    // using SE3Matrix = Eigen::Matrix<double, 4, 4, Eigen::RowMajor>;
    // SE3Matrix T = SE3Matrix::Random().array();
    // cout << "T:" << endl << T << endl;

    // pinocchio::SE3 a(T);
    // cout << "pin::SE3 a:" << endl << a << endl;
    // cout << "log(a):" << endl << pinocchio::log6(a) << endl;

    // // pinocchio::Motion
    // //
    // b(Eigen::Vector3d::Random().array(),Eigen::Vector3d::Random().array());
    // pinocchio::Motion b(Vector6d::Random());
    // cout << "pin::Motion b:" << endl << b << endl;
    // cout << "pin::Motion b.linear:" << b.linear().transpose() << endl;
    // cout << "pin::Motion b.angular:" << b.angular().transpose() << endl;
    // cout << "pin::Motion b.angular.norm():" << b.angular().norm() << endl;
    // cout << "exp(b):" << endl << pinocchio::exp6(b) << endl;
    // b.angular()(1) = 0.4;
    // cout << "pin::Motion b.angular()(1)=0.4:" << endl << b << endl;

    // Eigen::Vector3d myvec(1, 2, 3);
    // cout << "Eig::Vector3d myvec:" << myvec.transpose() << endl;
    // cout << "pin::skew(myvec):" << endl << pinocchio::skew(myvec) << endl;

    {
      std::cout << "************************************";
      std::cout << "************************************";
      std::cout << "************************************";
      std::cout << std::endl;
      unsigned int offset_rows = 2;
      unsigned int offset_cols = 3;
      unsigned int steps = 4;
      Eigen::MatrixXd H(Eigen::MatrixXd::Random(3, 2));
      Eigen::MatrixXd A_lin(Eigen::MatrixXd::Zero(
          offset_rows + steps * H.rows(), offset_cols + steps * H.cols()));
      Eigen::MatrixXd HH(Eigen::MatrixXd::Zero(H.rows(), A_lin.cols()));
      HH.block(0, offset_cols, H.rows(), H.cols()) = H;
      std::cout << "H:" << std::setprecision(2) << std::endl << H << std::endl;
      std::cout << "HH:" << std::setprecision(2) << std::endl
                << HH << std::endl;
      for (unsigned int n = 0; n < steps; ++n) {
        A_lin.block(offset_rows + n * H.rows(), 0, H.rows(), A_lin.cols())
            << block_rcircshift(HH, n * H.cols());
      }
      std::cout << "A_lin:" << std::setprecision(2) << std::endl
                << A_lin << std::endl;
      std::cout << "************************************" << std::endl;
    }

    {
      TickTock a("wait 0.1s");
      std::cout << "test TickTock duration: waiting 0.1s..." << std::endl;
      std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
  }

  // **************************************************
  // small H test
  {
    Eigen::Vector3d u;
    u << 1, 0, 0; // x=1
    auto pos = Eigen::Vector3d::Random();

    pinocchio::SE3 pose(Eigen::AngleAxisd(PI / 12, u).matrix(), pos);
    pinocchio::SE3 pose2(Eigen::AngleAxisd(-PI / 12, u).matrix(), pos);
    {
      TickTock a("pinocchio log6");
      std::cout << "pin log6:" << std::endl;
      std::cout << pinocchio::log6(pose) << std::endl;
    }
    // {
    //   TickTock a("closest_log6()");
    //   std::cout << "closest log6" << std::endl;
    //   std::cout << closest_log6(pose, pose2) << std::endl;
    // }

    // unsigned int ndt = 1;
    // unsigned int H2 = 4;
    unsigned int H2 = 100;
    unsigned int ndt = 40;
    TickTock a("Planifier(H=" + to_string(H2) + ") (substract waits)");
    std::cout << "************************************" << std::endl;
    std::cout << "************************************" << std::endl;
    cout << __FUNCTION__
         << ":Pose Planifier(H=" + to_string(H2) + "ndt=" + to_string(ndt) +
                ", nx=6, nu=6, ny=6)"
         << endl;
    auto params2 = HorizonParameters(H2, ndt);
    params2.print();
    auto mpcprob2 = Planifier(params2);
    // mpcprob2.print_matrices();

    auto pos0 = Eigen::Vector3d::Random();
    Eigen::Vector3d posf = pos0.array() + 2;
    pinocchio::SE3 pose0(Eigen::Matrix3d::Identity(), pos0);
    pinocchio::SE3 posef(Eigen::AngleAxisd(PI / 12, u).matrix(), posf);
    lmpcpp::Goal goal;
    // goal.u0 = mpcprob2.u_max().max.array() + 1e-3;
    goal.start.pose = pose0.toHomogeneousMatrix();
    goal.pose = posef.toHomogeneousMatrix();
    cout << __FUNCTION__ << ":Pose Planifier.solve_async()" << endl;
    auto future = mpcprob2.solve_async(goal);
    cout << lmpcpp::Planifier::StateToString(mpcprob2.status()) << endl;
    {
      std::cout << "------------------------------------" << std::endl;
      int wait_ms = 20;
      cout << __FUNCTION__
           << ":solve_async print something to proof it's async..." << endl;
      std::this_thread::sleep_for(std::chrono::milliseconds(wait_ms));
      cout << lmpcpp::Planifier::StateToString(mpcprob2.status()) << endl;
      cout << __FUNCTION__ << ":wait for result..." << endl;
      cout << __FUNCTION__ << ":future.valid():" << future.valid() << endl;
      auto horizon = future.get();
      auto result = *horizon;
      cout << lmpcpp::Planifier::StateToString(mpcprob2.status()) << endl;
      cout << __FUNCTION__ << ":future.valid():" << future.valid() << endl;
    }
    {
      std::cout << "------------------------------------" << std::endl;
      cout << __FUNCTION__ << ":new replanning but wait for result..." << endl;
      Eigen::Vector3d posf2 = pos0.array() + 3;
      pinocchio::SE3 posef2(Eigen::AngleAxisd(PI / 12, u).matrix(), posf2);
      goal.pose = posef2;
      auto future2 = mpcprob2.solve_async(goal);
      cout << __FUNCTION__ << ":future2.valid():" << future2.valid() << endl;
      auto horizon = future2.get();
      auto result = *horizon;
      cout << __FUNCTION__ << ":future2.valid():" << future2.valid() << endl;

      {
        for (double t_traj = 0; t_traj < mpcprob2.input_horizon_duration();
             t_traj += mpcprob2.params.dt) {
          auto cartesian_vel_k_1 = result.twist(t_traj - mpcprob2.params.dt);
          auto cartesian_vel_k = result.twist(t_traj);
          auto cartesian_acceleration_local =
              (cartesian_vel_k - cartesian_vel_k_1) / mpcprob2.params.dt;
          bool error = false;
          double eps = 1e-3; // depends on qp solver precision
          for (unsigned int i = 0; i < mpcprob2.params.nu; ++i) {
            if (cartesian_acceleration_local(i) >
                mpcprob2.ud_max().max(i) + eps) {
              error = true;
              cout << "Error: ud>ud_max. i:" << i << "  t_traj:" << t_traj
                   << "s" << endl;
            }
            if (cartesian_acceleration_local(i) <
                mpcprob2.ud_max().min(i) - eps) {
              error = true;
              cout << "Error: ud<ud_max. i:" << i << "  t_traj:" << t_traj
                   << "s" << endl;
            }
          }
          if (error) {
            cout << "acc_max:" << mpcprob2.ud_max().max.transpose() << endl;
            cout << "    acc:" << cartesian_acceleration_local.transpose()
                 << endl;
            cout << "acc_min:" << mpcprob2.ud_max().min.transpose() << endl;
            cout << "vel_k-1:" << cartesian_vel_k_1.transpose() << endl;
            cout << "  vel_k:" << cartesian_vel_k.transpose() << endl;
            cout << "     u0:" << goal.u0.transpose() << endl;
            exit(EXIT_FAILURE);
          }
        }
      }
    }

    cout << lmpcpp::Planifier::StateToString(mpcprob2.status()) << endl;

    cout << __FUNCTION__ << ":Pose Planifier: done" << endl;
    std::cout << "************************************" << std::endl;
    std::cout << "************************************" << std::endl;
  }

  std::cout << "++++++++++++++++++++++++++++++++++++" << std::endl;
  std::cout << "test_mpclib DONE" << std::endl;

  exit(EXIT_SUCCESS);
}
