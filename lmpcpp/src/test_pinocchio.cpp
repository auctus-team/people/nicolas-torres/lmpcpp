#include <Eigen/Core>
// #include <Eigen/Geometry>
#include <chrono>
#include <cmath>
#include <iomanip>
#include <iostream>

// #include "pinocchio/fwd.hpp"
// #include "pinocchio/parsers/urdf.hpp"
// #include "pinocchio/algorithm/joint-configuration.hpp"
// #include "pinocchio/algorithm/kinematics.hpp"
// #include "pinocchio/algorithm/jacobian.hpp"
// #include "pinocchio/algorithm/aba.hpp"
// #include "pinocchio/algorithm/rnea.hpp"
// #include "pinocchio/algorithm/crba.hpp"
// #include "pinocchio/algorithm/frames.hpp"
// #include "pinocchio/multibody/model.hpp"
// #include "pinocchio/algorithm/model.hpp"
// #include "pinocchio/algorithm/center-of-mass.hpp"

#include "pinocchio/spatial/explog.hpp"
#include "pinocchio/spatial/se3.hpp"

using namespace std;

constexpr double PI = 3.14159265359;
constexpr int m = 2; // % Must be 2 or 3 for plotting
constexpr int n = 5; //% Must be greater than m
using mymatrix = Eigen::Matrix<double, n, m, Eigen::RowMajor>;
using mycolvectorn = Eigen::Matrix<double, n, 1>;
using mycolvectorm = Eigen::Matrix<double, m, 1>;
using Vector6d = Eigen::Matrix<double, 6, 1>;

Eigen::Vector3d skew_cross3(Eigen::Vector3d x, Eigen::Vector3d y) {
  Eigen::Vector3d res;
  res(0) = -x(2) * y(1) + x(1) * y(2);
  res(1) = x(2) * y(0) - x(0) * y(2);
  res(2) = -x(1) * y(0) + x(0) * y(1);
  return res;
}

pinocchio::Motion brse3(pinocchio::Motion x, pinocchio::Motion y,
                        float factor) {
  return pinocchio::Motion(skew_cross3(x.angular(), y.linear()) * factor -
                               skew_cross3(y.angular(), x.linear()) * factor,
                           skew_cross3(x.angular(), y.angular()) * factor);
}

Vector6d brse3(Vector6d x, Vector6d y) {
  auto lin1 = x.head<3>();
  auto ang1 = x.tail<3>();
  auto lin2 = y.head<3>();
  auto ang2 = y.tail<3>();

  Vector6d res;
  res << ang1.cross(lin2) - ang2.cross(lin1), ang1.cross(ang2);
  return res;
}

std::vector<pinocchio::Motion> random_motions(long long N) {
  std::vector<pinocchio::Motion> motions;
  long long i = 0;
  while (i < N) {
    // auto so3 = Eigen::Vector3d::Random()*PI;
    // if (so3.norm()<= PI){
    //   auto pos = Eigen::Vector3d::Random();
    //   motions.push_back(pinocchio::Motion(pos,so3));
    //   i++;
    // }

    auto rand = pinocchio::Motion(Vector6d::Random());
    rand.angular() = rand.angular() * PI;
    // rand.angular() = rand.angular() * 0.5;
    // rand.linear() = rand.linear() * 2; // increase linear range
    // rand.linear() = rand.linear() * .3;
    if (rand.angular().norm() <= PI / 2) {
      // if (rand.angular().norm() <= PI / 6) {
      motions.push_back(rand);
      i++;
    }
  }
  return motions;
}

int main() {
  cout << "*******************************************************" << endl;
  cout << "                  Hello World!" << endl;

  mymatrix A_mid = mymatrix::Random().array() - 0.5;
  cout << "A_mid:" << endl << A_mid << endl;

  using SE3Matrix = Eigen::Matrix<double, 4, 4, Eigen::RowMajor>;
  SE3Matrix T = SE3Matrix::Random().array();
  cout << "T:" << endl << T << endl;

  pinocchio::SE3 a(T);
  cout << "pin::SE3 a:" << endl << a << endl;
  cout << "log(a):" << endl << pinocchio::log6(a) << endl;

  // pinocchio::Motion
  // b(Eigen::Vector3d::Random().array(),Eigen::Vector3d::Random().array());
  pinocchio::Motion b(Vector6d::Random());
  cout << "pin::Motion b:" << endl << b << endl;
  cout << "pin::Motion b.linear:" << b.linear().transpose() << endl;
  cout << "pin::Motion b.angular:" << b.angular().transpose() << endl;
  cout << "pin::Motion b.angular.norm():" << b.angular().norm() << endl;
  cout << "exp(b):" << endl << pinocchio::exp6(b) << endl;
  b.angular()(1) = 0.4;
  cout << "pin::Motion b.angular()(1)=0.4:" << endl << b << endl;

  Eigen::Vector3d myvec(1, 2, 3);
  cout << "Eig::Vector3d myvec:" << myvec.transpose() << endl;
  cout << "pin::skew(myvec):" << endl << pinocchio::skew(myvec) << endl;
}
