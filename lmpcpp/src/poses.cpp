#include "poses.hpp"

using namespace lmpcpp;

bool lmpcpp::operator==(const TimedPose &lhs, const TimedPose &rhs) {
  return (lhs.t == rhs.t) && (lhs.pose == rhs.pose);
}
bool lmpcpp::operator!=(const TimedPose &lhs, const TimedPose &rhs) {
  return !(lhs == rhs);
}

bool lmpcpp::operator==(const Goal &lhs, const Goal &rhs) {
  return (lhs.start == rhs.start) && (lhs.pose == rhs.pose) &&
         (lhs.u0 == rhs.u0);
}
bool lmpcpp::operator!=(const Goal &lhs, const Goal &rhs) {
  return !(lhs == rhs);
}
