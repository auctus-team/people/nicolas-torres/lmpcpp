/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2012, Willow Garage, Inc.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of Willow Garage nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#include "moveit_trajectory_interface/moveit_trajectory.hpp"

#include "pinocchio/algorithm/frames.hpp"
#include "pinocchio/algorithm/joint-configuration.hpp"
#include "pinocchio/algorithm/kinematics.hpp"
#include "pinocchio/algorithm/model.hpp"
#include "pinocchio/parsers/urdf.hpp"
#include <eigen_conversions/eigen_msg.h>
// #include "pinocchio/algorithm/jacobian.hpp"
// #include "pinocchio/fwd.hpp"
// #include "pinocchio/multibody/model.hpp"
//

#include <cmath>
#include <iostream>
#include <ros/ros.h> // ros::param, etc..

// #include <cstddef>
// #include <fstream>
// #include <iomanip>
// #include <polylib.hpp>
#include <lmpcpp/methods.hpp>
#include <ros/console.h>
#include <sstream>
#include <vector>

#define NODE_NAME "/lmpcpp_moveit"
#define REALTIME
#ifndef REALTIME
#define publock(publisher, instructions)                                       \
  {                                                                            \
    publisher.lock();                                                          \
    instructions;                                                              \
    publisher.unlockAndPublish();                                              \
  }
#else
#define publock(publisher, instructions)                                       \
  if (publisher.trylock()) {                                                   \
    instructions;                                                              \
    publisher.unlockAndPublish();                                              \
  }
#endif

constexpr double pi = 3.14159265358979323846;
// constexpr double delta_compute_time = 0.015; // [s]
constexpr double delta_compute_time = 0.0015; // [s]

template <typename T>
bool is_in_vector(const std::vector<T> &vector, const T &elt) {
  return vector.end() != std::find(vector.begin(), vector.end(), elt);
}

bool getRosParamEigen(const std::string &param_name,
                      Eigen::VectorXd &param_data, bool verbose = false) {
  std::vector<double> std_param;
  if (!ros::param::get(param_name, std_param)) {
    return false;
  }

  if (std_param.size() == param_data.size()) {
    for (int i = 0; i < param_data.size(); ++i)
      param_data(i) = std_param[i];
    if (verbose)
      ROS_INFO_STREAM(param_name << " : " << param_data.transpose());
    return true;
  }

  ROS_FATAL_STREAM("Wrong matrix size for param "
                   << param_name
                   << ". Check the Yaml config file. Killing ros");
  ros::shutdown();
  return false; // shouldn't ever reach this but avoids -Wreturn-type
}

MoveItTrajectory::MoveItTrajectory()
    : trajectory_is_built{false},
      horizon_publish_delay(
          0.05), //
                 //   H(8), ndt(30), trajcount(0),
                 //   mpcprob(new lmpcpp::Planifier(lmpcpp::HorizonParameters(H,
                 //   ndt))), mpc_update_delay_ttraj(2 *
                 //   mpcprob->compute_time_limit())
                 // horizon debug settings
      H(8), ndt(20), trajcount(0),
      mpcprob(new lmpcpp::Planifier(lmpcpp::HorizonParameters(H, ndt))),
      mpc_update_delay_ttraj(mpcprob->input_horizon_duration() / 2) //
{}

Eigen::VectorXd MoveItTrajectory::xd_limits() {
  // pinocchio::SE3 current_pose;
  // {
  //   std::lock_guard<std::mutex> guard(pin_mutex);
  //   pinocchio::forwardKinematics(model, data, current_q);
  //   pinocchio::updateFramePlacements(model, data);
  //   current_pose = data.oMf[model.getFrameId(controlled_frame)];
  // }
  // pinocchio::SE3 ref_frame(current_pose.rotation(),
  // Eigen::Vector3d::Zero());

  // return ref_frame.toActionMatrixInverse() * mpcprob->u_max().max;
  return mpcprob->u_max().max; // LOCAL frame
}

void print_limits(lmpcpp::Planifier &mpcprob) {
  ROS_WARN_STREAM("effective u.max:" << mpcprob.u_max().max.transpose());
  ROS_WARN_STREAM("effective u.min:" << mpcprob.u_max().min.transpose());
  ROS_WARN_STREAM("effective ud.max:" << mpcprob.ud_max().max.transpose());
  ROS_WARN_STREAM("effective ud.min:" << mpcprob.ud_max().min.transpose());
  ROS_WARN_STREAM("effective udd.max:" << mpcprob.udd_max().max.transpose());
  ROS_WARN_STREAM("effective udd.min:" << mpcprob.udd_max().min.transpose());
}

void MoveItTrajectory::init(std::string planning_group,
                            std::string robot_description_param_name,
                            Eigen::VectorXd q_init,
                            std::vector<std::string> joint_names) {
  if (ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME,
                                     ros::console::levels::Info)) {
    ros::console::notifyLoggerLevelsChanged();
  }

  this->planning_group = planning_group;
  this->robot_description_param_name = robot_description_param_name;
  // ROS_WARN("Received new trajectory");
  // The :planning_interface:`MoveGroupInterface` class can be easily
  // setup using just the name of the planning group you would like to control
  // and plan for.
  std::string robot_description;
  ros::param::get(robot_description_param_name, robot_description);
  load_robot_model(robot_description, joint_names);
  // ros::NodeHandle node_handle("/" + model.name);
  ros::NodeHandle node_handle(NODE_NAME);
  current_q = q_init;
  current_qd = Eigen::VectorXd::Zero(q_init.rows());
  current_qdd = Eigen::VectorXd::Zero(q_init.rows());

  pinocchio::forwardKinematics(model, data, q_init);
  pinocchio::updateFramePlacements(model, data);
  target_pose = data.oMf[model.getFrameId(controlled_frame)];
  target_pose_log = pinocchio::log6(target_pose).toVector();

  if (!control_frame_set)
    controlled_frame = model.frames[model.nframes - 1].name;
  cartesian_pose.matrix() =
      data.oMf[model.getFrameId(controlled_frame)].toHomogeneousMatrix();

  current_initial_mpc_pose_rostime = ros::Time::now();
  current_initial_mpc_pose = Eigen::MatrixXd::Zero(6, 1);
  cartesian_velocity = Eigen::MatrixXd::Zero(6, 1);
  cartesian_velocity_local = Eigen::MatrixXd::Zero(6, 1);
  cartesian_acceleration = Eigen::MatrixXd::Zero(6, 1);
  cartesian_acceleration_localpub = Eigen::MatrixXd::Zero(6, 1);
  cartesian_acceleration_local = Eigen::MatrixXd::Zero(6, 1);
  cartesian_acceleration_local_prev = Eigen::MatrixXd::Zero(6, 1);
  cartesian_jerk_local = Eigen::MatrixXd::Zero(6, 1);

  joint_configuration.resize(model.nv);
  joint_velocity.resize(model.nv);
  joint_acceleration.resize(model.nv);
  jointspace_limit_distance_publisher.init(node_handle,
                                           "jointspace_limit_distance", 1);

  min_velocity_se3_publisher.init(node_handle, "min_velocity_se3", 1);
  max_velocity_se3_publisher.init(node_handle, "max_velocity_se3", 1);
  current_velocity_se3_publisher.init(node_handle, "current_velocity_se3", 1);
  desired_velocity_se3_publisher.init(node_handle, "desired_velocity_se3", 1);
  current_acceleration_se3_publisher.init(node_handle,
                                          "current_acceleration_se3", 1);
  desired_acceleration_se3_publisher.init(node_handle,
                                          "desired_acceleration_se3", 1);
  desired_jerk_se3_publisher.init(node_handle, "desired_jerk_se3", 1);

  current_pose_se3_publisher.init(node_handle, "current_pose_se3", 1);
  error_target_pose_se3_publisher.init(node_handle, "error_target_pose_se3", 1);
  desired_pose_se3_publisher.init(node_handle, "desired_pose_se3", 1);
  target_pose_se3_publisher.init(node_handle, "target_pose_se3", 1);
  initial_mpc_pose_se3_publisher.init(node_handle,
                                      "current_initial_mpc_pose_se3", 1);
  obstacle_distance_se3_publisher.init(node_handle, "obstacle_distance_se3", 1);

  horizon_publisher.init(node_handle, "horizon", 1);
  target_pose_publisher.init(node_handle, "target_pose", 1);
  initial_pose_publisher.init(node_handle, "current_initial_mpc_pose", 1);
  desired_pose_publisher.init(node_handle, "desired_pose", 1);
  desired_velocity_world_publisher.init(node_handle, "desired_velocity_world",
                                        1);
  desired_acceleration_world_publisher.init(node_handle,
                                            "desired_acceleration_world", 1);

  const double eps = 1e-3;
  Eigen::VectorXd ux = Eigen::VectorXd::Zero(3);
  ux << 1, 0, 0;
  Eigen::VectorXd uy = Eigen::VectorXd::Zero(3);
  uy << 0, 1, 0;
  Eigen::VectorXd uz = Eigen::VectorXd::Zero(3);
  uz << 0, 0, 1;
  epsR = Eigen::AngleAxisd(eps, uz).matrix() *
         Eigen::AngleAxisd(eps, uy).matrix() *
         Eigen::AngleAxisd(eps, ux).matrix();

  std::string param_name;
  const std::string root_name = "/" + model.name + NODE_NAME;
  auto param_path = [&root_name](const std::string &name) {
    return root_name + "/" + name;
  };

  // *******************************************
  bool need_reconstruct_mpc = false;
  param_name = "H";
  double Htemp;
  if (node_handle.getParam(param_path(param_name), Htemp)) {
    H = static_cast<unsigned int>(Htemp);
    ROS_WARN_STREAM("Load " << param_name << ":" << H << " steps");
    need_reconstruct_mpc = true;
  }

  std::string solver = lmpcpp_default_solver;
  param_name = "solver";
  if (node_handle.getParam(param_path(param_name), solver)) {
    ROS_WARN_STREAM("Load " << param_name << ":" << solver);
    need_reconstruct_mpc = true;
  }

  double ndt_temp;
  param_name = "ndt";
  if (node_handle.getParam(param_path(param_name), ndt_temp)) {
    ndt = static_cast<unsigned int>(ndt_temp);
    ROS_WARN_STREAM("Load " << param_name << ":" << ndt
                            << " (step inside horizon hdt = ndt*dt)");
    need_reconstruct_mpc = true;
  }

  param_name = "mpc_wreg1";
  if (node_handle.getParam(param_path(param_name), mpc_wreg1)) {
    ROS_WARN_STREAM("Load " << param_name << ":" << mpc_wreg1);
    need_reconstruct_mpc = true;
  }

  param_name = "mpc_wreg2";
  if (node_handle.getParam(param_path(param_name), mpc_wreg2)) {
    ROS_WARN_STREAM("Load " << param_name << ":" << mpc_wreg2);
    need_reconstruct_mpc = true;
  }

  param_name = "control_period";
  if (node_handle.getParam(param_path(param_name), _control_period)) {
    ROS_WARN_STREAM("Load " << param_name << ":" << _control_period);
    need_reconstruct_mpc = true;
  }

  if (need_reconstruct_mpc) {
    mpcprob.reset(new lmpcpp::Planifier(
        lmpcpp::HorizonParameters(H, ndt, _control_period), solver));
    mpcprob->set_wreg1(mpc_wreg1);
    mpcprob->set_wreg2(mpc_wreg2);
  }

  bool use_origin_dlog = false;
  param_name = "use_origin_dlog";
  if (node_handle.getParam(param_path(param_name), use_origin_dlog)) {
    ROS_WARN_STREAM("Load " << param_name << ":" << use_origin_dlog);
  }
  mpcprob->set_use_origin_dlog(use_origin_dlog);

  param_name = "use_delta_reference";
  if (node_handle.getParam(param_path(param_name), use_delta_reference)) {
    ROS_WARN_STREAM("Load " << param_name << ":" << use_delta_reference);
  }

  double linear_weight = 1;
  param_name = "mpc_cost_linear";
  if (node_handle.getParam(param_path(param_name), linear_weight)) {
    ROS_WARN_STREAM("Load " << param_name << ":" << linear_weight);
  }
  double ang_weight = 1;
  param_name = "mpc_cost_angular";
  if (node_handle.getParam(param_path(param_name), ang_weight)) {
    ROS_WARN_STREAM("Load " << param_name << ":" << ang_weight);
  }

  double mpc_cost_terminal_weight_factor = 1;
  param_name = "mpc_cost_terminal_weight_factor";
  if (node_handle.getParam(param_path(param_name),
                           mpc_cost_terminal_weight_factor)) {
    ROS_WARN_STREAM("Load " << param_name << ":"
                            << mpc_cost_terminal_weight_factor);
  }
  mpcprob->update_cost_ponderation(lmpcpp::CostPonderation(
      linear_weight, ang_weight, mpc_cost_terminal_weight_factor));

  param_name = "mpc_update_delay";
  if (node_handle.getParam(param_path(param_name), mpc_update_delay_ttraj)) {
    ROS_WARN_STREAM("Load " << param_name << ":" << mpc_update_delay_ttraj
                            << "s");
  }

  param_name = "stop_the_world";
  if (node_handle.getParam(param_path(param_name), stop_the_world)) {
    ROS_WARN_STREAM("Load " << param_name << ":" << stop_the_world);
  }

  param_name = "mpc_compute_time";
  if (node_handle.getParam(param_path(param_name), mpc_compute_time)) {
    if (mpc_compute_time <= 0) {
      ROS_FATAL_STREAM(param_name << "parameter must be higher than 0, value:"
                                  << mpc_compute_time << "s");
    }
    ROS_WARN_STREAM("Load " << param_name << ":" << mpc_compute_time << "s");
    mpcprob->set_compute_time(mpc_compute_time);
  }

  ROS_WARN_STREAM("mpc compute time:" << mpcprob->compute_time_limit() << "s");
  ROS_WARN_STREAM("mpc state horizon:" << mpcprob->horizon_duration() << "s");
  ROS_WARN_STREAM("mpc input horizon:" << mpcprob->input_horizon_duration()
                                       << "s");
  ROS_WARN_STREAM("mpc wreg1:" << mpcprob->get_wreg1());
  ROS_WARN_STREAM("mpc wreg2:" << mpcprob->get_wreg2());
  ROS_WARN_STREAM("mpc origin dlog:" << mpcprob->use_origin_dlog());
  ROS_WARN_STREAM("mpc cost  linear:" << linear_weight);
  ROS_WARN_STREAM("mpc cost angular:" << ang_weight);

  double t_start_solve = (mpc_update_delay_ttraj -
                          (mpcprob->compute_time_limit() + delta_compute_time));

  ROS_WARN_STREAM(__LINE__ << ":t_start_solve:" << t_start_solve << "s");
  if (t_start_solve >= mpcprob->input_horizon_duration()) {
    ROS_ERROR_STREAM(__FILE__);
    ROS_ERROR_STREAM(__LINE__ << ":" << __PRETTY_FUNCTION__);
    ROS_ERROR_STREAM(
        __LINE__
        << ":t_start_solve CANNOT be "
           "lower than input horizon duration, check your values for:");
    ROS_ERROR_STREAM(__LINE__ << ":mpc_update_delay_ttraj:"
                              << mpc_update_delay_ttraj << "s");
    ROS_ERROR_STREAM(__LINE__ << ":delta_compute_time:" << delta_compute_time
                              << "s");
    ROS_ERROR_STREAM(__LINE__ << ":mpcprob->compute_time_limit():"
                              << mpcprob->compute_time_limit() << "s");
    ROS_ERROR_STREAM(__LINE__ << ":mpcprob->input_horizon_duration():"
                              << mpcprob->input_horizon_duration() << "s");
  }

  // *******************************************

  param_name = "filter_w0";
  if (node_handle.getParam(param_path(param_name), filter_w0)) {
    ROS_WARN_STREAM("Load " << param_name << ":" << filter_w0 << "Hz");
  }
  ROS_WARN_STREAM("filter cutoff freq (" << param_name << "):" << filter_w0
                                         << "Hz");
  if (filter_w0 < 0) {
    ROS_WARN_STREAM("filter_w0<0, disabling filter");
  }

  Eigen::VectorXd u_max = Eigen::VectorXd::Zero(6);
  u_max << 1.7, 1.7, 1.7, 2.5, 2.5, 2.5;
  param_name = "u_max";
  if (!getRosParamEigen(param_path(param_name), u_max)) {
    ROS_WARN_STREAM("Could not find parameter "
                    << param_name
                    << ", will use default values: " << u_max.transpose());
  }
  ROS_WARN_STREAM("Load " << param_name << ": " << u_max.transpose());
  Eigen::VectorXd u_min(-u_max);
  solveparams.u = {u_min, u_max};

  Eigen::VectorXd ud_max = Eigen::VectorXd::Zero(6);
  // NOTE : biggest cube inside R-sphere=> L=2R/sqrt(3)
  ud_max << 13, 13, 13, 25, 25, 25;
  param_name = "ud_max";
  if (!getRosParamEigen(param_path(param_name), ud_max)) {
    ROS_WARN_STREAM("Could not find parameter "
                    << param_name
                    << ", will use default values: " << ud_max.transpose());
  }
  ROS_WARN_STREAM("Load " << param_name << ": " << ud_max.transpose());
  Eigen::VectorXd ud_min(-ud_max);
  solveparams.ud = {ud_min, ud_max};

  {
    Eigen::VectorXd udd_max = Eigen::VectorXd::Zero(6);
    ud_max << 6500, 6500, 6500, 12500, 12500, 12500;
    param_name = "udd_max";
    if (!getRosParamEigen(param_path(param_name), udd_max)) {
      ROS_WARN_STREAM("Could not find parameter "
                      << param_name
                      << ", will use default values: " << udd_max.transpose());
    }
    ROS_WARN_STREAM("Load " << param_name << ": " << udd_max.transpose());
    Eigen::VectorXd udd_min(-udd_max);
    solveparams.udd = {udd_min, udd_max};
  }

  double u_scaling_factor = 0.1;
  param_name = "u_scaling_factor";
  if (!node_handle.getParam(param_path(param_name), u_scaling_factor)) {
    ROS_WARN_STREAM("Could not find parameter "
                    << param_name
                    << ", will use default values:" << u_scaling_factor);
  }
  ROS_WARN_STREAM("Load " << param_name << ":" << u_scaling_factor);
  solveparams.u_scaling_factor = u_scaling_factor;

  double ud_scaling_factor = 0.1;
  param_name = "ud_scaling_factor";
  if (!node_handle.getParam(param_path(param_name), ud_scaling_factor)) {
    ROS_WARN_STREAM("Could not find parameter "
                    << param_name
                    << ", will use default values:" << ud_scaling_factor);
  }
  ROS_WARN_STREAM("Load " << param_name << ":" << ud_scaling_factor);
  solveparams.ud_scaling_factor = ud_scaling_factor;

  {
    double udd_scaling_factor = 0.1;
    param_name = "udd_scaling_factor";
    if (!node_handle.getParam(param_path(param_name), udd_scaling_factor)) {
      ROS_WARN_STREAM("Could not find parameter "
                      << param_name
                      << ", will use default values:" << udd_scaling_factor);
    }
    ROS_WARN_STREAM("Load " << param_name << ":" << udd_scaling_factor);
    solveparams.udd_scaling_factor = udd_scaling_factor;
  }

  {
    param_name = "u_scaling_max";
    if (node_handle.getParam(param_path(param_name), u_scaling_max)) {
      ROS_WARN_STREAM("Load " << param_name << ":" << u_scaling_max);
    }
    param_name = "u_scaling_min";
    if (node_handle.getParam(param_path(param_name), u_scaling_min)) {
      ROS_WARN_STREAM("Load " << param_name << ":" << u_scaling_min);
    }
    param_name = "u_scaling_update_delay";
    if (node_handle.getParam(param_path(param_name), u_scaling_ttraj_delay)) {
      ROS_WARN_STREAM("Load " << param_name << ":" << u_scaling_ttraj_delay);
    }
    param_name = "u_scaling_update_increment";
    if (node_handle.getParam(param_path(param_name),
                             u_scaling_update_increment)) {
      ROS_WARN_STREAM("Load " << param_name << ":"
                              << u_scaling_update_increment);
    }
  }

  mpcprob->update_solve_parameters(solveparams);
  print_limits(*mpcprob);

  is_initialized = true;
}

void MoveItTrajectory::set_u_scaling_factor(double u_scaling_factor) {
  if (current_horizon_mutex.try_lock()) {
    solveparams.u_scaling_factor = u_scaling_factor;
    current_horizon_mutex.unlock();
  }
}

bool MoveItTrajectory::isInitialized() { return is_initialized; }

std::string MoveItTrajectory::getRobotName() { return model.name; }

bool MoveItTrajectory::load_robot_model(std::string robot_description,
                                        std::vector<std::string> joint_names) {
  pinocchio::Model temp_model;
  pinocchio::urdf::buildModelFromXML(robot_description, temp_model, false);
  if (!joint_names.empty()) {
    std::vector<pinocchio::JointIndex> list_of_joints_to_keep_unlocked_by_id;
    for (std::vector<std::string>::const_iterator it = joint_names.begin();
         it != joint_names.end(); ++it) {
      const std::string &joint_name = *it;
      if (temp_model.existJointName(joint_name))
        list_of_joints_to_keep_unlocked_by_id.push_back(
            temp_model.getJointId(joint_name));
    }

    // Transform the list into a list of joints to lock
    std::vector<pinocchio::JointIndex> list_of_joints_to_lock_by_id;
    for (pinocchio::JointIndex joint_id = 1;
         joint_id < temp_model.joints.size(); ++joint_id) {
      const std::string joint_name = temp_model.names[joint_id];
      if (is_in_vector(joint_names, joint_name))
        continue;
      else {
        list_of_joints_to_lock_by_id.push_back(joint_id);
      }
    }

    // Build the reduced temp_model from the list of lock joints
    Eigen::VectorXd q_rand = pinocchio::randomConfiguration(temp_model);
    model = pinocchio::buildReducedModel(temp_model,
                                         list_of_joints_to_lock_by_id, q_rand);
  }
  // TODO: so joint_names can in fact be empty or this should be a critical
  // error?
  data = pinocchio::Data(model);

  return true;
}
double MoveItTrajectory::control_period() { return _control_period; }

void error_stream(std::string msg) { ROS_ERROR_STREAM(msg); }

void MoveItTrajectory::catch_update_horizon(bool force_wait) {
  try {
    ROS_WARN_STREAM("HORIZON UPDATE:mpc_update_next_ttraj:"
                    << mpc_update_next_ttraj << "s  t_traj:" << t_traj << "s");
    if ((!force_wait) && (mpcprob->status() != lmpcpp::Planifier::Waiting)) {
      ROS_WARN_STREAM("HORIZON UPDATE: error, mpcprob.status != Waiting");
      return;
    }
    lmpcpp::TickTock temp("MoveItTrajectory::catch_update_horizon");
    ROS_WARN_STREAM("HORIZON UPDATE:try");
    current_horizon = std::move(future_horizon.get());
    mpc_update_next_ttraj = t_traj + mpc_update_delay_ttraj;
    ROS_WARN_STREAM("HORIZON UPDATE:sucess");

    if (current_horizon->solve_time() >=
        mpcprob->compute_time_limit() + delta_compute_time) {
      std::stringstream ss;
      ss << "solve_time: " << current_horizon->solve_time() * 1000
         << "ms exeeds requested compute time ("
         << mpcprob->compute_time_limit() << "s) + delta_compute_time ("
         << delta_compute_time << "s)";
      error_stream(ss.str());
      error_stream(ss.str());
      error_stream(ss.str());
    }

    const double max_update_horizon_time = 0.0003; // [s]
    double tock = temp.tock_s();
    if (tock > max_update_horizon_time) {
      std::stringstream ss;
      ss << "horizon update time exeeded!: " << tock * 1000 << "ms";
      error_stream(ss.str());
      error_stream(ss.str());
      error_stream(ss.str());
    }

  } catch (const std::exception &e) {
    ROS_ERROR_STREAM("HORIZON UPDATE:caught exception " << e.what());
    ROS_ERROR_STREAM("future_horizon.valid():" << future_horizon.valid());
    ROS_ERROR_STREAM("-->  REUSE LAST HORIZON <--");
    ROS_ERROR_STREAM("-->  REUSE LAST HORIZON <--");
    ROS_ERROR_STREAM("-->  REUSE LAST HORIZON <--");
    ROS_ERROR_STREAM("-->  REUSE LAST HORIZON <--");
  }

  print_limits(*mpcprob);
  ROS_WARN_STREAM("HORIZON UPDATE:t_traj:" << t_traj << "s");
  ROS_WARN_STREAM("----------------------------");
}

void MoveItTrajectory::updateTrajectory(const Eigen::VectorXd &q,
                                        const Eigen::VectorXd &qd,
                                        const Eigen::VectorXd &qdd) {

  // pinocchio::SE3 current_pose;
  // Eigen::VectorXd current_twist;
  {
    std::lock_guard<std::mutex> guard(pin_mutex);
    current_q = q;
    current_qd = qd;
    current_qdd = qdd;

    pinocchio::forwardKinematics(model, data, current_q, current_qd,
                                 current_qdd);
    pinocchio::updateFramePlacements(model, data);
    current_pose = data.oMf[model.getFrameId(controlled_frame)];

    auto frameid = model.getFrameId(controlled_frame);
    current_twist = pinocchio::getFrameVelocity(
                        model, data, frameid, pinocchio::ReferenceFrame::LOCAL)
                        .toVector();

    current_twist_acc =
        pinocchio::getFrameVelocity(model, data, frameid,
                                    pinocchio::ReferenceFrame::LOCAL)
            .toVector();
  }
  current_twist = lptwist.filter(current_twist, filter_w0,
                                 mpcprob->params.dt); // low-pass filter
  // current_twist_acc = lptwist.filter(current_twist_acc, filter_w0,
  //                                    mpcprob->params.dt); // low-pass
  //                                    filter

  if (trajectory_is_built.load()) {
    /////////////////////////////////////////////////
    // only recompute trajectory if not planified until the end!
    // const pinocchio::SE3 horizon_end = pinocchio::exp6(
    //     current_mpcresult->x(current_mpcresult->horizon_duration()));
    // Eigen::Matrix<double, 6, 1> err =
    //     pinocchio::log6(horizon_end.actInv(target_pose)).toVector();

    // Eigen::Matrix<double, 6, 1> err =
    //     pinocchio::log6(current_pose.actInv(target_pose)).toVector();

    // const auto &lin = err.head<3>();
    // const auto &ang = err.tail<3>();
    // bool err_condition = false;
    // if ((lin.norm() < 1e-3) && (ang.norm() < 1e-3)) {
    //   err_condition = true;
    // }

    double t_start_solve =
        (mpc_update_next_ttraj -
         (mpcprob->compute_time_limit() + delta_compute_time));

    if (stop_the_world) {
      t_start_solve = mpc_update_next_ttraj;
    }

    if ((t_traj >= t_start_solve) && !future_horizon.valid()) {
      // if (t_traj >= u_scaling_ttraj_next) {
      //   if ((vel_scaling_iter % 2) == 0) {
      //     solveparams.u_scaling_factor += u_scaling_update_increment;
      //     if (solveparams.u_scaling_factor > u_scaling_max) {
      //       solveparams.u_scaling_factor = u_scaling_max;
      //       vel_scaling_iter++;
      //     }
      //   } else {
      //     solveparams.u_scaling_factor -= u_scaling_update_increment;
      //     if (solveparams.u_scaling_factor < u_scaling_min) {
      //       solveparams.u_scaling_factor = u_scaling_min;
      //       vel_scaling_iter++;
      //     }
      //   }
      //   u_scaling_ttraj_next = t_traj + u_scaling_ttraj_delay;
      // }

      std::cout << "max_vel_scaling: " << vel_scaling << std::endl;

      if (current_horizon_mutex.try_lock()) {
        ROS_WARN_STREAM("****************************");
        auto t_horizon = current_horizon->t_horizon(t_traj);

        ROS_WARN_STREAM("RECOMPUTE HORIZON: t_horizon:"
                        << t_horizon << "s  t_start_solve:" << t_start_solve
                        << "  t_traj:" << t_traj);
        ROS_WARN_STREAM("compute_time_limit:" << mpcprob->compute_time_limit()
                                              << "s  delta_compute_time:"
                                              << delta_compute_time << "s");
        ROS_WARN_STREAM("mpc_update_delay_ttraj:" << mpc_update_delay_ttraj);
        ROS_WARN_STREAM(
            "status==Waiting:"
            << (mpcprob->status() == lmpcpp::Planifier::State::Waiting));
        ROS_WARN_STREAM(
            "status==Computing:"
            << (mpcprob->status() == lmpcpp::Planifier::State::Computing));

        current_initial_mpc_pose_rostime = ros::Time::now();
        current_initial_mpc_pose = pinocchio::log6(current_pose).toVector();
        lmpcpp::Goal goal;
        goal.start.t = t_traj;
        goal.start.pose = current_pose.toHomogeneousMatrix();
        // goal.start.pose =
        //     pinocchio::SE3::Interpolate(
        //         current_pose, pinocchio::SE3(current_horizon->pose(t_traj +
        //         .05)), 0.5) .toHomogeneousMatrix();

        // Eigen::MatrixXd J(6, model.nv);
        // pinocchio::computeFrameJacobian(
        //     model, data, current_q, model.getFrameId(controlled_frame),
        //     pinocchio::ReferenceFrame::LOCAL_WORLD_ALIGNED, J);
        // J = ref_pose.toActionMatrix() * J;

        // POLYTOPE
        // double polytope_horizon = 0.015;
        // mpcprob->update_cartvel_polytope(
        //     {model.lowerPositionLimit, model.upperPositionLimit},
        //     {-model.velocityLimit, model.velocityLimit}, current_q,
        //     polytope_horizon, J);

        // OBSTACLE
        // Eigen::Vector3d obs_pos;
        // obs_pos << 0.5, 0.1, 0.3;
        // const double r(2);
        // pinocchio::SE3 obs_pose(current_pose.rotation(), obs_pos);
        // // mpcprob->update_obstacle(current_pose.actInv(obs_pose), r);
        // {
        //   auto rosnow = ros::Time::now();
        //   publock(
        //       obstacle_distance_se3_publisher, geometry_msgs::Twist
        //       twistmsg; tf::twistEigenToMsg(
        //           // lmpcpp::jexp_dist(current_pose.toHomogeneousMatrix(),
        //           //                   obs_pose.toHomogeneousMatrix()),
        //           lmpcpp::jexp_dist(
        //               current_pose.actInv(current_pose).toHomogeneousMatrix(),
        //               current_pose.actInv(obs_pose).toHomogeneousMatrix()),
        //           twistmsg);
        //       obstacle_distance_se3_publisher.msg_.header.stamp = rosnow;
        //       obstacle_distance_se3_publisher.msg_.header.frame_id =
        //       "world"; obstacle_distance_se3_publisher.msg_.twist =
        //       twistmsg;);
        // }

        goal.pose = pinocchio::exp6(target_pose_log).toHomogeneousMatrix();
        goal.u0 = current_twist;
        // TODO DANGER!! it seems ddq_d is too noisy and makes
        // current_twist_acc NOT trust-worthy
        goal.ud0 = current_twist_acc;

        future_horizon = std::move(mpcprob->solve_async(goal, solveparams));
        ROS_WARN_STREAM("t_traj:"
                        << t_traj
                        << "s after solve_async::future_horizon.valid():"
                        << future_horizon.valid());
        ROS_WARN_STREAM(
            "status==Waiting:"
            << (mpcprob->status() == lmpcpp::Planifier::State::Waiting));
        ROS_WARN_STREAM(
            "status==Computing:"
            << (mpcprob->status() == lmpcpp::Planifier::State::Computing));

        current_horizon_mutex.unlock();
      }
    }

    if (t_traj >= mpc_update_next_ttraj) {
      if (current_horizon_mutex.try_lock()) {
        catch_update_horizon();
        current_horizon_mutex.unlock();
      }
    }

    /////////////////////////////////////////////////

    {
      auto t_horizon = current_horizon->t_horizon(t_traj);
      if (t_horizon >= mpcprob->horizon_duration()) {
        ROS_ERROR_STREAM_THROTTLE(
            .1, "t_horizon (" << t_horizon << "s) exeeds horizon_duration ("
                              << mpcprob->horizon_duration() << "s)");
      }
    }

    if (use_delta_reference) {
      cartesian_pose.matrix() =
          current_horizon->pose(t_traj + mpcprob->params.dt, current_pose);
    } else {
      cartesian_pose.matrix() =
          current_horizon->pose(t_traj + mpcprob->params.dt);
    }

    // TODO: discuss if it's better to use the actual pose or the planified
    // pose
    Eigen::Matrix<double, 6, 6> adj =
        // pinocchio::SE3(cartesian_pose.matrix()).toActionMatrix();
        current_pose.toActionMatrix();
    // Eigen::Matrix<double, 6, 6> adj =
    //     pinocchio::SE3(cartesian_pose.rotation(), Eigen::Vector3d::Zero())
    // .toActionMatrix();

    cartesian_velocity_local = current_horizon->twist(t_traj);
    cartesian_velocity = adj * cartesian_velocity_local;
    // cartesian_velocity = cartesian_velocity_local;
    // TODO remove
    // cartesian_velocity.setZero();

    {
      auto t_horizon = current_horizon->t_horizon(t_traj);
      Eigen::VectorXd cartesian_vel_k_1;
      if (t_horizon == 0) {
        cartesian_vel_k_1 = current_horizon->u0();
        std::stringstream ss;
        ss << "--------------------------------------" << std::endl;
        ss << "               t_horizon=0            " << std::endl;
        ss << "--------------------------------------" << std::endl;
        std::cout << ss.str();
      } else {
        cartesian_vel_k_1 = current_horizon->twist(t_traj - mpcprob->params.dt);
      }
      auto cartesian_vel_k = current_horizon->twist(t_traj);
      cartesian_acceleration_local =
          (cartesian_vel_k - cartesian_vel_k_1) / mpcprob->params.dt;
      // const auto Zero6 = Eigen::VectorXd::Zero(6);
      if (t_horizon >= mpcprob->input_horizon_duration()) {
        // if ((cartesian_vel_k_1 == Zero6) || (cartesian_vel_k == Zero6)) {
        cartesian_acceleration_localpub.setZero();
        std::stringstream ss;
        ss << "--------------------------------------" << std::endl;
        ss << "            ZERO ACC INPUT            " << std::endl;
        ss << "--------------------------------------" << std::endl;
        input_zeroaccmsg_delay.print(ss.str());
      } else {
        cartesian_acceleration_localpub = cartesian_acceleration_local;
      }

      cartesian_jerk_local =
          cartesian_acceleration_local - cartesian_acceleration_local_prev;
      cartesian_acceleration_local_prev = cartesian_acceleration_local;
    }

    // cartesian_acceleration_local = Eigen::MatrixXd::Zero(6, 1);
    // cartesian_acceleration = cartesian_acceleration_local;
    cartesian_acceleration = adj * cartesian_acceleration_local;

    // TODO: use realtime or rely on this?
    // https://github.com/frankaemika/franka_ros/blob/8b8d1c2bc12c2ce8cb9587ce37b185421895fec5/franka_control/src/franka_control_node.cpp#L122
    t_traj += mpcprob->params.dt;
  }
  publish_trajectory();
}

Eigen::Affine3d MoveItTrajectory::getCartesianPose() { return cartesian_pose; }

Eigen::Matrix<double, 6, 1> MoveItTrajectory::getCartesianVelocity() {
  return cartesian_velocity;
  // return cartesian_velocity_local;
}

Eigen::Matrix<double, 6, 1> MoveItTrajectory::getCartesianAcceleration() {
  return cartesian_acceleration;
}

void MoveItTrajectory::setControlledFrame(std::string controlled_frame) {
  this->controlled_frame = controlled_frame;
  control_frame_set = true;
}

void MoveItTrajectory::sortJointState(moveit_msgs::RobotTrajectory &msg) {
  // This function is used to sort the joint comming from the /joint_state
  // topic They are order alphabetically while we need them to be order as in
  // the URDF file With use the joint_names variable that defined the joints
  // in the correct order
  moveit_msgs::RobotTrajectory sorted_joint_state = msg;
  std::vector<std::string> joint_names_from_urdf;
  for (pinocchio::JointIndex joint_id = 1;
       joint_id < (pinocchio::JointIndex)model.njoints; ++joint_id) {
    joint_names_from_urdf.push_back(model.names[joint_id]);
  }
  if (joint_names_from_urdf != msg.joint_trajectory.joint_names) {
    for (int num_points = 0; num_points < msg.joint_trajectory.points.size();
         num_points++) {
      int i = 0;
      for (auto ith_unsorted_joint_name : msg.joint_trajectory.joint_names) {
        int j = 0;
        for (auto jth_joint_name : joint_names_from_urdf) {
          if (ith_unsorted_joint_name.find(jth_joint_name) !=
              std::string::npos) {
            sorted_joint_state.joint_trajectory.points[num_points]
                .positions[j] =
                msg.joint_trajectory.points[num_points].positions[i];
            //  sorted_joint_state.joint_trajectory.joint_names[j] =
            //  msg.joint_trajectory.joint_names[i];
            break;
          } else {
            j++;
          }
        }
        i++;
      }
    }
  }
  msg = sorted_joint_state;
}

void MoveItTrajectory::abort_objective() {
  trajectory_is_built.store(false);
  set_objective(current_q, current_q);
}

void MoveItTrajectory::set_objective(const Eigen::VectorXd start,
                                     const Eigen::VectorXd end) {
  std::lock_guard<std::mutex> guard(current_horizon_mutex);
  ROS_WARN("current_horizon_mutex locked...");
  // ------------------------------------------------------------
  // ------------------------------------------------------------
  // MPC
  ///////////////////////////////////////////////////////////////
  // 1
  cartesian_acceleration.setZero();
  cartesian_velocity.setZero();

  lmpcpp::Goal goal;
  {
    std::lock_guard<std::mutex> guard(pin_mutex);
    pinocchio::forwardKinematics(model, data, start);
    pinocchio::updateFramePlacements(model, data);
    auto starting_posee = data.oMf[model.getFrameId(controlled_frame)];
    auto from = pinocchio::log6(starting_posee).toVector();
    cartesian_pose.matrix() = starting_posee.toHomogeneousMatrix();

    pinocchio::forwardKinematics(model, data, end);
    pinocchio::updateFramePlacements(model, data);
    target_pose = data.oMf[model.getFrameId(controlled_frame)];
    target_pose_log = pinocchio::log6(target_pose).toVector();
    auto target = target_pose_log;

    goal.start.t = 0;
    goal.start.pose = starting_posee.toHomogeneousMatrix();
    goal.pose = target_pose.toHomogeneousMatrix();

    current_initial_mpc_pose_rostime = ros::Time::now();
    current_initial_mpc_pose = from;
  }

  // std::cout << "    x0:" << x0.transpose() << std::endl;
  // std::cout << "    xf:" << xf.transpose() << std::endl;
  // std::cout << "exp x0:" << pinocchio::exp6(x0) << std::endl;
  // std::cout << "exp xf:" << pinocchio::exp6(xf) << std::endl;
  {
    ROS_WARN("!!!!!!!!!!!!!!!!!!!!!!!!!!");
    ROS_WARN("!!!!!!!!!!!!!!!!!!!!!!!!!!");
    ROS_WARN("     NEW TRAJECTORY REQUEST");
    // ROS_WARN_STREAM("  from:" << from.transpose());
    // ROS_WARN_STREAM("target:" << target.transpose());
    // std::stringstream ss;
    // ss << "  from:" << pinocchio::exp6(from) << std::endl;
    // ss << "target:" << pinocchio::exp6(target) << std::endl;
    // ROS_WARN(ss.str().c_str());
  }

  // // HACK to change velocity limits
  // vel_scaling_iter++;
  // if (vel_scaling_iter > 1) {
  //   vel_scaling_iter = 0;
  //   solveparams.u_scaling_factor += 0.1;
  //   if (solveparams.u_scaling_factor > 0.5) {
  //     solveparams.u_scaling_factor = 0.1;
  //   }
  // }
  // // if ((vel_scaling_iter % 2) == 0) {
  // //   vel_scaling = 0.3;
  // // }
  // std::cout << "max_vel_scaling: " << vel_scaling << std::endl;

  ROS_WARN("waiting for planifier first result...");
  future_horizon = std::move(mpcprob->solve_async(goal, solveparams));
  mpcprob->set_compute_time(mpc_compute_time * 15);
  catch_update_horizon(true);
  mpcprob->set_compute_time(mpc_compute_time);
  ROS_WARN("DONE!");

  ROS_WARN(" NEW TRAJECTORY COMPUTED");
  ROS_WARN("!!!!!!!!!!!!!!!!!!!!!!!!!!");
  ROS_WARN("!!!!!!!!!!!!!!!!!!!!!!!!!!");

  t_traj = 0.0;
  mpc_update_next_ttraj = t_traj + mpc_update_delay_ttraj;
  u_scaling_ttraj_next = t_traj + u_scaling_ttraj_delay;
  trajectory_is_built.store(true);
}

bool MoveItTrajectory::computeNewTrajectory(
    moveit_msgs::RobotTrajectory path, double max_velocity_scaling_factor,
    double max_acceleration_scaling_factor) {
  if (path.joint_trajectory.points.size() == 0) {
    ROS_WARN("Empty trajectory not doing anything");
    return false;
  }
  trajectory_is_built.store(false);
  traj_number++;
  sortJointState(path);

  std::vector<Eigen::VectorXd> waypoints;

  std::stringstream ss;
  ss << std::endl << "---------------------------------------" << std::endl;
  ss << "model.nv:" << model.nv << std::endl;
  ss << "  points:" << path.joint_trajectory.points.size() << std::endl;
  for (int point = 0; point < path.joint_trajectory.points.size(); point++) {
    Eigen::VectorXd waypoint(model.nv);
    for (int i = 0; i < waypoint.size(); ++i) {
      waypoint[i] = path.joint_trajectory.points[point].positions[i];
    }
    waypoints.push_back(waypoint);
  }
  ss << "start configuration:" << waypoints[0].transpose() << std::endl;
  auto finalwp = path.joint_trajectory.points.size() - 1;
  ss << "end   configuration:" << waypoints[finalwp].transpose() << std::endl;
  ss << "---------------------------------------" << std::endl;
  ROS_WARN(ss.str().c_str());

  ROS_WARN_STREAM("max velocity scaling: " << max_velocity_scaling_factor);
  ROS_WARN_STREAM(
      "max acceleration scaling: " << max_acceleration_scaling_factor);

  // TODO: disable for now
  // mpcprob->update_max_vel_scaling(max_velocity_scaling_factor);
  // mpcprob->update_max_acc_scaling(max_acceleration_scaling_factor);

  Eigen::VectorXd q;
  {
    std::lock_guard<std::mutex> guard(pin_mutex);
    q = current_q;
  }
  set_objective(q, waypoints[finalwp]);
  // NOTE: waypoints[0] would work fine except when bypassing gazebo
  // simulation set_objective(waypoints[0], waypoints[finalwp]);
  return true;
}

bool MoveItTrajectory::GoalReached() {
  std::lock_guard<std::mutex> guard(pin_mutex);
  pinocchio::SE3 tipMdes;
  pinocchio::forwardKinematics(model, data, current_q);
  pinocchio::updateFramePlacements(model, data);

  tipMdes = data.oMf[model.getFrameId(controlled_frame)].actInv(target_pose);
  Eigen::Matrix<double, 6, 1> err = pinocchio::log6(tipMdes).toVector();

  const auto &lin = err.head<3>();
  const auto &ang = err.tail<3>();
  if ((lin.norm() < 1e-3) && (ang.norm() < 1e-3)) {
    return true;
  }
  return false;
}

void MoveItTrajectory::publish_trajectory() {

  // throttle pubishing horizon
  if (horizon_publish_delay.passed() && trajectory_is_built.load()) {
    publock(
        horizon_publisher,
        horizon_publisher.msg_.header.stamp = ros::Time::now();
        horizon_publisher.msg_.header.frame_id = "world";
        auto pose_vec = current_horizon->sample_horizon();

        horizon_publisher.msg_.poses.resize(pose_vec.size());
        for (unsigned int i = 0; i < pose_vec.size(); ++i) {
          Eigen::Affine3d posek;
          posek.matrix() = pose_vec[i];
          geometry_msgs::Pose posek_msg;
          tf::poseEigenToMsg(posek, posek_msg);
          horizon_publisher.msg_.poses[i] = posek_msg;
        } horizon_publish_delay.refresh();
        ROS_WARN_THROTTLE(.5, "PUBLISH HORIZON");)
  }

  auto rosnow = ros::Time::now();
  tf::poseEigenToMsg(cartesian_pose, cartesian_pose_msg);
  tf::twistEigenToMsg(cartesian_velocity, cartesian_velocity_msg);
  tf::twistEigenToMsg(cartesian_acceleration, cartesian_acceleration_msg);

  // if (current_mpcresult_starttime == 0) {
  //   ROS_WARN_STREAM("***********************************");
  //   ROS_WARN_STREAM("***********************************");
  //   ROS_WARN_STREAM("PUBLISH POSE0");
  //   ROS_WARN_STREAM("***********************************");
  //   ROS_WARN_STREAM("***********************************");
  publock(initial_mpc_pose_se3_publisher, geometry_msgs::Twist twistmsg;
          tf::twistEigenToMsg(current_initial_mpc_pose, twistmsg);
          initial_mpc_pose_se3_publisher.msg_.header.stamp =
              current_initial_mpc_pose_rostime;
          initial_mpc_pose_se3_publisher.msg_.header.frame_id = "world";
          initial_mpc_pose_se3_publisher.msg_.twist = twistmsg;);
  // }

  auto frameid = model.getFrameId(controlled_frame);
  {
    std::lock_guard<std::mutex> guard(pin_mutex);
    pinocchio::Motion current_pose_se3;
    pinocchio::forwardKinematics(model, data, current_q, current_qd);
    pinocchio::updateFramePlacements(model, data);
    current_pose_se3 = pinocchio::log6(current_pose);
    publock(current_pose_se3_publisher, geometry_msgs::Twist twistmsg;
            tf::twistEigenToMsg(current_pose_se3.toVector(), twistmsg);
            current_pose_se3_publisher.msg_.header.stamp = rosnow;
            current_pose_se3_publisher.msg_.header.frame_id = "world";
            current_pose_se3_publisher.msg_.twist = twistmsg;);
    publock(desired_pose_se3_publisher,
            auto desired_pose_se3 =
                pinocchio::log6(cartesian_pose.matrix()).toVector();
            geometry_msgs::Twist twistmsg;
            tf::twistEigenToMsg(desired_pose_se3, twistmsg);
            desired_pose_se3_publisher.msg_.header.stamp = rosnow;
            desired_pose_se3_publisher.msg_.header.frame_id = "world";
            desired_pose_se3_publisher.msg_.twist = twistmsg;);
    publock(current_velocity_se3_publisher, geometry_msgs::Twist twistmsg;
            current_velocity_se3_publisher.msg_.header.frame_id = "local";
            // current_velocity_se3_publisher.msg_.header.frame_id = "world";
            // auto xd = pinocchio::getFrameVelocity(
            //     model, data, frameid, pinocchio::ReferenceFrame::LOCAL);
            // // pinocchio::ReferenceFrame::WORLD);
            // tf::twistEigenToMsg(xd.toVector(), twistmsg);
            tf::twistEigenToMsg(current_twist, twistmsg);
            current_velocity_se3_publisher.msg_.header.stamp = rosnow;
            current_velocity_se3_publisher.msg_.twist = twistmsg;);
    publock(min_velocity_se3_publisher, geometry_msgs::Twist twistmsg;
            min_velocity_se3_publisher.msg_.header.frame_id = "local";
            tf::twistEigenToMsg(mpcprob->u_max().min, twistmsg);
            min_velocity_se3_publisher.msg_.header.stamp = rosnow;
            min_velocity_se3_publisher.msg_.twist = twistmsg;);
    publock(max_velocity_se3_publisher, geometry_msgs::Twist twistmsg;
            max_velocity_se3_publisher.msg_.header.frame_id = "local";
            tf::twistEigenToMsg(mpcprob->u_max().max, twistmsg);
            max_velocity_se3_publisher.msg_.header.stamp = rosnow;
            max_velocity_se3_publisher.msg_.twist = twistmsg;);
    publock(desired_velocity_se3_publisher, geometry_msgs::Twist twistmsg;
            // desired_velocity_se3_publisher.msg_.header.frame_id = "world";
            // tf::twistEigenToMsg(cartesian_velocity, twistmsg);
            tf::twistEigenToMsg(cartesian_velocity_local, twistmsg);
            desired_velocity_se3_publisher.msg_.twist = twistmsg;
            desired_velocity_se3_publisher.msg_.header.frame_id = "local";
            desired_velocity_se3_publisher.msg_.header.stamp = rosnow;);
    publock(current_acceleration_se3_publisher, geometry_msgs::Twist twistmsg;
            current_acceleration_se3_publisher.msg_.header.frame_id = "local";
            tf::twistEigenToMsg(current_twist_acc, twistmsg);
            current_acceleration_se3_publisher.msg_.header.stamp = rosnow;
            current_acceleration_se3_publisher.msg_.twist = twistmsg;);
    publock(desired_acceleration_se3_publisher, geometry_msgs::Twist twistmsg;
            desired_acceleration_se3_publisher.msg_.header.frame_id = "local";
            tf::twistEigenToMsg(cartesian_acceleration_localpub, twistmsg);
            desired_acceleration_se3_publisher.msg_.header.stamp = rosnow;
            desired_acceleration_se3_publisher.msg_.twist = twistmsg;);
    publock(desired_jerk_se3_publisher, geometry_msgs::Twist twistmsg;
            desired_jerk_se3_publisher.msg_.header.frame_id = "local";
            tf::twistEigenToMsg(cartesian_jerk_local, twistmsg);
            desired_jerk_se3_publisher.msg_.header.stamp = rosnow;
            desired_jerk_se3_publisher.msg_.twist = twistmsg;);
    publock(
        jointspace_limit_distance_publisher,
        jointspace_limit_distance_publisher.msg_.header.frame_id =
            "joint-space";
        jointspace_limit_distance_publisher.msg_.header.stamp = rosnow;
        jointspace_limit_distance_publisher.msg_.position.resize(model.nv);
        jointspace_limit_distance_publisher.msg_.name.resize(model.nv);

        Eigen::VectorXd pos(model.nv); Eigen::VectorXd vel(model.nv);
        Eigen::VectorXd acc(model.nv);
        pos = (model.upperPositionLimit - current_q)
                  .cwiseMin(current_q - model.lowerPositionLimit);
        for (int i = 0; i < model.nv; ++i) {
          jointspace_limit_distance_publisher.msg_.name[i] =
              std::string("joint") + std::to_string(i + 1);
          jointspace_limit_distance_publisher.msg_.position[i] = pos[i];
          // jointspace_limit_distance_publisher.msg_.velocity.push_back(0);
          // jointspace_limit_distance_publisher.msg_.effort.push_back(0);
        };);
  }

  publock(error_target_pose_se3_publisher,
          auto err_target_pose = pinocchio::log6(
              current_pose.actInv(pinocchio::exp6(target_pose_log)));
          geometry_msgs::Twist twistmsg;
          tf::twistEigenToMsg(err_target_pose, twistmsg);
          error_target_pose_se3_publisher.msg_.header.stamp = rosnow;
          error_target_pose_se3_publisher.msg_.header.frame_id = "world";
          error_target_pose_se3_publisher.msg_.twist = twistmsg;);

  publock(target_pose_se3_publisher, auto target_pose_se3 = target_pose_log;
          geometry_msgs::Twist twistmsg;
          tf::twistEigenToMsg(target_pose_se3, twistmsg);
          target_pose_se3_publisher.msg_.header.stamp = rosnow;
          target_pose_se3_publisher.msg_.header.frame_id = "world";
          target_pose_se3_publisher.msg_.twist = twistmsg;);
  publock(target_pose_publisher, geometry_msgs::Pose xmsg; Eigen::Affine3d x;
          x.matrix() = pinocchio::exp6(target_pose_log).toHomogeneousMatrix();
          tf::poseEigenToMsg(x, xmsg);
          target_pose_publisher.msg_.header.stamp = ros::Time::now();
          target_pose_publisher.msg_.header.frame_id = "world";
          target_pose_publisher.msg_.pose = xmsg;);

  publock(initial_pose_publisher, geometry_msgs::Pose xmsg; Eigen::Affine3d x;
          x.matrix() =
              pinocchio::exp6(current_initial_mpc_pose).toHomogeneousMatrix();
          tf::poseEigenToMsg(x, xmsg);
          initial_pose_publisher.msg_.header.stamp = ros::Time::now();
          initial_pose_publisher.msg_.header.frame_id = "world";
          initial_pose_publisher.msg_.pose = xmsg;);
  // Publish robot desired pose
  publock(desired_pose_publisher, geometry_msgs::PoseStamped X_des_stamp;
          desired_pose_publisher.msg_.header.stamp = ros::Time::now();
          desired_pose_publisher.msg_.header.frame_id = "world";
          desired_pose_publisher.msg_.pose = cartesian_pose_msg;);
  // Publish robot desired velocity
  publock(desired_velocity_world_publisher,
          desired_velocity_world_publisher.msg_.header.stamp = ros::Time::now();
          desired_velocity_world_publisher.msg_.header.frame_id = "world";
          desired_velocity_world_publisher.msg_.twist =
              cartesian_velocity_msg;);
  // Publish robot desired acceleration
  publock(desired_acceleration_world_publisher,
          desired_acceleration_world_publisher.msg_.header.stamp =
              ros::Time::now();
          desired_acceleration_world_publisher.msg_.header.frame_id = "world";
          desired_acceleration_world_publisher.msg_.twist =
              cartesian_acceleration_msg;);
}

double MoveItTrajectory::getTimeProgress() {
  // pinocchio::forwardKinematics(model, data, current_q);
  // pinocchio::updateFramePlacements(model, data);

  // const pinocchio::SE3 current_to_ending =
  //     data.oMf[model.getFrameId(controlled_frame)].actInv(target_pose);
  // Eigen::Matrix<double, 6, 1> current_to_ending_se3 =
  //     pinocchio::log6(current_to_ending).toVector();

  // const pinocchio::SE3 start_to_ending =
  //     starting_pose.actInv(target_pose);
  // Eigen::Matrix<double, 6, 1> start_to_ending_se3 =
  //     pinocchio::log6(start_to_ending).toVector();

  // // auto progress = (current_to_ending_se3.array() /
  // start_to_ending_se3.array()).matrix().norm(); auto delta =
  // (start_to_ending_se3.norm() -
  //               current_to_ending_se3.norm());
  // if (delta < 0){
  //   delta = 0;
  // }
  //   auto progress = delta/ start_to_ending_se3.norm();

  // // ROS_INFO_STREAM_THROTTLE(.1, "current_to_ending: " <<
  // current_to_ending_se3.transpose());
  // // ROS_INFO_STREAM_THROTTLE(.1, "  start_to_ending: " <<
  // start_to_ending_se3.transpose()); ROS_INFO_STREAM_THROTTLE(.1,
  // "*******************************************"); ROS_INFO_STREAM_THROTTLE(
  //     .1, "delta:" << delta);
  // ROS_INFO_STREAM_THROTTLE(
  //     .1, "start_to_ending_se3.norm():" << start_to_ending_se3.norm());
  // ROS_INFO_STREAM_THROTTLE(.1,
  //                          "current_to_ending_se3.norm():"<<
  //                          current_to_ending_se3.norm());
  // ROS_INFO_STREAM_THROTTLE(
  //     .1, "         progress: "
  //             << (current_to_ending_se3.array() /
  //             start_to_ending_se3.array())
  //                    .matrix()
  //                    .transpose());
  // ROS_INFO_STREAM_THROTTLE(.1, "             norm: " << progress);
  // ROS_INFO_STREAM_THROTTLE(.1, "traj progress: " << std::setprecision(2)
  //                                                << progress * 100 << "%");
  // return progress;

  // double traj_duration = mpcprob->H * mpcprob->ndt * mpcprob->dt;
  double traj_duration = 1;
  if (t_traj < traj_duration) {

    double progress = t_traj / traj_duration;
    // std::stringstream ss;
    // ss << "traj progress: " << std::setprecision(2) << progress * 100 <<
    // "%"; ROS_INFO_THROTTLE(.1, ss.str());
    ROS_INFO_STREAM_THROTTLE(.1, "t_traj: " << t_traj << "s");
    ROS_INFO_STREAM_THROTTLE(.1, "traj progress: " << std::setprecision(2)
                                                   << progress * 100 << "%");
    return progress;
  }
  return 1;
}
