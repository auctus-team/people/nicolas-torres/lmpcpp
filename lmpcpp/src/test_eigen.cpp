#include <Eigen/Dense>
#include <Eigen/Geometry>
#include <Eigen/Sparse>
#include <Eigen/src/Core/Matrix.h>
#include <chrono>
#include <cmath>
#include <iomanip>
#include <iostream>

using namespace std;

constexpr double PI = 3.14159265359;
constexpr int m = 2; // % Must be 2 or 3 for plotting
constexpr int n = 5; //% Must be greater than m
using mymatrix = Eigen::Matrix<double, n, m, Eigen::RowMajor>;
using mycolvectorn = Eigen::Matrix<double, n, 1>;
using mycolvectorm = Eigen::Matrix<double, m, 1>;
using Vector6d = Eigen::Matrix<double, 6, 1>;

template <typename T> void print_size(std::string str, T mat) {
  std::cout << str << " " << mat.rows() << " x " << mat.cols() << std::endl;
}

struct TestA {
  TestA() {
    print_size("Matrix a:", a);
    print_size("Vector b:", a);
  }
  Eigen::MatrixXd a = Eigen::MatrixXd::Zero(0, 0);
  Eigen::VectorXd b = Eigen::VectorXd::Zero(0);
};

Eigen::Vector3d skew_cross3(Eigen::Vector3d x, Eigen::Vector3d y) {
  Eigen::Vector3d res;
  res(0) = -x(2) * y(1) + x(1) * y(2);
  res(1) = x(2) * y(0) - x(0) * y(2);
  res(2) = -x(1) * y(0) + x(0) * y(1);
  return res;
}

Vector6d brse3(Vector6d x, Vector6d y) {
  auto lin1 = x.head<3>();
  auto ang1 = x.tail<3>();
  auto lin2 = y.head<3>();
  auto ang2 = y.tail<3>();

  Vector6d res;
  res << ang1.cross(lin2) - ang2.cross(lin1), ang1.cross(ang2);
  return res;
}

// TODO: dumb-per column implementation
Eigen::MatrixXd rcircshift(Eigen::MatrixXd toShift, unsigned int shift) {
  // right circular shift
  unsigned int cols = toShift.cols();
  cout << "rcircshift::cols:" << cols << endl;
  for (int i = shift; i > 0; i -= 1) {
    // cout << "rcircshift::col(" << i << "):" << endl<<toShift.col(i) << endl;
    cout << "rcircshift::i:" << i << endl;
    for (unsigned int k = 0; k < cols - 1; k += 1) {
      toShift.col(0).swap(toShift.col(k + 1));
    }
    cout << toShift << endl;
  }
  return toShift;
}

Eigen::MatrixXd block_rcircshift(Eigen::MatrixXd toShift, unsigned int shift) {
  // right circular shift
  unsigned int cols = toShift.cols();
  unsigned int rows = toShift.rows();
  // cout << "toShift(" << rows << "," << cols << ")" << endl;
  // cout << "shift:" << cols - shift << endl;
  // cout << "cols-shift:" << cols - shift << endl;
  Eigen::MatrixXd shifted(rows, cols);
  // cout << "shifted(" << shifted.rows() << "," << shifted.cols() << ")" <<
  // endl;
  shifted << toShift.block(0, cols - shift, rows, shift),
      toShift.block(0, 0, rows, cols - shift);
  return shifted;
}

void _update_A_underline1(unsigned int H, Eigen::MatrixXd A) {

  unsigned int cols = A.cols();
  unsigned int rows = A.rows();
  Eigen::MatrixXd A_underline(Eigen::MatrixXd(H * rows, H * cols));
  {
    unsigned int c = 0, r = 0;
    for (c = 0, r = 0; c < H * cols; c += cols) {
      A_underline.block(r, c, rows, cols) = A;
      r += rows;
    }
  }
}

int main() {
  cout << "*******************************************************" << endl;
  cout << "                  Hello World!" << endl;

  cout << "static Matrix:" << endl;
  cout << Eigen::Matrix<double, 3, 4>::Identity() << endl;

  Eigen::MatrixXd mymat(Eigen::MatrixXd::Identity(3, 5));
  cout << "dynamic MatrixXd:" << endl;
  cout << mymat << endl;

  cout << "right circ shifting..." << endl;
  // auto rcircshifted = rcircshift(mymat, 2);
  auto rcircshifted = rcircshift(Eigen::MatrixXd::Identity(3, 5), 2);
  cout << "right circ shifted 2" << endl;
  cout << rcircshifted << endl;

  cout << "block right circ shifting..." << endl;
  auto brcircshifted = block_rcircshift(Eigen::MatrixXd::Identity(3, 5), 2);
  cout << "block right circ shifted 2" << endl;
  cout << brcircshifted << endl;

  cout << "Random(2,3)" << endl;
  cout << Eigen::MatrixXd::Random(2, 3) << endl;

  auto myblock = Eigen::MatrixXd::Random(2, 3);
  unsigned int H = 3;
  auto cols = myblock.cols();
  auto rows = myblock.rows();
  Eigen::MatrixXd myzero = Eigen::MatrixXd::Zero(H * rows, H * cols);
  cout << "Zero(" << rows << "," << cols << ") with block diag" << endl;
  {
    unsigned int c = 0, r = 0;
    for (c = 0, r = 0; c < H * cols; c += cols) {
      // cout<<"c:"<<c<<"->"<<c+cols<<"  r:"<<r<<"->"<<r+rows<<endl;
      // cout << "myzero.block:" << endl
      // << myzero.block(r, c, rows, cols) << endl;
      myzero.block(r, c, rows, cols) = myblock;
      // cout << "myzero:" << endl << myzero << endl;
      r += rows;
    }
  }
  cout << myzero << endl;

  Eigen::MatrixXd hstack(myblock.rows(), 2 * myblock.cols());
  hstack << myblock, myblock;
  Eigen::MatrixXd vstack(2 * myblock.rows(), myblock.cols());
  vstack << myblock, myblock;
  cout << "hstack:" << endl;
  cout << hstack << endl;
  cout << "vstack:" << endl;
  cout << vstack << endl;
  cout << "matrix.block(0,0,0,0):" << endl;
  cout << vstack.block(0, 0, 0, 0) << endl;
  cout << "assigning a (0,0) matrix:" << endl;
  vstack.block(0, 0, 0, 0) = Eigen::MatrixXd::Random(0, 0);
  cout << vstack << endl;

  Eigen::MatrixXd qstack(2 * myblock.rows(), 2 * myblock.cols());
  qstack << myblock, myblock, myblock, myblock;
  cout << "qstack (stack when there's no similarity):" << endl;
  cout << qstack << endl;

  cout << "myblock.replicate(3,2):" << endl;
  cout << myblock.replicate(3, 2) << endl;

  Eigen::SparseMatrix<double> mysparse = qstack.sparseView();
  // mysparse.makeCompressed();
  cout << "sparse compressed mysparse:" << endl;
  cout << mysparse << endl;
  // mymatrix A_mid = mymatrix::Random().array() - 0.5;
  // cout << "A_mid:" << endl << A_mid << endl;

  // using SE3Matrix = Eigen::Matrix<double, 4, 4, Eigen::RowMajor>;
  // SE3Matrix T = SE3Matrix::Random().array();
  // cout << "T:" << endl << T << endl;

  {
    unsigned int H = 1000;
    Eigen::MatrixXd A(Eigen::MatrixXd::Random(10, 10));
    auto startt = std::chrono::steady_clock::now();
    _update_A_underline1(H, qstack);
    auto total = std::chrono::duration_cast<std::chrono::nanoseconds>(
                     std::chrono::steady_clock::now() - startt)
                     .count();
    cout << "time "
         << "_update_A_underline1"
         << ":" << total / 1000000 << "ms "
         << "(H:" << H << ", A:" << A.rows() << "x" << A.cols() << ")" << endl;
  }

  {
    cout << "Test 0-size matrix and vectors:" << endl;
    TestA a;
  }

  {
    Eigen::VectorXd vel = Eigen::VectorXd::Zero(3);
    Eigen::VectorXd vel_max = Eigen::VectorXd::Ones(3);
    Eigen::VectorXd acc_max = 10 * Eigen::VectorXd::Ones(3);

    std::cout << "vel:" << vel.transpose() << std::endl;
    std::cout << "vel_max:" << vel_max.transpose() << std::endl;
    std::cout << "acc_max:" << acc_max.transpose() << std::endl;

    double dt = 1e-3;
    Eigen::VectorXd lb = -vel_max.cwiseMax(-acc_max * dt + vel);
    std::cout << "-vel_max.cwiseMax(acc_max...):" << lb.transpose()
              << std::endl;
    lb = (-vel_max).cwiseMax(-acc_max * dt + vel);
    std::cout << "(-vel_max).cwiseMax(acc_max...):" << lb.transpose()
              << std::endl;
  }
}
