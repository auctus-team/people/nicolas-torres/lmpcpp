#pragma once

#include <Eigen/Dense>

namespace lmpcpp {
struct TimedPose {
  double t{0};
  Eigen::Matrix4d pose{Eigen::Matrix4d::Zero()};

  friend bool operator==(const TimedPose &lhs, const TimedPose &rhs);
  friend bool operator!=(const TimedPose &lhs, const TimedPose &rhs);
};

bool operator==(const TimedPose &lhs, const TimedPose &rhs);
bool operator!=(const TimedPose &lhs, const TimedPose &rhs);

struct Goal {
  TimedPose start;
  Eigen::Matrix4d pose{Eigen::Matrix4d::Zero()};
  Eigen::VectorXd u0{Eigen::VectorXd::Zero(6)};
  Eigen::VectorXd ud0{Eigen::VectorXd::Zero(6)};

  friend bool operator==(const Goal &lhs, const Goal &rhs);
  friend bool operator!=(const Goal &lhs, const Goal &rhs);
};

bool operator==(const Goal &lhs, const Goal &rhs);
bool operator!=(const Goal &lhs, const Goal &rhs);

} // namespace lmpcpp
