#pragma once

#include "moveit_trajectory_interface/moveit_trajectory.hpp"
#include "moveit_trajectory_interface/moveit_trajectory_action_server.hpp"

class TrajectoryInterface {
public:
  TrajectoryInterface();
  void action_thread(std::shared_ptr<MoveItTrajectory> interface);
  std::shared_ptr<MoveItTrajectory> interface;
};
