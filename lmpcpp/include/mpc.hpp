#pragma once

#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <Eigen/src/Core/Matrix.h>
#include <atomic>
#include <chrono>
#include <mutex>
#include <string>
#include <thread>
#include <tuple>
#include <vector>

#include <pinocchio/spatial/explog.hpp>
#include <pinocchio/spatial/se3.hpp>
#include <pinocchio/spatial/skew.hpp>

#include "horizon.hpp"
#include "methods.hpp"
#include "mpc_matrices.hpp"
#include "poses.hpp"
#include "qp_problem.hpp"
#include <fstream>
#include <future>
#include <iostream>
#include <memory>

#define STRINGIFY(x) #x
#define MACRO_STRINGIFY(x) STRINGIFY(x)

#define lmpcpp_default_solver "osqp"

namespace lmpcpp {

// const unsigned int polytope_rows = 21;

// Eigen::MatrixXd Ginv(const Eigen::Vector3d &so3);

// pinocchio::Motion closest_log6(const pinocchio::SE3 &val,
//                                const pinocchio::SE3 &ref);

using UniqueHorizon = std::unique_ptr<Horizon>;

class Planifier {
public:
  enum State {
    OK,
    Computing,
    Failed,
    NonInitialized,
    Waiting,
    Ending,
  };

  static const std::string StateToString(const State s);

  Planifier(HorizonParameters params,
            std::string solver = lmpcpp_default_solver);
  // ~Planifier();

  Planifier(Planifier const &other);

  State status() const;

  c_int test(Eigen::VectorXd x0 = Eigen::VectorXd::Zero(0),
             Eigen::MatrixXd A = Eigen::MatrixXd::Zero(0, 0));
  void test2(Eigen::VectorXd x0, Eigen::MatrixXd A);

  std::future<UniqueHorizon> solve_async(const Goal);

  std::future<UniqueHorizon> solve_async(const Goal,
                                         const SolveParameters solveparams);

  Horizon solve(const Goal, const SolveParameters solveparams);

  // **************************************************
  // redirecting calls
  void update_cost_ponderation(const CostPonderation ponderation);
  void update_solve_parameters(const SolveParameters solveparams);
  void set_compute_time(double tt);
  void set_use_origin_dlog(bool val);
  bool use_origin_dlog() const;

  double compute_time_limit() const;

  void print_matrices() const;
  VectorLimits u_max() const;
  VectorLimits ud_max() const;
  VectorLimits udd_max() const;
  void set_wreg1(double w);
  double get_wreg1() const;
  void set_wreg2(double w);
  double get_wreg2() const;
  double t_horizon(double t) const;
  double horizon_duration() const;
  double input_horizon_duration() const;

  void update_cartvel_polytope(const VectorLimits qlims,
                               const VectorLimits qdlims,
                               const Eigen::VectorXd current_q,
                               const double polytope_horizon,
                               const Eigen::MatrixXd J);

  void update_obstacle(const pinocchio::SE3 obs_pose, const double r);

  const HorizonParameters params;
  std::string solver;

  // TODO all these should be private
  MPCMatrices mpcmat;
  std::unique_ptr<QPProblem> qpprob;
  LinearInequalityConstraints polytope_linconstraints;
  LinearInequalityConstraints obstacle_linconstraints;

private:
  void _solve(std::promise<UniqueHorizon> &&promise, const Goal goal);

  std::atomic<State> _status;
  unsigned int iter = 0;
  Horizon _horizon;

  mutable std::mutex _mutex; // protects _mpcmat

  bool _use_origin_dlog = false;
};

} // namespace lmpcpp
