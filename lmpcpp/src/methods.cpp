#include "methods.hpp"
#include "polycap.hpp"

#include <cmath>
#include <iostream>
#include <pinocchio/spatial/explog.hpp>
#include <pinocchio/spatial/se3.hpp>

using namespace lmpcpp;

std::string lmpcpp::str_val(std::string str, double val) {
  return str + ":" + std::to_string(val);
}

void lmpcpp::print_error(const std::stringstream &ss) {
  std::cout << "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx" << std::endl;
  std::cout << "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx" << std::endl;
  std::cout << ss.str();
  std::cout << "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx" << std::endl;
  std::cout << "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx" << std::endl;
}

std::string lmpcpp::str_size(std::string str, Eigen::MatrixXd mat) {
  std::stringstream ss;
  ss << str << " " << mat.rows() << " x " << mat.cols();
  return ss.str();
}

void lmpcpp::print_size(std::string str, Eigen::MatrixXd mat) {
  std::cout << str_size(str, mat) << std::endl;
}

std::string lmpcpp::str_mat(std::string str, Eigen::MatrixXd mat) {
  std::stringstream ss;
  ss << str_size(str, mat) << std::endl;
  if (mat.cols() == 1) {
    ss << mat.transpose() << std::endl;
  } else {
    ss << mat << std::endl;
  }
  return ss.str();
}

void lmpcpp::print_mat(std::string str, Eigen::MatrixXd mat) {
  std::cout << str_mat(str, mat) << std::endl;
}

Eigen::MatrixXd lmpcpp::block_rcircshift(Eigen::MatrixXd toShift,
                                         unsigned int shift) {
  // right circular shift
  unsigned int cols = toShift.cols();
  unsigned int rows = toShift.rows();
  Eigen::MatrixXd shifted(rows, cols);
  shifted << toShift.block(0, cols - shift, rows, shift),
      toShift.block(0, 0, rows, cols - shift);
  return shifted;
}

VectorLimits lmpcpp::qdmax_from_qqlims(const VectorLimits q,
                                       const VectorLimits qd,
                                       const Eigen::VectorXd current_q,
                                       const double polytope_horizon) {
  return {(-qd.min).cwiseMin((q.min - current_q) / polytope_horizon),
          qd.max.cwiseMin((q.max - current_q) / polytope_horizon)};
}

LinearInequalityConstraints
lmpcpp::compute_Jqd_halfplane(const VectorLimits qd, const Eigen::MatrixXd J,
                              const unsigned int nu,
                              const unsigned int Jqd_polytope_rows) {

  // polytope capacity
  // double polytope_horizon = 250*mpcprob.dt; // [s]
  // double polytope_horizon = compute_time_limit() + 0.050; // [s]
  // double polytope_horizon = delta_time + 0.050; // [s]
  // ROS_ERROR_STREAM_THROTTLE(5, "polytope_horizon: " <<
  // polytope_horizon*1000<<"ms"); double polytope_horizon = 1; // [s]

  // ROS_INFO_STREAM("model.velocityLimit.cwiseMin");

  // pinocchio::Data::Matrix6x J;
  // TODO: fix ref_pose
  // J = ref_pose.toActionMatrix() * J;

  Eigen::MatrixXd H;
  Eigen::VectorXd d;

  // ROS_INFO_STREAM("hyper_plane_shift_method");
  hyper_plane_shift_method(H, d, J, qd.min, qd.max);

  // ROS_INFO_STREAM("mpcprob.H: " << mpcprob.H);
  // print_sizes(H, "H");
  // print_sizes(d, "d");

  if (H.cols() != nu) {
    std::cout << "H.cols() != nu: " << H.cols() << " != " << nu << std::endl;
  }
  if (H.rows() != 2 * Jqd_polytope_rows) {
    std::cout << "H.rows() != 2*Jqd_polytope_rows: " << H.rows()
              << " != " << 2 * Jqd_polytope_rows << std::endl;
  }

  Eigen::MatrixXd Hf(Eigen::MatrixXd::Zero(Jqd_polytope_rows, H.cols()));
  Eigen::VectorXd ld(Eigen::VectorXd::Zero(Jqd_polytope_rows));
  Eigen::VectorXd ud(Eigen::VectorXd::Zero(Jqd_polytope_rows));
  // print_sizes(Hf, "setting Hf");
  // print_sizes(ld, "setting ld");
  // print_sizes(ud, "setting ud");
  for (unsigned int row = 0; row < Jqd_polytope_rows; row++) {
    Hf.block(row, 0, 1, Hf.cols()) = H.block(2 * row, 0, 1, H.cols());
    ud(row) = d(2 * row);
    ld(row) = -d(2 * row + 1);
  }

  // Eigen::MatrixXd A_lin(Eigen::MatrixXd::Zero(
  //     A_lin_acc.rows() + (params.H - 1) * Hf.rows(), A_lin_acc.cols()));
  // Eigen::MatrixXd HH(Eigen::MatrixXd::Zero(Jq_polytope_rows,
  // A_lin.cols()));
  // // print_sizes(HH, "setting HH");
  // HH.block(0, params.nx_hat, Hf.rows(), Hf.cols()) = Hf;

  return {Hf, {ld, ud}};
}

Eigen::VectorXd lmpcpp::jexp_dist(const Eigen::Matrix4d cur_pose_mat,
                                  const Eigen::Matrix4d obs_pose_mat) {
  pinocchio::SE3 cur_pose(cur_pose_mat);
  pinocchio::SE3 obs_pose(obs_pose_mat);
  pinocchio::SE3 obs_pose_with_curR(cur_pose.rotation(),
                                    obs_pose.translation());
  auto obs_pose_with_curR_log = pinocchio::log6(obs_pose_with_curR);

  Eigen::MatrixXd jexp(6, 6);
  pinocchio::Jexp6(obs_pose_with_curR_log, jexp);

  return jexp * (obs_pose_with_curR_log.toVector() -
                 pinocchio::log6(cur_pose).toVector());
}

LinearInequalityConstraints
lmpcpp::mindistance_obstacle(const Eigen::Matrix4d obs_pose_mat,
                             const double r) {
  pinocchio::SE3 cur_pose(obs_pose_mat);
  // NOTE: obstacle pose must be aligned with the starting pose of the robot
  auto pose_obs_zeroR = pinocchio::SE3(obs_pose_mat);
  auto pose_obs_zeroR_log = pinocchio::log6(pose_obs_zeroR);

  // JEXP
  // Eigen::MatrixXd jexp(6, 6);
  // pinocchio::Jexp6(pose_obs_zeroR_log, jexp);
  // // take only linear constraints
  // LinearInequalityConstraints cons;
  // cons.A = jexp.block(2, 0, 1, 6);
  // std::cout << "jexp * pose_obs_zeroR_log + r:"
  //           << (jexp * pose_obs_zeroR_log.toVector()).block(2, 0, 1, 1) +
  //                  Eigen::VectorXd::Constant(1, r)
  //           << std::endl;
  // // TODO: is there a better way to establish max?
  // cons.b.max = Eigen::VectorXd::Constant(1, 100);
  // cons.b.min = (jexp * pose_obs_zeroR_log.toVector()).block(2, 0, 1, 1) +
  //              Eigen::VectorXd::Constant(1, r);

  // JLOG
  Eigen::MatrixXd jlog(6, 6);
  Eigen::VectorXd rz(6);
  rz << 0, 0, r, 0, 0, 0;
  pinocchio::Jlog6(pose_obs_zeroR, jlog);
  Eigen::MatrixXd iden6(Eigen::MatrixXd::Identity(6, 6));
  // take only linear constraints
  LinearInequalityConstraints cons;
  cons.A = iden6.block(2, 0, 1, 6);
  std::cout << "iden6 * pose_obs_zeroR_log + r:"
            << (iden6 * pose_obs_zeroR_log.toVector()).block(2, 0, 1, 1) +
                   (jlog * rz).block(2, 0, 1, 1)
            << std::endl;
  // TODO: is there a better way to establish max?
  cons.b.max = Eigen::VectorXd::Constant(1, 100);
  cons.b.min = (iden6 * pose_obs_zeroR_log.toVector()).block(2, 0, 1, 1) +
               (jlog * rz).block(2, 0, 1, 1);

  return cons;
}

SquareMatrix6d lmpcpp::dlog(const Vector6d &x) {
  // const auto &lin = x.head<3>();
  // const auto &ang = x.tail<3>();
  // SquareMatrix6d dexp0(SquareMatrix6d::Zero());
  // pinocchio::skew(ang, dexp0.block<3, 3>(0, 0));
  // pinocchio::skew(lin, dexp0.block<3, 3>(0, 3));
  // // pinocchio::skew(ang, dexp0.block<3, 3>(3, 3));
  // dexp0.block<3, 3>(3, 3) = dexp0.block<3, 3>(0, 0);
  // return SquareMatrix6d::Identity() + dexp0 / 2 + dexp0 * dexp0 / 12;

  Eigen::MatrixXd temp(6, 6);
  pinocchio::Jlog6(pinocchio::exp6(x), temp);
  // pinocchio::Jexp6(pinocchio::Motion(x), temp);
  return temp;
}

Vector6d lmpcpp::brse3(const Vector6d &x, const Vector6d &y) {
  const auto &lin1 = x.head<3>();
  const auto &ang1 = x.tail<3>();
  const auto &lin2 = y.head<3>();
  const auto &ang2 = y.tail<3>();

  Vector6d res;
  res << ang1.cross(lin2) - ang2.cross(lin1), ang1.cross(ang2);
  return res;
}

// SquareMatrix6d dexp2(const Vector6d &x) {
//   Eigen::MatrixXd temp(6, 6);
//   pinocchio::Hlog6(pinocchio::exp6(x), temp);
//   // pinocchio::Jexp6(pinocchio::Motion(x), temp);
//   return temp;
// }

double
lmpcpp::tock_ms(std::chrono::time_point<std::chrono::steady_clock> start) {
  return std::chrono::duration_cast<std::chrono::nanoseconds>(
             std::chrono::steady_clock::now() - start)
             .count() /
         1000000.;
}

std::chrono::time_point<std::chrono::steady_clock> lmpcpp::precise_now() {
  return std::chrono::steady_clock::now();
}

///////////////////////////////////////////////////////
// Delay
Delay::Delay(double delay_s) : delay_s(delay_s) {}

Delay::Delay(const Delay &other)
    : start(other.start), delay_s(other.delay_s),
      initialized(other.initialized) {}

void Delay::print(const std::string &str) {
  if (passed()) {
    std::cout << str;
    refresh();
  }
}

bool Delay::passed() {
  if (!initialized) {
    refresh();
    return true;
  }

  if (tock_ms(start) / 1000 > delay_s) {
    return true;
  }
  return false;
}

void Delay::refresh() {
  initialized = true;
  start = precise_now();
}

///////////////////////////////////////////////////////
// TickTock
TickTock::TickTock(std::string msg, bool print)
    : start(precise_now()), msg(msg), _print(print) {}

TickTock::~TickTock() {
  if (_print) {
    std::cout << "duration " << msg << " :: " << std::setprecision(3)
              << tock_ms(start) << "ms" << std::endl;
  }
}

double TickTock::tock_s() { return tock_ms(start) / 1000.; }

///////////////////////////////////////////////////////
// LPFilter
Eigen::VectorXd LPFilter::filter(Eigen::VectorXd cur, double w0, double dt) {
  if (last.rows() == 0) {
    last.resize(cur.rows(), 1);
    last.setZero();
  }

  if (w0 < 0) {
    return cur;
  }

  double beta = exp(-w0 * dt);
  Eigen::VectorXd filtered = beta * last + (1 - beta) * cur;
  last = cur;
  return cur;
}
