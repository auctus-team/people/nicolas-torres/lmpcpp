#include "qp_problem.hpp"
#include <iostream>
#include <osqp.h>
#include <sstream>

using namespace lmpcpp;

//////////////////////////////////////////////////////
// QPProblem
QPProblem::QPProblem(unsigned int nz) : nz(nz) {}

double QPProblem::compute_time_limit() { return compute_time; };

void QPProblem::set_compute_time(double tt) { compute_time = tt; }

void QPProblem::update_cost_function(const Eigen::MatrixXd &a,
                                     const Eigen::MatrixXd &Hreg) {
  _cost_a = a;
  _Hreg = Hreg;
}

void QPProblem::mark_initialized() { initialized = true; }

//////////////////////////////////////////////////////
// OSQPProblem
OSQPProblem::OSQPProblem(unsigned int nz) : QPProblem(nz), P(nullptr, &c_free) {
  // TickTock temp("osqp_set_default_settings");
  osqp_set_default_settings(&osqp_settings);
  // osqp_settings.polish = false;
  osqp_settings.polish = true;
  osqp_settings.polish_refine_iter = 2;
  osqp_settings.max_iter = 100000;
  osqp_settings.verbose = false;
  // osqp_settings.verbose = true;
  osqp_settings.eps_abs = 1e-5;    // 1e-5;
  osqp_settings.eps_rel = 1e-5;    // 1e-5;
  osqp_settings.warm_start = true; // NOTE: default is true

  osqp_settings.time_limit = compute_time_s;

  // TODO: remove debug time limits
  // osqp_settings.time_limit = 100; // [s], yes, it's a lot! (for
  // debugging)
  // osqp_settings.time_limit = 200e-3; // [s]
  // osqp_settings.time_limit = 1; // [s]
}
// OSQPProblem::~OSQPProblem(){
// Cleanup
// source: https://osqp.org/docs/examples/setup-and-solve.html#c
// TODO: osqp_cleanup actually calls free for everything so osqp_data
// would need to become a pointer (or just don't call osqp_cleanup)
// https://github.com/osqp/osqp/blob/dc49cc0ab6a27011ccd9f1817f9719a09908b071/src/osqp.c#L659
// std::cout << "osqp_cleanup(osqp_workspace)" << std::endl;
// osqp_cleanup(osqp_workspace);
// if (osqp_data.A) {
//   std::cout << "free(osqp_data.A)" << std::endl;
//   c_free(osqp_data.A);
// }
// if (osqp_data.P) {
//   std::cout << "free(osqp_data.P)" << std::endl;
//   c_free(osqp_data.P);
// }
// }

void OSQPProblem::set_compute_time(double tt) {
  QPProblem::set_compute_time(tt);
  osqp_settings.time_limit = tt;
}

void OSQPProblem::update_cost_function(const Eigen::MatrixXd &a,
                                       const Eigen::MatrixXd &Hreg) {
  QPProblem::update_cost_function(a, Hreg);

  TickTock temp("complete update_cost_function");
  {
    // TickTock temp("update_cost_function:: --> QP matrix Q");
    Q.noalias() = 2 * a.transpose() * a + Hreg;

    // NOTE: it seems this is actually slower
    // Q.setZero();
    // Q.selfadjointView<Eigen::Lower>().rankUpdate(a.transpose());
    // Q.selfadjointView<Eigen::Upper>().rankUpdate(Q.transpose());

    // Q += 2 * w_reg * Eigen::MatrixXd::Identity(nz, nz);
    // Q += 2 * w_reg * mpcmat.Fu_underline.transpose() * mpcmat.Fu_underline;

    print_size("update_cost_function::Q:", Q);
  }

  // Eigen::Matrix<c_int, Eigen::Dynamic, 1> P_row_ind;
  // Eigen::Matrix<c_int, Eigen::Dynamic, 1> P_col_ind;
  // Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> U(
  //     Q.rows(), Q.cols());
  {
    // TickTock temp("update_cost_function:: sparse osqp_data.P");
    U.resize(Q.rows(), Q.cols());
    U = Q.triangularView<Eigen::UpLoType::Upper>();
    Q_sparse = U.sparseView();
    Q_sparse.makeCompressed();
    P_row_ind = Eigen::Map<Eigen::VectorXi>(Q_sparse.innerIndexPtr(),
                                            Q_sparse.nonZeros())
                    .cast<c_int>();
    P_col_ind = Eigen::Map<Eigen::VectorXi>(Q_sparse.outerIndexPtr(),
                                            Q_sparse.outerSize() + 1)
                    .cast<c_int>();
    // if (!P) {
    // std::cout << "free(P)" << std::endl;
    // c_free(P);
    // }

    P.reset(csc_matrix(Q_sparse.rows(), Q_sparse.cols(), Q_sparse.nonZeros(),
                       Q_sparse.valuePtr(), P_row_ind.data(),
                       P_col_ind.data()));

    if (initialized) {
      // TODO: this is assuming sparsity doesn't change, if it does,
      // we'd need to call osqp_setup again
      osqp_update_P(osqp_workspace, Q_sparse.valuePtr(), OSQP_NULL,
                    Q_sparse.nonZeros());
    }
  }
}

int OSQPProblem::solve(const Eigen::MatrixXd cost_a, const Eigen::MatrixXd Hreg,
                       const Eigen::MatrixXd cost_b, const Eigen::MatrixXd Alin,
                       Eigen::VectorXd Alin_min, Eigen::VectorXd Alin_max) {
  update_cost_function(cost_a, Hreg);
  return solve(cost_b, Alin, Alin_min, Alin_max);
}

int OSQPProblem::solve(const Eigen::MatrixXd cost_b, const Eigen::MatrixXd Alin,
                       Eigen::VectorXd Alin_min, Eigen::VectorXd Alin_max) {

  // std::cout << "-> nz:" << nz << std::endl;
  // lmpcpp::print_size("cost_b.transpose():", cost_b.transpose());
  // lmpcpp::print_size("cost_a:", _cost_a);
  // lmpcpp::print_size("cost_b^T * _cost_a:", cost_b.transpose() * _cost_a);
  // Eigen::VectorXd g(Eigen::VectorXd::Zero(nz));
  Eigen::VectorXd g;
  // lmpcpp::print_size("g:", g);
  {
    // TickTock temp("QP matrix g");
    // g.noalias() = -2 * cost_b.transpose() * _cost_a;
    // TODO: WTF, why is it inverted in the refactoring?
    g.noalias() = -2 * _cost_a.transpose() * cost_b;
  }

  // setup QP in OSQP
  Eigen::Matrix<c_int, Eigen::Dynamic, 1> A_row_ind;
  Eigen::Matrix<c_int, Eigen::Dynamic, 1> A_col_ind;

  {
    // TickTock temp("sparse osqp_data.A");
    Alin_sparse = Alin.sparseView();
    Alin_sparse.makeCompressed();
    A_row_ind = Eigen::Map<Eigen::VectorXi>(Alin_sparse.innerIndexPtr(),
                                            Alin_sparse.nonZeros())
                    .cast<c_int>();
    A_col_ind = Eigen::Map<Eigen::VectorXi>(Alin_sparse.outerIndexPtr(),
                                            Alin_sparse.outerSize() + 1)
                    .cast<c_int>();
    osqp_data.A = csc_matrix(Alin_sparse.rows(), Alin_sparse.cols(),
                             Alin_sparse.nonZeros(), Alin_sparse.valuePtr(),
                             A_row_ind.data(), A_col_ind.data());
  }

  if (!initialized) {
    // number of constraints
    // m =
    //    nx_overline  | from A_und X_und + B_und U_und - X_over = 0
    // +  nx           | from x0
    // +  H*nx         | from H state variable bounds (x_min, x_max)
    // +  H*nu         | from H input variable bounds (u_min, u_max)
    // osqp_data.m = nx_overline + nx + H*nx + H*nu;
    {
      // TickTock temp("osqp_data P,q,m,l,u,n");
      osqp_data.P = P.get();
      osqp_data.q = g.data();
      osqp_data.m = Alin.rows();
      osqp_data.l = Alin_min.data();
      osqp_data.u = Alin_max.data();
      osqp_data.n = nz; // number of variables
    }

    // std::cout << "++++++++++++++++++++++++++++++++++++" << std::endl;
    // std::cout << "++++++++++++++++++++++++++++++++++++" << std::endl;
    // print_size("Alin_min:", Alin_min);
    // print_size("Alin_max:", Alin_max);
    // std::cout << "Alin_min:" << std::endl << Alin_min.transpose() <<
    // std::endl; std::cout << "Alin_max:" << std::endl << Alin_max.transpose()
    // << std::endl; if (Alin_min.rows() > 180) {
    //   std::cout << "Alin_min.segment(180,10):" << std::endl
    //             << Alin_min.segment(180, 10).transpose() << std::endl;
    //   std::cout << "Alin_max.segment(180,10):" << std::endl
    //             << Alin_max.segment(180, 10).transpose() << std::endl;
    // }
    // std::cout << "++++++++++++++++++++++++++++++++++++" << std::endl;
    // std::cout << "++++++++++++++++++++++++++++++++++++" << std::endl;

    {
      // TickTock temp("osqp_setup");
      c_int ret = osqp_setup(&osqp_workspace, &osqp_data, &osqp_settings);
      if (0 != ret) {
        std::stringstream ss;
        ss << "Error: osqp_setup() FAILED" << std::endl;
        ss << "Error: osqp_setup() FAILED" << std::endl;
        ss << "ret:" << ret << std::endl;
        ss << "g:" << g.transpose() << std::endl;
        print_error(ss);
        return ret;
      }
    }
    mark_initialized();
  } else {
    {
      // TickTock temp("osqp_update_...");
      c_int ret = osqp_update_lin_cost(osqp_workspace, g.data());
      if (0 != ret) {
        std::stringstream ss;
        ss << "Error: osqp_update_lin_cost() FAILED" << std::endl;
        ss << "Error: osqp_update_lin_cost() FAILED" << std::endl;
        ss << "ret:" << ret << std::endl;
        ss << str_mat("g:", g) << std::endl;
        print_error(ss);
      }

      ret =
          osqp_update_bounds(osqp_workspace, Alin_min.data(), Alin_max.data());
      if (0 != ret) {
        std::stringstream ss;
        ss << "Error: osqp_update_bounds() FAILED" << std::endl;
        ss << "Error: osqp_update_bounds() FAILED" << std::endl;
        ss << "ret:" << ret << std::endl;
        for (unsigned int i = 0; i < Alin_min.rows(); ++i) {
          if (Alin_min(i) > Alin_max(i)) {
            ss << "Alin_min(" << i << ")>=Alin_max(" << i << "): ";
            ss << Alin_min(i) << ">" << Alin_max(i) << std::endl;
          }
        }
        // ss << str_mat("Alin_min:", Alin_min) << std::endl;
        // ss << str_mat("Alin_max:", Alin_max) << std::endl;
        print_error(ss);
      }
    }
  }

  c_int ret = 0;
  {
    // TickTock temp("--> osqp_solve");
    ret = osqp_solve(osqp_workspace);
  }

  if (ret != 0) {
    std::stringstream ss;
    ss << "Error: osqp::solve() FAILED" << std::endl;
    ss << "Error: osqp::solve() FAILED" << std::endl;
    ss << "ret:" << ret << std::endl;
    print_error(ss);
  }

  return ret;
}

Eigen::VectorXd OSQPProblem::result() {
  // Eigen::VectorXd result(nz);
  // result = Eigen::Map<Eigen::VectorXd>(osqp_workspace->solution->x, nz, 1);
  // return result;
  return Eigen::Map<Eigen::VectorXd>(osqp_workspace->solution->x, nz, 1);
}

//////////////////////////////////////////////////////
// QPOasesProblem

QPOasesProblem::QPOasesProblem(unsigned int nz)
    : QPProblem(nz), bounds{Eigen::VectorXd::Zero(nz),
                            Eigen::VectorXd::Zero(nz)} {
  // bounds.min.Constant(-qpOASES::INFTY);
  // bounds.max.Constant(qpOASES::INFTY);

  // options.setToReliable();
  // options.setToDefault();
  options.setToMPC();
  // options.enableRegularisation = qpOASES::BT_TRUE;
  // options.enableFlippingBounds = qpOASES::BT_FALSE;
  // options.initialStatusBounds = qpOASES::ST_INACTIVE;
  // options.numRefinementSteps = 1e3;
  // options.enableCholeskyRefactorisation = 1;
}

// void update_variable_bounds(VectorLimits varbounds) {
//   bounds = varbounds;
// }

void QPOasesProblem::update_cost_function(const Eigen::MatrixXd &a,
                                          const Eigen::MatrixXd &Hreg) {
  QPProblem::update_cost_function(a, Hreg);

  TickTock temp("complete update_cost_function");
  {
    // TickTock temp("update_cost_function:: --> QP matrix Q");
    H.noalias() = 2 * a.transpose() * a + Hreg;
  }
}

int QPOasesProblem::solve(const Eigen::MatrixXd cost_a,
                          const Eigen::MatrixXd Hreg,
                          const Eigen::MatrixXd cost_b,
                          const Eigen::MatrixXd Alin, Eigen::VectorXd Alin_min,
                          Eigen::VectorXd Alin_max) {
  update_cost_function(cost_a, Hreg);
  return solve(cost_b, Alin, Alin_min, Alin_max);
}

int QPOasesProblem::solve(const Eigen::MatrixXd cost_b,
                          const Eigen::MatrixXd Alin, Eigen::VectorXd Alin_min,
                          Eigen::VectorXd Alin_max) {

  Eigen::VectorXd g;
  // lmpcpp::print_size("g:", g);
  {
    // TickTock temp("QP matrix g");
    // g.noalias() = -2 * cost_b.transpose() * _cost_a;
    // TODO: WTF, why is it inverted in the refactoring?
    g.noalias() = -2 * _cost_a.transpose() * cost_b;
  }

  MatrixXdRowmajor A(Alin);
  qpOASES::returnValue ret;
  int nWSR = max_iterations;
  qpOASES::real_t cputime = compute_time;
  if (!initialized) {
    solver.reset(new qpOASES::SQProblem(nz, Alin.rows(), qpOASES::HST_POSDEF));

    solver->setOptions(options);
    // solver->setPrintLevel(qpOASES::PL_NONE);
    solver->setPrintLevel(qpOASES::PL_LOW);
    // solver->setPrintLevel(qpOASES::PL_HIGH);
    // solver->setPrintLevel(qpOASES::PL_DEBUG_ITER);

    // ret =
    //     solver->init(H.data(), g.data(), Alin.data(), bounds.min.data(),
    //                  bounds.max.data(), Alin_min.data(), Alin_max.data(),
    //                  nWSR);

    ret = solver->init(H.data(), g.data(), A.data(), nullptr, nullptr,
                       Alin_min.data(), Alin_max.data(), nWSR, &cputime);

    if (ret != qpOASES::RET_INIT_FAILED) {
      mark_initialized();
    }
  } else {
    // ret = solver->hotstart(H.data(), g.data(), Alin.data(),
    // bounds.min.data(),
    //                        bounds.max.data(), Alin_min.data(),
    //                        Alin_max.data(), nWSR);
    ret = solver->hotstart(H.data(), g.data(), A.data(), nullptr, nullptr,
                           Alin_min.data(), Alin_max.data(), nWSR, &cputime);
  }

  if ((ret == qpOASES::RET_MAX_NWSR_REACHED) && (nWSR > 0)) {
    std::cout << "Warning: qpOASES RET_MAX_NWSR_REACHED but nWSR > 0 (cputime "
                 "probably exceeded)"
              << std::endl;
    ret = qpOASES::SUCCESSFUL_RETURN;
  }

  if ((ret == qpOASES::RET_INIT_FAILED) ||
      (ret == qpOASES::RET_HOTSTART_FAILED)) {
    std::stringstream ss;
    ss << "Error: qpOASES::solve() FAILED" << std::endl;
    ss << "Error: qpOASES::solve() FAILED" << std::endl;
    ss << "ret:" << (int)ret << std::endl;
    ss << "ret==qpOASES::RET_MAX_NWSR_REACHED:" << std::boolalpha
       << (ret == qpOASES::RET_MAX_NWSR_REACHED) << std::endl;
    print_error(ss);
  }

  return ret;
}

Eigen::VectorXd QPOasesProblem::result() {
  Eigen::VectorXd result(nz);
  solver->getPrimalSolution(result.data());
  return result;
}
