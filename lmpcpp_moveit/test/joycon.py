#!/usr/bin/env python3

# Software License Agreement (BSD License)
#
# Copyright (c) 2013, SRI International
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above
#    copyright notice, this list of conditions and the following
#    disclaimer in the documentation and/or other materials provided
#    with the distribution.
#  * Neither the name of SRI International nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
# Author: Acorn Pooley, Mike Lautman

## BEGIN_SUB_TUTORIAL imports
##
## To use the Python MoveIt interfaces, we will import the `moveit_commander`_ namespace.
## This namespace provides us with a `MoveGroupCommander`_ class, a `PlanningSceneInterface`_ class,
## and a `RobotCommander`_ class. (More on these below)
##
## We also import `rospy`_ and some messages that we will use:
##

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
from math import pi
from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list
from moveit_trajectory_interface.srv import *
from pyjoycon import ButtonEventJoyCon, get_R_id
from tf2_ros import Buffer, TransformListener, TransformBroadcaster
from rospy import Time, Duration
from geometry_msgs.msg import Pose, PoseStamped
import time
from math import pi


class MoveGroupPythonIntefaceTutorial(object):
    """MoveGroupPythonIntefaceTutorial"""

    def __init__(self):
        super(MoveGroupPythonIntefaceTutorial, self).__init__()

        ## First initialize `moveit_commander`_ and a `rospy`_ node:
        moveit_commander.roscpp_initialize(sys.argv)
        rospy.init_node("move_group_python_interface_tutorial", anonymous=True)
        # rospy.wait_for_service('/panda/velocity_qp/getNewCartesianPath')

        ## Instantiate a `RobotCommander`_ object. This object is the outer-level interface to
        ## the robot:
        robot = moveit_commander.RobotCommander("/panda/robot_description", ns="panda")

        ## Instantiate a `PlanningSceneInterface`_ object.  This object is an interface
        ## to the world surrounding the robot:
        scene = moveit_commander.PlanningSceneInterface(ns="panda")

        ## Instantiate a `MoveGroupCommander`_ object.  This object is an interface
        ## to one group of joints.  In this case the group is the joints in the Panda
        ## arm so we set ``group_name = panda_arm``. If you are using a different robot,
        ## you should change this value to the name of your robot arm planning group.
        ## This interface can be used to plan and execute motions on the Panda:
        group_name = "panda_arm"
        group = moveit_commander.MoveGroupCommander(
            group_name, robot_description="/panda/robot_description", ns="panda"
        )

        ## We create a `DisplayTrajectory`_ publisher which is used later to publish
        ## trajectories for RViz to visualize:
        display_trajectory_publisher = rospy.Publisher(
            "/move_group/display_planned_path",
            moveit_msgs.msg.DisplayTrajectory,
            queue_size=20,
        )
        display_target_state_publisher = rospy.Publisher(
            "/panda/display_target_state",
            moveit_msgs.msg.DisplayRobotState,
            queue_size=20,
        )
        target_pose_pub = rospy.Publisher(
            "/panda/target_pose",
            geometry_msgs.msg.PoseStamped,
            queue_size=10,
        )

        # We can get the name of the reference frame for this robot:
        planning_frame = group.get_planning_frame()
        print("============ Reference frame: %s" % planning_frame)

        # We can also print the name of the end-effector link for this group:
        eef_link = group.get_end_effector_link()
        print("============ End effector: %s" % eef_link)

        # We can get a list of all the groups in the robot:
        group_names = robot.get_group_names()
        print("============ Robot Groups:", robot.get_group_names())

        # Sometimes for debugging it is useful to print the entire state of the
        # robot:
        print("============ Printing robot state")
        print(robot.get_current_state())
        print("")

        print("============ Printing group joint values")
        print(group.get_current_joint_values())
        print("")

        # Misc variables
        self.box_name = ""
        self.robot = robot
        self.scene = scene
        self.group = group
        self.display_trajectory_publisher = display_trajectory_publisher
        self.display_target_state_publisher = display_target_state_publisher
        self.target_pose_pub = target_pose_pub
        self.planning_frame = planning_frame
        self.eef_link = eef_link
        self.group_names = group_names

    def plan_to_tf(self, tf_name):
        tfBuffer = Buffer()
        tfListener = TransformListener(tfBuffer)
        for i in range(0, 20):
            tf_pose_to_go = tfBuffer.lookup_transform(
                "world", tf_name, Time(0), Duration(20)
            )
        pose_goal = Pose()

        pose_goal.position.x = tf_pose_to_go.transform.translation.x
        pose_goal.position.y = tf_pose_to_go.transform.translation.y
        pose_goal.position.z = tf_pose_to_go.transform.translation.z
        pose_goal.orientation.x = tf_pose_to_go.transform.rotation.x
        pose_goal.orientation.y = tf_pose_to_go.transform.rotation.y
        pose_goal.orientation.z = tf_pose_to_go.transform.rotation.z
        pose_goal.orientation.w = tf_pose_to_go.transform.rotation.w

        # print("go_to_tf / goal")
        # print(pose_goal)
        return self.plan_to_pose(pose_goal)

    def plan_to_pose(self, pose_to_go):
        # Copy class variables to local variables to make the web tutorials more clear.
        # In practice, you should use the class variables directly unless you have a good
        # reason not to.

        group = self.group
        waypoints = []

        waypoints.append(group.get_current_pose().pose)
        pose_goal = group.get_current_pose().pose
        pose_goal.position = pose_to_go.position
        pose_goal.orientation = pose_to_go.orientation

        waypoints.append(copy.deepcopy(pose_goal))

        # First try to go following a carteisan path
        (my_plan, fraction) = group.compute_cartesian_path(
            waypoints, 0.01, 0.0  # waypoints to follow  # eef_step
        )  # jump_threshold
        print("fraction " + str(fraction))
        if fraction < 1.0:
            print(
                "Fraction is less than one in Cartesian space, planning in joint space"
            )
            group.set_pose_target(pose_goal)
            # if fraction < 1.0 there is an obstacle along the path
            # Hence we plan in joint space
            plan = group.plan()  # jump_threshold
            # print(plan)
            my_plan = plan[1]
            fraction = plan[2]
            # Note: We are just planning, not asking move_group to actually move the robot yet:
        self.my_plan = my_plan

        if fraction == 1.0:
            pose_goal_stamped = PoseStamped()
            pose_goal_stamped.header.frame_id = "world"
            pose_goal_stamped.header.stamp = rospy.Time.now()
            pose_goal_stamped.pose = pose_goal
            self.target_pose_pub.publish(pose_goal_stamped)
            self.display_target_pose(my_plan)
        return fraction

    def go_to_plan(self):
        self.group.execute(self.my_plan, wait=False)

    def go_home(self):
        joint_goal = self.group.get_current_joint_values()
        joint_goal[0] = 0
        joint_goal[1] = 0
        joint_goal[2] = 0
        joint_goal[3] = -pi / 2
        joint_goal[4] = 0
        joint_goal[5] = pi / 2
        joint_goal[6] = pi / 4

        self.group.set_joint_value_target(joint_goal)
        # The go command can be called with joint values, poses, or without any
        # parameters if you have already set the pose or joint target for the group
        self.group.go(joint_goal, wait=False)

    def stop(self):
        self.group.stop()

    def display_target_pose(self, plan):
        # Copy class variables to local variables to make the web tutorials more clear.
        # In practice, you should use the class variables directly unless you have a good
        # reason not to.
        display_target_state_publisher = self.display_target_state_publisher
        print(plan)
        display_target_state = moveit_msgs.msg.DisplayRobotState()
        display_target_state.state.joint_state.name = plan.joint_trajectory.joint_names
        display_target_state.state.joint_state.header.frame_id = (
            plan.joint_trajectory.header.frame_id
        )
        display_target_state.state.joint_state.header.stamp = rospy.Time.now()
        display_target_state.state.joint_state.position = plan.joint_trajectory.points[
            -1
        ].positions

        display_target_state_publisher.publish(display_target_state)


def main():

    joycon_id = get_R_id()
    joycon = ButtonEventJoyCon(*joycon_id)
    print("connected to joycon")
    tutorial = MoveGroupPythonIntefaceTutorial()
    print("ready")
    while not rospy.is_shutdown():
        for event_type, status in joycon.events():
            print(event_type, status)
            if event_type == "a" and bool(status) == True:
                print("Planning to joycon")
                fraction = tutorial.plan_to_tf("joycon")
                print("fraction: " + str(fraction))
                rumble_data = b"\x05\x10\x10\x05\x05\x10\x10\x05"
                if fraction == 1.0:
                    print("fration is 1 rumbling once")
                    joycon.rumble_simple(rumble_data)
                    joycon.rumble_stop()
                else:
                    print("fration is <1 rumbling twice")
                    joycon.rumble_simple(rumble_data)
                    time.sleep(0.3)
                    joycon.rumble_simple(rumble_data)

                # tutorial.plan_to_tf("joycon")
            if event_type == "x" and bool(status) == True:
                tutorial.stop()
            if event_type == "y" and bool(status) == True:
                print("going to joycon")
                tutorial.stop()
                tutorial.go_to_plan()
            if event_type == "plus" and bool(status) == True:
                tutorial.plan_to_tf("joycon")
                tutorial.stop()
                time.sleep(2)
                tutorial.go_to_plan()
            if event_type == "home" and bool(status) == True:
                tutorial.stop()
                tutorial.go_home()


if __name__ == "__main__":
    main()
